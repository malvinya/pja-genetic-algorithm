##  Execution results
### Michalewicz Function
- Evolution cycles: 500
- Population size: 10
```
/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/bin/java "-javaagent:/Users/malva/Library/Application Support/JetBrains/Toolbox/apps/IDEA-U/ch-0/192.7142.36/IntelliJ IDEA.app/Contents/lib/idea_rt.jar=51339:/Users/malva/Library/Application Support/JetBrains/Toolbox/apps/IDEA-U/ch-0/192.7142.36/IntelliJ IDEA.app/Contents/bin" -Dfile.encoding=UTF-8 -classpath /Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/charsets.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/cldrdata.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/dnsns.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/jaccess.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/localedata.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/nashorn.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/sunec.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/sunjce_provider.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/sunpkcs11.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/ext/zipfs.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/jce.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/jsse.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/management-agent.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/resources.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/jre/lib/rt.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/lib/dt.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/lib/jconsole.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/lib/sa-jdi.jar:/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home/lib/tools.jar:/Users/malva/IdeaProjects/java-fundamentals-collections/04/GeneticAlgorithm/target/classes:/Users/malva/.m2/repository/ch/qos/logback/logback-classic/1.2.3/logback-classic-1.2.3.jar:/Users/malva/.m2/repository/ch/qos/logback/logback-core/1.2.3/logback-core-1.2.3.jar:/Users/malva/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar Algorithm
Generation: 0 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.749 [main] INFO Algorithm - Selected Individual{fitness=0.29, genes=[1.97, 0.37]}
17:05:53.758 [main] DEBUG Individual - Mutated by: 0.05
17:05:53.758 [main] DEBUG Individual - Percentage: 0.02
17:05:53.758 [main] DEBUG Individual - Mutated by: 0.26
17:05:53.759 [main] DEBUG Individual - Percentage: 0.08
17:05:53.759 [main] DEBUG Algorithm - New offspring Individual{fitness=0.42, genes=[2.02, 0.64]}
17:05:53.762 [main] DEBUG Algorithm - Weakest Individual{fitness=0.00, genes=[0.47, 3.13]}
17:05:53.767 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.03, genes=[0.09, 1.84]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.00, genes=[1.08, 2.40]}, Individual{fitness=0.00, genes=[0.64, 0.36]}, Individual{fitness=0.00, genes=[3.05, 1.10]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.768 [main] INFO Algorithm - Generation: 1 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.768 [main] INFO Algorithm - Selected Individual{fitness=0.03, genes=[0.09, 1.84]}
17:05:53.769 [main] DEBUG Individual - Mutated by: 0.16
17:05:53.769 [main] DEBUG Individual - Percentage: 0.05
17:05:53.769 [main] DEBUG Individual - Mutated by: 0.24
17:05:53.769 [main] DEBUG Individual - Percentage: 0.08
17:05:53.769 [main] DEBUG Algorithm - New offspring Individual{fitness=0.00, genes=[0.25, 2.08]}
17:05:53.770 [main] DEBUG Algorithm - Weakest Individual{fitness=0.00, genes=[0.64, 0.36]}
17:05:53.772 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.03, genes=[0.09, 1.84]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.00, genes=[1.08, 2.40]}, Individual{fitness=0.00, genes=[0.25, 2.08]}, Individual{fitness=0.00, genes=[3.05, 1.10]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.773 [main] INFO Algorithm - Generation: 2 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.773 [main] INFO Algorithm - Selected Individual{fitness=0.29, genes=[1.97, 0.37]}
17:05:53.773 [main] DEBUG Individual - Mutated by: 0.30
17:05:53.774 [main] DEBUG Individual - Percentage: 0.10
17:05:53.774 [main] DEBUG Individual - Mutated by: 0.22
17:05:53.774 [main] DEBUG Individual - Percentage: 0.07
17:05:53.774 [main] DEBUG Algorithm - New offspring Individual{fitness=0.72, genes=[2.27, 0.59]}
17:05:53.775 [main] DEBUG Algorithm - Weakest Individual{fitness=0.00, genes=[0.25, 2.08]}
17:05:53.778 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.03, genes=[0.09, 1.84]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.00, genes=[1.08, 2.40]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=0.00, genes=[3.05, 1.10]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.778 [main] INFO Algorithm - Generation: 3 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.778 [main] INFO Algorithm - Selected Individual{fitness=0.80, genes=[2.18, 2.46]}
17:05:53.779 [main] DEBUG Individual - Mutated by: 0.15
17:05:53.779 [main] DEBUG Individual - Percentage: 0.05
17:05:53.779 [main] DEBUG Individual - Mutated by: 0.29
17:05:53.779 [main] DEBUG Individual - Percentage: 0.09
17:05:53.779 [main] DEBUG Algorithm - New offspring Individual{fitness=0.91, genes=[2.33, 2.75]}
17:05:53.780 [main] DEBUG Algorithm - Weakest Individual{fitness=0.00, genes=[1.08, 2.40]}
17:05:53.783 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.03, genes=[0.09, 1.84]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=0.00, genes=[3.05, 1.10]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.783 [main] INFO Algorithm - Generation: 4 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.783 [main] INFO Algorithm - Selected Individual{fitness=0.03, genes=[0.09, 1.84]}
17:05:53.784 [main] DEBUG Individual - Mutated by: -0.31
17:05:53.784 [main] DEBUG Individual - Percentage: 0.10
17:05:53.784 [main] DEBUG Individual - Mutated by: -0.03
17:05:53.784 [main] DEBUG Individual - Percentage: 0.01
17:05:53.785 [main] DEBUG Algorithm - New offspring Individual{fitness=0.06, genes=[0.00, 1.81]}
17:05:53.785 [main] DEBUG Algorithm - Weakest Individual{fitness=0.00, genes=[3.05, 1.10]}
17:05:53.787 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.03, genes=[0.09, 1.84]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=0.06, genes=[0.00, 1.81]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.787 [main] INFO Algorithm - Generation: 5 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.788 [main] INFO Algorithm - Selected Individual{fitness=0.42, genes=[2.02, 0.64]}
17:05:53.788 [main] DEBUG Individual - Mutated by: 0.25
17:05:53.788 [main] DEBUG Individual - Percentage: 0.08
17:05:53.788 [main] DEBUG Individual - Mutated by: 0.17
17:05:53.788 [main] DEBUG Individual - Percentage: 0.05
17:05:53.789 [main] DEBUG Algorithm - New offspring Individual{fitness=0.72, genes=[2.27, 0.81]}
17:05:53.789 [main] DEBUG Algorithm - Weakest Individual{fitness=0.03, genes=[0.09, 1.84]}
17:05:53.791 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.72, genes=[2.27, 0.81]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=0.06, genes=[0.00, 1.81]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.792 [main] INFO Algorithm - Generation: 6 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.792 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.792 [main] DEBUG Individual - Mutated by: 0.15
17:05:53.792 [main] DEBUG Individual - Percentage: 0.05
17:05:53.793 [main] DEBUG Individual - Mutated by: 0.09
17:05:53.793 [main] DEBUG Individual - Percentage: 0.03
17:05:53.793 [main] DEBUG Algorithm - New offspring Individual{fitness=1.10, genes=[2.50, 1.56]}
17:05:53.793 [main] DEBUG Algorithm - Weakest Individual{fitness=0.06, genes=[0.00, 1.81]}
17:05:53.798 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.72, genes=[2.27, 0.81]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.29, genes=[1.97, 0.37]}]}
17:05:53.798 [main] INFO Algorithm - Generation: 7 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.799 [main] INFO Algorithm - Selected Individual{fitness=0.94, genes=[0.47, 1.61]}
17:05:53.799 [main] DEBUG Individual - Mutated by: -0.10
17:05:53.800 [main] DEBUG Individual - Percentage: 0.03
17:05:53.803 [main] DEBUG Individual - Mutated by: -0.02
17:05:53.804 [main] DEBUG Individual - Percentage: 0.01
17:05:53.804 [main] DEBUG Algorithm - New offspring Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.804 [main] DEBUG Algorithm - Weakest Individual{fitness=0.29, genes=[1.97, 0.37]}
17:05:53.806 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.72, genes=[2.27, 0.81]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.806 [main] INFO Algorithm - Generation: 8 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.807 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.807 [main] DEBUG Individual - Mutated by: -0.04
17:05:53.807 [main] DEBUG Individual - Percentage: 0.01
17:05:53.807 [main] DEBUG Individual - Mutated by: -0.21
17:05:53.807 [main] DEBUG Individual - Percentage: 0.07
17:05:53.807 [main] DEBUG Algorithm - New offspring Individual{fitness=0.28, genes=[0.32, 1.38]}
17:05:53.808 [main] DEBUG Algorithm - Weakest Individual{fitness=0.42, genes=[2.02, 0.64]}
17:05:53.812 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.72, genes=[2.27, 0.81]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.813 [main] INFO Algorithm - Generation: 9 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.813 [main] INFO Algorithm - Selected Individual{fitness=0.80, genes=[2.18, 2.46]}
17:05:53.813 [main] DEBUG Individual - Mutated by: -0.28
17:05:53.813 [main] DEBUG Individual - Percentage: 0.09
17:05:53.813 [main] DEBUG Individual - Mutated by: -0.21
17:05:53.813 [main] DEBUG Individual - Percentage: 0.07
17:05:53.813 [main] DEBUG Algorithm - New offspring Individual{fitness=0.16, genes=[1.90, 2.25]}
17:05:53.813 [main] DEBUG Algorithm - Weakest Individual{fitness=0.42, genes=[2.02, 0.64]}
17:05:53.815 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.72, genes=[2.27, 0.81]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.42, genes=[2.02, 0.64]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.815 [main] INFO Algorithm - Generation: 10 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.815 [main] INFO Algorithm - Selected Individual{fitness=0.74, genes=[2.27, 0.82]}
17:05:53.818 [main] DEBUG Individual - Mutated by: -0.09
17:05:53.819 [main] DEBUG Individual - Percentage: 0.03
17:05:53.819 [main] DEBUG Individual - Mutated by: -0.24
17:05:53.819 [main] DEBUG Individual - Percentage: 0.08
17:05:53.819 [main] DEBUG Algorithm - New offspring Individual{fitness=0.79, genes=[2.18, 0.58]}
17:05:53.819 [main] DEBUG Algorithm - Weakest Individual{fitness=0.42, genes=[2.02, 0.64]}
17:05:53.820 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.72, genes=[2.27, 0.81]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.821 [main] INFO Algorithm - Generation: 11 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.833 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.843 [main] DEBUG Individual - Mutated by: -0.07
17:05:53.843 [main] DEBUG Individual - Percentage: 0.02
17:05:53.843 [main] DEBUG Individual - Mutated by: -0.16
17:05:53.843 [main] DEBUG Individual - Percentage: 0.05
17:05:53.843 [main] DEBUG Algorithm - New offspring Individual{fitness=0.82, genes=[2.27, 1.31]}
17:05:53.843 [main] DEBUG Algorithm - Weakest Individual{fitness=0.72, genes=[2.27, 0.81]}
17:05:53.845 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.845 [main] INFO Algorithm - Generation: 12 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.845 [main] INFO Algorithm - Selected Individual{fitness=0.80, genes=[2.18, 2.46]}
17:05:53.845 [main] DEBUG Individual - Mutated by: -0.06
17:05:53.845 [main] DEBUG Individual - Percentage: 0.02
17:05:53.845 [main] DEBUG Individual - Mutated by: -0.25
17:05:53.846 [main] DEBUG Individual - Percentage: 0.08
17:05:53.846 [main] DEBUG Algorithm - New offspring Individual{fitness=0.72, genes=[2.13, 2.21]}
17:05:53.846 [main] DEBUG Algorithm - Weakest Individual{fitness=0.72, genes=[2.27, 0.59]}
17:05:53.851 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.851 [main] INFO Algorithm - Generation: 13 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.851 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.851 [main] DEBUG Individual - Mutated by: 0.04
17:05:53.852 [main] DEBUG Individual - Percentage: 0.01
17:05:53.852 [main] DEBUG Individual - Mutated by: 0.25
17:05:53.852 [main] DEBUG Individual - Percentage: 0.08
17:05:53.852 [main] DEBUG Algorithm - New offspring Individual{fitness=0.03, genes=[0.41, 1.83]}
17:05:53.852 [main] DEBUG Algorithm - Weakest Individual{fitness=0.72, genes=[2.27, 0.59]}
17:05:53.853 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.853 [main] INFO Algorithm - Generation: 14 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.854 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.854 [main] DEBUG Individual - Mutated by: 0.17
17:05:53.854 [main] DEBUG Individual - Percentage: 0.05
17:05:53.855 [main] DEBUG Individual - Mutated by: 0.12
17:05:53.855 [main] DEBUG Individual - Percentage: 0.04
17:05:53.855 [main] DEBUG Algorithm - New offspring Individual{fitness=0.43, genes=[0.53, 1.71]}
17:05:53.855 [main] DEBUG Algorithm - Weakest Individual{fitness=0.72, genes=[2.27, 0.59]}
17:05:53.857 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.858 [main] INFO Algorithm - Generation: 15 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.860 [main] INFO Algorithm - Selected Individual{fitness=1.10, genes=[2.50, 1.56]}
17:05:53.860 [main] DEBUG Individual - Mutated by: -0.04
17:05:53.860 [main] DEBUG Individual - Percentage: 0.01
17:05:53.860 [main] DEBUG Individual - Mutated by: -0.26
17:05:53.860 [main] DEBUG Individual - Percentage: 0.08
17:05:53.860 [main] DEBUG Algorithm - New offspring Individual{fitness=0.26, genes=[2.46, 1.30]}
17:05:53.861 [main] DEBUG Algorithm - Weakest Individual{fitness=0.72, genes=[2.27, 0.59]}
17:05:53.863 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=0.72, genes=[2.27, 0.59]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.863 [main] INFO Algorithm - Generation: 16 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.864 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.864 [main] DEBUG Individual - Mutated by: -0.20
17:05:53.864 [main] DEBUG Individual - Percentage: 0.06
17:05:53.864 [main] DEBUG Individual - Mutated by: -0.06
17:05:53.864 [main] DEBUG Individual - Percentage: 0.02
17:05:53.864 [main] DEBUG Algorithm - New offspring Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:53.864 [main] DEBUG Algorithm - Weakest Individual{fitness=0.72, genes=[2.27, 0.59]}
17:05:53.867 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.867 [main] INFO Algorithm - Generation: 17 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.867 [main] INFO Algorithm - Selected Individual{fitness=1.10, genes=[2.50, 1.56]}
17:05:53.867 [main] DEBUG Individual - Mutated by: 0.08
17:05:53.867 [main] DEBUG Individual - Percentage: 0.03
17:05:53.868 [main] DEBUG Individual - Mutated by: 0.17
17:05:53.868 [main] DEBUG Individual - Percentage: 0.05
17:05:53.868 [main] DEBUG Algorithm - New offspring Individual{fitness=0.31, genes=[2.58, 1.74]}
17:05:53.868 [main] DEBUG Algorithm - Weakest Individual{fitness=0.74, genes=[2.27, 0.82]}
17:05:53.869 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=0.74, genes=[2.27, 0.82]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.869 [main] INFO Algorithm - Generation: 18 Fittest: Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.869 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.869 [main] DEBUG Individual - Mutated by: -0.13
17:05:53.869 [main] DEBUG Individual - Percentage: 0.04
17:05:53.870 [main] DEBUG Individual - Mutated by: -0.02
17:05:53.870 [main] DEBUG Individual - Percentage: 0.01
17:05:53.870 [main] DEBUG Algorithm - New offspring Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.870 [main] DEBUG Algorithm - Weakest Individual{fitness=0.74, genes=[2.27, 0.82]}
17:05:53.871 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=0.79, genes=[2.18, 0.58]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.871 [main] INFO Algorithm - Generation: 19 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.871 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.871 [main] DEBUG Individual - Mutated by: 0.08
17:05:53.871 [main] DEBUG Individual - Percentage: 0.02
17:05:53.871 [main] DEBUG Individual - Mutated by: 0.02
17:05:53.872 [main] DEBUG Individual - Percentage: 0.01
17:05:53.872 [main] DEBUG Algorithm - New offspring Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.872 [main] DEBUG Algorithm - Weakest Individual{fitness=0.79, genes=[2.18, 0.58]}
17:05:53.877 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.80, genes=[2.18, 2.46]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.877 [main] INFO Algorithm - Generation: 20 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.878 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.878 [main] DEBUG Individual - Mutated by: -0.24
17:05:53.878 [main] DEBUG Individual - Percentage: 0.08
17:05:53.878 [main] DEBUG Individual - Mutated by: -0.04
17:05:53.878 [main] DEBUG Individual - Percentage: 0.01
17:05:53.879 [main] DEBUG Algorithm - New offspring Individual{fitness=0.97, genes=[0.12, 1.54]}
17:05:53.879 [main] DEBUG Algorithm - Weakest Individual{fitness=0.80, genes=[2.18, 2.46]}
17:05:53.881 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.82, genes=[2.27, 1.31]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.883 [main] INFO Algorithm - Generation: 21 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.884 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.885 [main] DEBUG Individual - Mutated by: -0.01
17:05:53.885 [main] DEBUG Individual - Percentage: 0.00
17:05:53.885 [main] DEBUG Individual - Mutated by: -0.20
17:05:53.885 [main] DEBUG Individual - Percentage: 0.06
17:05:53.885 [main] DEBUG Algorithm - New offspring Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.885 [main] DEBUG Algorithm - Weakest Individual{fitness=0.82, genes=[2.27, 1.31]}
17:05:53.890 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.891 [main] INFO Algorithm - Generation: 22 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.891 [main] INFO Algorithm - Selected Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.891 [main] DEBUG Individual - Mutated by: 0.03
17:05:53.891 [main] DEBUG Individual - Percentage: 0.01
17:05:53.891 [main] DEBUG Individual - Mutated by: 0.26
17:05:53.891 [main] DEBUG Individual - Percentage: 0.08
17:05:53.892 [main] DEBUG Algorithm - New offspring Individual{fitness=0.38, genes=[2.45, 1.76]}
17:05:53.892 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.892 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.892 [main] INFO Algorithm - Generation: 23 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.893 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.893 [main] DEBUG Individual - Mutated by: 0.05
17:05:53.893 [main] DEBUG Individual - Percentage: 0.02
17:05:53.893 [main] DEBUG Individual - Mutated by: 0.26
17:05:53.893 [main] DEBUG Individual - Percentage: 0.08
17:05:53.893 [main] DEBUG Algorithm - New offspring Individual{fitness=0.64, genes=[2.40, 1.73]}
17:05:53.893 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.894 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.894 [main] INFO Algorithm - Generation: 24 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.894 [main] INFO Algorithm - Selected Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:53.894 [main] DEBUG Individual - Mutated by: -0.26
17:05:53.894 [main] DEBUG Individual - Percentage: 0.08
17:05:53.897 [main] DEBUG Individual - Mutated by: -0.06
17:05:53.897 [main] DEBUG Individual - Percentage: 0.02
17:05:53.897 [main] DEBUG Algorithm - New offspring Individual{fitness=0.29, genes=[1.88, 1.35]}
17:05:53.897 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.898 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.898 [main] INFO Algorithm - Generation: 25 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.898 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.898 [main] DEBUG Individual - Mutated by: 0.20
17:05:53.898 [main] DEBUG Individual - Percentage: 0.06
17:05:53.898 [main] DEBUG Individual - Mutated by: 0.08
17:05:53.898 [main] DEBUG Individual - Percentage: 0.03
17:05:53.898 [main] DEBUG Algorithm - New offspring Individual{fitness=0.66, genes=[0.56, 1.67]}
17:05:53.899 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.900 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.900 [main] INFO Algorithm - Generation: 26 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.901 [main] INFO Algorithm - Selected Individual{fitness=1.10, genes=[2.50, 1.56]}
17:05:53.902 [main] DEBUG Individual - Mutated by: 0.00
17:05:53.902 [main] DEBUG Individual - Percentage: 0.00
17:05:53.903 [main] DEBUG Individual - Mutated by: 0.23
17:05:53.903 [main] DEBUG Individual - Percentage: 0.07
17:05:53.903 [main] DEBUG Algorithm - New offspring Individual{fitness=0.19, genes=[2.50, 1.80]}
17:05:53.903 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.903 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.903 [main] INFO Algorithm - Generation: 27 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.904 [main] INFO Algorithm - Selected Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:53.906 [main] DEBUG Individual - Mutated by: -0.04
17:05:53.906 [main] DEBUG Individual - Percentage: 0.01
17:05:53.906 [main] DEBUG Individual - Mutated by: -0.23
17:05:53.906 [main] DEBUG Individual - Percentage: 0.07
17:05:53.906 [main] DEBUG Algorithm - New offspring Individual{fitness=0.66, genes=[2.10, 1.18]}
17:05:53.906 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.907 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.907 [main] INFO Algorithm - Generation: 28 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.907 [main] INFO Algorithm - Selected Individual{fitness=0.94, genes=[0.47, 1.61]}
17:05:53.907 [main] DEBUG Individual - Mutated by: 0.23
17:05:53.907 [main] DEBUG Individual - Percentage: 0.07
17:05:53.907 [main] DEBUG Individual - Mutated by: 0.05
17:05:53.908 [main] DEBUG Individual - Percentage: 0.02
17:05:53.908 [main] DEBUG Algorithm - New offspring Individual{fitness=0.69, genes=[0.69, 1.66]}
17:05:53.908 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.909 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=0.83, genes=[2.20, 1.25]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.909 [main] INFO Algorithm - Generation: 29 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.909 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.909 [main] DEBUG Individual - Mutated by: 0.27
17:05:53.909 [main] DEBUG Individual - Percentage: 0.09
17:05:53.909 [main] DEBUG Individual - Mutated by: 0.11
17:05:53.909 [main] DEBUG Individual - Percentage: 0.03
17:05:53.909 [main] DEBUG Algorithm - New offspring Individual{fitness=1.12, genes=[2.48, 1.56]}
17:05:53.920 [main] DEBUG Algorithm - Weakest Individual{fitness=0.83, genes=[2.20, 1.25]}
17:05:53.921 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.921 [main] INFO Algorithm - Generation: 30 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.922 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.922 [main] DEBUG Individual - Mutated by: 0.21
17:05:53.922 [main] DEBUG Individual - Percentage: 0.07
17:05:53.923 [main] DEBUG Individual - Mutated by: 0.31
17:05:53.923 [main] DEBUG Individual - Percentage: 0.10
17:05:53.923 [main] DEBUG Algorithm - New offspring Individual{fitness=0.43, genes=[2.43, 1.76]}
17:05:53.923 [main] DEBUG Algorithm - Weakest Individual{fitness=0.91, genes=[2.33, 2.75]}
17:05:53.924 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.924 [main] INFO Algorithm - Generation: 31 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.924 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.924 [main] DEBUG Individual - Mutated by: -0.11
17:05:53.925 [main] DEBUG Individual - Percentage: 0.03
17:05:53.925 [main] DEBUG Individual - Mutated by: -0.24
17:05:53.925 [main] DEBUG Individual - Percentage: 0.08
17:05:53.925 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.24, 1.23]}
17:05:53.925 [main] DEBUG Algorithm - Weakest Individual{fitness=0.91, genes=[2.33, 2.75]}
17:05:53.928 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=0.91, genes=[2.33, 2.75]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.928 [main] INFO Algorithm - Generation: 32 Fittest: Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.929 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.929 [main] DEBUG Individual - Mutated by: 0.15
17:05:53.929 [main] DEBUG Individual - Percentage: 0.05
17:05:53.929 [main] DEBUG Individual - Mutated by: 0.12
17:05:53.929 [main] DEBUG Individual - Percentage: 0.04
17:05:53.929 [main] DEBUG Algorithm - New offspring Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.929 [main] DEBUG Algorithm - Weakest Individual{fitness=0.91, genes=[2.33, 2.75]}
17:05:53.933 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.933 [main] INFO Algorithm - Generation: 33 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.933 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.933 [main] DEBUG Individual - Mutated by: -0.14
17:05:53.933 [main] DEBUG Individual - Percentage: 0.05
17:05:53.933 [main] DEBUG Individual - Mutated by: -0.12
17:05:53.933 [main] DEBUG Individual - Percentage: 0.04
17:05:53.933 [main] DEBUG Algorithm - New offspring Individual{fitness=0.70, genes=[0.22, 1.47]}
17:05:53.934 [main] DEBUG Algorithm - Weakest Individual{fitness=0.94, genes=[0.47, 1.61]}
17:05:53.934 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=0.94, genes=[0.47, 1.61]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.934 [main] INFO Algorithm - Generation: 34 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.934 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.934 [main] DEBUG Individual - Mutated by: 0.20
17:05:53.934 [main] DEBUG Individual - Percentage: 0.06
17:05:53.934 [main] DEBUG Individual - Mutated by: 0.18
17:05:53.935 [main] DEBUG Individual - Percentage: 0.06
17:05:53.935 [main] DEBUG Algorithm - New offspring Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:53.935 [main] DEBUG Algorithm - Weakest Individual{fitness=0.94, genes=[0.47, 1.61]}
17:05:53.935 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.935 [main] INFO Algorithm - Generation: 35 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.935 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:53.935 [main] DEBUG Individual - Mutated by: 0.10
17:05:53.935 [main] DEBUG Individual - Percentage: 0.03
17:05:53.936 [main] DEBUG Individual - Mutated by: 0.29
17:05:53.936 [main] DEBUG Individual - Percentage: 0.09
17:05:53.936 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.31, 1.74]}
17:05:53.936 [main] DEBUG Algorithm - Weakest Individual{fitness=0.97, genes=[0.12, 1.54]}
17:05:53.936 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.940 [main] INFO Algorithm - Generation: 36 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.941 [main] INFO Algorithm - Selected Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:53.941 [main] DEBUG Individual - Mutated by: -0.27
17:05:53.941 [main] DEBUG Individual - Percentage: 0.08
17:05:53.941 [main] DEBUG Individual - Mutated by: -0.09
17:05:53.941 [main] DEBUG Individual - Percentage: 0.03
17:05:53.941 [main] DEBUG Algorithm - New offspring Individual{fitness=0.23, genes=[1.88, 1.32]}
17:05:53.941 [main] DEBUG Algorithm - Weakest Individual{fitness=0.97, genes=[0.12, 1.54]}
17:05:53.942 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.942 [main] INFO Algorithm - Generation: 37 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.942 [main] INFO Algorithm - Selected Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:53.942 [main] DEBUG Individual - Mutated by: 0.20
17:05:53.942 [main] DEBUG Individual - Percentage: 0.06
17:05:53.942 [main] DEBUG Individual - Mutated by: 0.02
17:05:53.942 [main] DEBUG Individual - Percentage: 0.00
17:05:53.942 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.61, 1.64]}
17:05:53.942 [main] DEBUG Algorithm - Weakest Individual{fitness=0.97, genes=[0.12, 1.54]}
17:05:53.943 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.943 [main] INFO Algorithm - Generation: 38 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.943 [main] INFO Algorithm - Selected Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.943 [main] DEBUG Individual - Mutated by: 0.30
17:05:53.943 [main] DEBUG Individual - Percentage: 0.09
17:05:53.943 [main] DEBUG Individual - Mutated by: 0.08
17:05:53.943 [main] DEBUG Individual - Percentage: 0.03
17:05:53.943 [main] DEBUG Algorithm - New offspring Individual{fitness=0.64, genes=[0.66, 1.67]}
17:05:53.943 [main] DEBUG Algorithm - Weakest Individual{fitness=0.97, genes=[0.12, 1.54]}
17:05:53.944 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.97, genes=[0.12, 1.54]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.944 [main] INFO Algorithm - Generation: 39 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.944 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.944 [main] DEBUG Individual - Mutated by: 0.19
17:05:53.944 [main] DEBUG Individual - Percentage: 0.06
17:05:53.944 [main] DEBUG Individual - Mutated by: 0.14
17:05:53.944 [main] DEBUG Individual - Percentage: 0.04
17:05:53.944 [main] DEBUG Algorithm - New offspring Individual{fitness=0.98, genes=[2.53, 1.61]}
17:05:53.944 [main] DEBUG Algorithm - Weakest Individual{fitness=0.97, genes=[0.12, 1.54]}
17:05:53.945 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=0.98, genes=[2.53, 1.61]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.945 [main] INFO Algorithm - Generation: 40 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.945 [main] INFO Algorithm - Selected Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:53.945 [main] DEBUG Individual - Mutated by: -0.12
17:05:53.945 [main] DEBUG Individual - Percentage: 0.04
17:05:53.945 [main] DEBUG Individual - Mutated by: -0.14
17:05:53.945 [main] DEBUG Individual - Percentage: 0.04
17:05:53.945 [main] DEBUG Algorithm - New offspring Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:53.945 [main] DEBUG Algorithm - Weakest Individual{fitness=0.98, genes=[2.53, 1.61]}
17:05:53.946 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.946 [main] INFO Algorithm - Generation: 41 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.946 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.946 [main] DEBUG Individual - Mutated by: -0.04
17:05:53.946 [main] DEBUG Individual - Percentage: 0.01
17:05:53.946 [main] DEBUG Individual - Mutated by: -0.16
17:05:53.946 [main] DEBUG Individual - Percentage: 0.05
17:05:53.946 [main] DEBUG Algorithm - New offspring Individual{fitness=0.93, genes=[2.33, 1.40]}
17:05:53.946 [main] DEBUG Algorithm - Weakest Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.953 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.954 [main] INFO Algorithm - Generation: 42 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.955 [main] INFO Algorithm - Selected Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:53.955 [main] DEBUG Individual - Mutated by: 0.17
17:05:53.955 [main] DEBUG Individual - Percentage: 0.05
17:05:53.955 [main] DEBUG Individual - Mutated by: 0.16
17:05:53.955 [main] DEBUG Individual - Percentage: 0.05
17:05:53.955 [main] DEBUG Algorithm - New offspring Individual{fitness=0.13, genes=[2.58, 1.79]}
17:05:53.956 [main] DEBUG Algorithm - Weakest Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.956 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.956 [main] INFO Algorithm - Generation: 43 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.956 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.956 [main] DEBUG Individual - Mutated by: 0.10
17:05:53.957 [main] DEBUG Individual - Percentage: 0.03
17:05:53.957 [main] DEBUG Individual - Mutated by: 0.22
17:05:53.957 [main] DEBUG Individual - Percentage: 0.07
17:05:53.957 [main] DEBUG Algorithm - New offspring Individual{fitness=0.28, genes=[2.46, 1.79]}
17:05:53.957 [main] DEBUG Algorithm - Weakest Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.957 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.958 [main] INFO Algorithm - Generation: 44 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.958 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.958 [main] DEBUG Individual - Mutated by: -0.19
17:05:53.958 [main] DEBUG Individual - Percentage: 0.06
17:05:53.958 [main] DEBUG Individual - Mutated by: -0.28
17:05:53.958 [main] DEBUG Individual - Percentage: 0.09
17:05:53.958 [main] DEBUG Algorithm - New offspring Individual{fitness=0.78, genes=[2.16, 1.20]}
17:05:53.959 [main] DEBUG Algorithm - Weakest Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.960 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.960 [main] INFO Algorithm - Generation: 45 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.960 [main] INFO Algorithm - Selected Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.960 [main] DEBUG Individual - Mutated by: 0.11
17:05:53.960 [main] DEBUG Individual - Percentage: 0.03
17:05:53.960 [main] DEBUG Individual - Mutated by: 0.31
17:05:53.960 [main] DEBUG Individual - Percentage: 0.10
17:05:53.960 [main] DEBUG Algorithm - New offspring Individual{fitness=0.13, genes=[2.53, 1.81]}
17:05:53.960 [main] DEBUG Algorithm - Weakest Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.961 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=0.99, genes=[0.36, 1.59]}]}
17:05:53.961 [main] INFO Algorithm - Generation: 46 Fittest: Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:53.964 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:53.966 [main] DEBUG Individual - Mutated by: -0.15
17:05:53.966 [main] DEBUG Individual - Percentage: 0.05
17:05:53.966 [main] DEBUG Individual - Mutated by: -0.01
17:05:53.967 [main] DEBUG Individual - Percentage: 0.00
17:05:53.967 [main] DEBUG Algorithm - New offspring Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:53.968 [main] DEBUG Algorithm - Weakest Individual{fitness=0.99, genes=[0.36, 1.59]}
17:05:53.974 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:53.981 [main] INFO Algorithm - Generation: 47 Fittest: Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:53.981 [main] INFO Algorithm - Selected Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:53.981 [main] DEBUG Individual - Mutated by: -0.11
17:05:53.982 [main] DEBUG Individual - Percentage: 0.04
17:05:53.982 [main] DEBUG Individual - Mutated by: -0.31
17:05:53.982 [main] DEBUG Individual - Percentage: 0.10
17:05:53.982 [main] DEBUG Algorithm - New offspring Individual{fitness=0.45, genes=[2.03, 1.10]}
17:05:53.982 [main] DEBUG Algorithm - Weakest Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.986 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:53.987 [main] INFO Algorithm - Generation: 48 Fittest: Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:53.992 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.994 [main] DEBUG Individual - Mutated by: -0.27
17:05:53.994 [main] DEBUG Individual - Percentage: 0.09
17:05:53.994 [main] DEBUG Individual - Mutated by: -0.17
17:05:53.995 [main] DEBUG Individual - Percentage: 0.05
17:05:53.995 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.08, 1.30]}
17:05:53.995 [main] DEBUG Algorithm - Weakest Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.996 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:53.996 [main] INFO Algorithm - Generation: 49 Fittest: Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:53.996 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:53.996 [main] DEBUG Individual - Mutated by: -0.01
17:05:53.996 [main] DEBUG Individual - Percentage: 0.00
17:05:53.996 [main] DEBUG Individual - Mutated by: -0.14
17:05:53.996 [main] DEBUG Individual - Percentage: 0.05
17:05:53.996 [main] DEBUG Algorithm - New offspring Individual{fitness=0.86, genes=[2.28, 1.35]}
17:05:53.996 [main] DEBUG Algorithm - Weakest Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.997 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:53.997 [main] INFO Algorithm - Generation: 50 Fittest: Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:53.997 [main] INFO Algorithm - Selected Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:53.997 [main] DEBUG Individual - Mutated by: -0.19
17:05:53.997 [main] DEBUG Individual - Percentage: 0.06
17:05:53.997 [main] DEBUG Individual - Mutated by: -0.08
17:05:53.997 [main] DEBUG Individual - Percentage: 0.03
17:05:53.997 [main] DEBUG Algorithm - New offspring Individual{fitness=1.08, genes=[2.16, 1.39]}
17:05:53.997 [main] DEBUG Algorithm - Weakest Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:53.998 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.09, genes=[2.42, 1.50]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.005 [main] INFO Algorithm - Generation: 51 Fittest: Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:54.007 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.008 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.008 [main] DEBUG Individual - Percentage: 0.04
17:05:54.008 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.008 [main] DEBUG Individual - Percentage: 0.03
17:05:54.008 [main] DEBUG Algorithm - New offspring Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.008 [main] DEBUG Algorithm - Weakest Individual{fitness=1.09, genes=[2.42, 1.50]}
17:05:54.008 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.10, genes=[2.50, 1.56]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.008 [main] INFO Algorithm - Generation: 52 Fittest: Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.008 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.008 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.009 [main] DEBUG Individual - Percentage: 0.05
17:05:54.009 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.009 [main] DEBUG Individual - Percentage: 0.06
17:05:54.009 [main] DEBUG Algorithm - New offspring Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.009 [main] DEBUG Algorithm - Weakest Individual{fitness=1.10, genes=[2.50, 1.56]}
17:05:54.009 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.009 [main] INFO Algorithm - Generation: 53 Fittest: Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.009 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.009 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.010 [main] DEBUG Individual - Percentage: 0.07
17:05:54.010 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.010 [main] DEBUG Individual - Percentage: 0.06
17:05:54.010 [main] DEBUG Algorithm - New offspring Individual{fitness=1.03, genes=[2.45, 1.64]}
17:05:54.010 [main] DEBUG Algorithm - Weakest Individual{fitness=1.12, genes=[2.48, 1.56]}
17:05:54.010 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.12, genes=[2.48, 1.56]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.010 [main] INFO Algorithm - Generation: 54 Fittest: Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.010 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.010 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.010 [main] DEBUG Individual - Percentage: 0.03
17:05:54.010 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.010 [main] DEBUG Individual - Percentage: 0.02
17:05:54.010 [main] DEBUG Algorithm - New offspring Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.011 [main] DEBUG Algorithm - Weakest Individual{fitness=1.12, genes=[2.48, 1.56]}
17:05:54.011 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.011 [main] INFO Algorithm - Generation: 55 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.011 [main] INFO Algorithm - Selected Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:54.011 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.011 [main] DEBUG Individual - Percentage: 0.08
17:05:54.016 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.017 [main] DEBUG Individual - Percentage: 0.04
17:05:54.017 [main] DEBUG Algorithm - New offspring Individual{fitness=0.28, genes=[1.88, 1.35]}
17:05:54.017 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.017 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.017 [main] INFO Algorithm - Generation: 56 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.018 [main] INFO Algorithm - Selected Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:54.018 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.018 [main] DEBUG Individual - Percentage: 0.00
17:05:54.018 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.018 [main] DEBUG Individual - Percentage: 0.02
17:05:54.018 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.13, 1.34]}
17:05:54.018 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.019 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.019 [main] INFO Algorithm - Generation: 57 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.020 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.022 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.022 [main] DEBUG Individual - Percentage: 0.05
17:05:54.023 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.024 [main] DEBUG Individual - Percentage: 0.04
17:05:54.025 [main] DEBUG Algorithm - New offspring Individual{fitness=0.65, genes=[2.05, 1.33]}
17:05:54.028 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.031 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.033 [main] INFO Algorithm - Generation: 58 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.034 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.036 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.037 [main] DEBUG Individual - Percentage: 0.04
17:05:54.037 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.037 [main] DEBUG Individual - Percentage: 0.09
17:05:54.038 [main] DEBUG Algorithm - New offspring Individual{fitness=0.62, genes=[2.36, 1.77]}
17:05:54.038 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.048 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.048 [main] INFO Algorithm - Generation: 59 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.048 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.048 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.048 [main] DEBUG Individual - Percentage: 0.08
17:05:54.048 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.048 [main] DEBUG Individual - Percentage: 0.06
17:05:54.049 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.10, 1.37]}
17:05:54.050 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.054 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.14, genes=[2.38, 1.66]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.054 [main] INFO Algorithm - Generation: 60 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.054 [main] INFO Algorithm - Selected Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:54.054 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.054 [main] DEBUG Individual - Percentage: 0.05
17:05:54.054 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.054 [main] DEBUG Individual - Percentage: 0.01
17:05:54.054 [main] DEBUG Algorithm - New offspring Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.054 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.38, 1.66]}
17:05:54.054 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.054 [main] INFO Algorithm - Generation: 61 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.054 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.054 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.054 [main] DEBUG Individual - Percentage: 0.05
17:05:54.055 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.055 [main] DEBUG Individual - Percentage: 0.03
17:05:54.055 [main] DEBUG Algorithm - New offspring Individual{fitness=0.78, genes=[2.06, 1.37]}
17:05:54.055 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:54.055 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.055 [main] INFO Algorithm - Generation: 62 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.067 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.068 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.068 [main] DEBUG Individual - Percentage: 0.05
17:05:54.068 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.068 [main] DEBUG Individual - Percentage: 0.00
17:05:54.068 [main] DEBUG Algorithm - New offspring Individual{fitness=1.11, genes=[2.48, 1.55]}
17:05:54.068 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:54.069 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.069 [main] INFO Algorithm - Generation: 63 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.069 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.069 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.070 [main] DEBUG Individual - Percentage: 0.05
17:05:54.072 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.072 [main] DEBUG Individual - Percentage: 0.08
17:05:54.072 [main] DEBUG Algorithm - New offspring Individual{fitness=0.26, genes=[2.47, 1.79]}
17:05:54.072 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:54.073 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.073 [main] INFO Algorithm - Generation: 64 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.073 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.073 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.073 [main] DEBUG Individual - Percentage: 0.06
17:05:54.073 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.073 [main] DEBUG Individual - Percentage: 0.07
17:05:54.073 [main] DEBUG Algorithm - New offspring Individual{fitness=0.34, genes=[2.49, 1.75]}
17:05:54.073 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:54.073 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.14, genes=[2.14, 1.41]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.073 [main] INFO Algorithm - Generation: 65 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.074 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.074 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.074 [main] DEBUG Individual - Percentage: 0.01
17:05:54.074 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.074 [main] DEBUG Individual - Percentage: 0.01
17:05:54.074 [main] DEBUG Algorithm - New offspring Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.074 [main] DEBUG Algorithm - Weakest Individual{fitness=1.14, genes=[2.14, 1.41]}
17:05:54.074 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.074 [main] INFO Algorithm - Generation: 66 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.074 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.074 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.074 [main] DEBUG Individual - Percentage: 0.08
17:05:54.074 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.074 [main] DEBUG Individual - Percentage: 0.08
17:05:54.074 [main] DEBUG Algorithm - New offspring Individual{fitness=0.45, genes=[2.46, 1.74]}
17:05:54.075 [main] DEBUG Algorithm - Weakest Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:54.075 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.17, genes=[2.41, 1.63]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.075 [main] INFO Algorithm - Generation: 67 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.075 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.075 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.075 [main] DEBUG Individual - Percentage: 0.06
17:05:54.075 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.075 [main] DEBUG Individual - Percentage: 0.02
17:05:54.075 [main] DEBUG Algorithm - New offspring Individual{fitness=1.30, genes=[2.40, 1.55]}
17:05:54.075 [main] DEBUG Algorithm - Weakest Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:54.076 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.30, genes=[2.40, 1.55]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.076 [main] INFO Algorithm - Generation: 68 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.076 [main] INFO Algorithm - Selected Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.084 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.084 [main] DEBUG Individual - Percentage: 0.02
17:05:54.084 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.084 [main] DEBUG Individual - Percentage: 0.02
17:05:54.084 [main] DEBUG Algorithm - New offspring Individual{fitness=1.00, genes=[2.14, 1.38]}
17:05:54.084 [main] DEBUG Algorithm - Weakest Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:54.084 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.30, genes=[2.40, 1.55]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.085 [main] INFO Algorithm - Generation: 69 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.087 [main] INFO Algorithm - Selected Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.090 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.090 [main] DEBUG Individual - Percentage: 0.07
17:05:54.090 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.091 [main] DEBUG Individual - Percentage: 0.10
17:05:54.091 [main] DEBUG Algorithm - New offspring Individual{fitness=0.71, genes=[2.12, 1.22]}
17:05:54.091 [main] DEBUG Algorithm - Weakest Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:54.091 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.30, genes=[2.40, 1.55]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.21, genes=[2.35, 1.47]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.091 [main] INFO Algorithm - Generation: 70 Fittest: Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.091 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.091 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.091 [main] DEBUG Individual - Percentage: 0.06
17:05:54.091 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.091 [main] DEBUG Individual - Percentage: 0.01
17:05:54.091 [main] DEBUG Algorithm - New offspring Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.091 [main] DEBUG Algorithm - Weakest Individual{fitness=1.21, genes=[2.35, 1.47]}
17:05:54.092 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.30, genes=[2.40, 1.55]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.092 [main] INFO Algorithm - Generation: 71 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.092 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.092 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.092 [main] DEBUG Individual - Percentage: 0.03
17:05:54.092 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.092 [main] DEBUG Individual - Percentage: 0.03
17:05:54.092 [main] DEBUG Algorithm - New offspring Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.092 [main] DEBUG Algorithm - Weakest Individual{fitness=1.30, genes=[2.40, 1.55]}
17:05:54.093 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.093 [main] INFO Algorithm - Generation: 72 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.093 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.093 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.093 [main] DEBUG Individual - Percentage: 0.02
17:05:54.093 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.093 [main] DEBUG Individual - Percentage: 0.09
17:05:54.093 [main] DEBUG Algorithm - New offspring Individual{fitness=0.77, genes=[2.27, 1.26]}
17:05:54.093 [main] DEBUG Algorithm - Weakest Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.104 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.104 [main] INFO Algorithm - Generation: 73 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.104 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.104 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.104 [main] DEBUG Individual - Percentage: 0.06
17:05:54.104 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.104 [main] DEBUG Individual - Percentage: 0.05
17:05:54.104 [main] DEBUG Algorithm - New offspring Individual{fitness=0.91, genes=[2.11, 1.37]}
17:05:54.104 [main] DEBUG Algorithm - Weakest Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.104 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.104 [main] INFO Algorithm - Generation: 74 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.104 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.105 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.105 [main] DEBUG Individual - Percentage: 0.08
17:05:54.105 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.105 [main] DEBUG Individual - Percentage: 0.03
17:05:54.105 [main] DEBUG Algorithm - New offspring Individual{fitness=1.03, genes=[2.05, 1.44]}
17:05:54.105 [main] DEBUG Algorithm - Weakest Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.105 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.38, genes=[2.21, 1.45]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.105 [main] INFO Algorithm - Generation: 75 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.105 [main] INFO Algorithm - Selected Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.105 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.105 [main] DEBUG Individual - Percentage: 0.01
17:05:54.105 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.105 [main] DEBUG Individual - Percentage: 0.01
17:05:54.105 [main] DEBUG Algorithm - New offspring Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.105 [main] DEBUG Algorithm - Weakest Individual{fitness=1.38, genes=[2.21, 1.45]}
17:05:54.106 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.106 [main] INFO Algorithm - Generation: 76 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.106 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.106 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.106 [main] DEBUG Individual - Percentage: 0.07
17:05:54.106 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.106 [main] DEBUG Individual - Percentage: 0.08
17:05:54.106 [main] DEBUG Algorithm - New offspring Individual{fitness=0.78, genes=[2.11, 1.31]}
17:05:54.106 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.107 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.107 [main] INFO Algorithm - Generation: 77 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.107 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.107 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.107 [main] DEBUG Individual - Percentage: 0.07
17:05:54.107 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.107 [main] DEBUG Individual - Percentage: 0.06
17:05:54.107 [main] DEBUG Algorithm - New offspring Individual{fitness=0.38, genes=[2.53, 1.73]}
17:05:54.107 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.107 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.107 [main] INFO Algorithm - Generation: 78 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.107 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.107 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.107 [main] DEBUG Individual - Percentage: 0.01
17:05:54.107 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.107 [main] DEBUG Individual - Percentage: 0.09
17:05:54.107 [main] DEBUG Algorithm - New offspring Individual{fitness=0.83, genes=[2.17, 1.82]}
17:05:54.107 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.108 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.108 [main] INFO Algorithm - Generation: 79 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.108 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.108 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.108 [main] DEBUG Individual - Percentage: 0.04
17:05:54.108 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.108 [main] DEBUG Individual - Percentage: 0.06
17:05:54.108 [main] DEBUG Algorithm - New offspring Individual{fitness=0.71, genes=[2.10, 1.28]}
17:05:54.108 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.108 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.108 [main] INFO Algorithm - Generation: 80 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.108 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.109 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.109 [main] DEBUG Individual - Percentage: 0.03
17:05:54.109 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.109 [main] DEBUG Individual - Percentage: 0.06
17:05:54.109 [main] DEBUG Algorithm - New offspring Individual{fitness=0.61, genes=[2.04, 1.33]}
17:05:54.109 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.109 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.109 [main] INFO Algorithm - Generation: 81 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.109 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.109 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.109 [main] DEBUG Individual - Percentage: 0.08
17:05:54.109 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.109 [main] DEBUG Individual - Percentage: 0.06
17:05:54.109 [main] DEBUG Algorithm - New offspring Individual{fitness=0.78, genes=[2.39, 1.71]}
17:05:54.109 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.110 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.110 [main] INFO Algorithm - Generation: 82 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.110 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.110 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.110 [main] DEBUG Individual - Percentage: 0.03
17:05:54.110 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.110 [main] DEBUG Individual - Percentage: 0.07
17:05:54.110 [main] DEBUG Algorithm - New offspring Individual{fitness=0.90, genes=[2.23, 1.32]}
17:05:54.110 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.110 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.110 [main] INFO Algorithm - Generation: 83 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.111 [main] INFO Algorithm - Selected Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.111 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.111 [main] DEBUG Individual - Percentage: 0.01
17:05:54.111 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.111 [main] DEBUG Individual - Percentage: 0.08
17:05:54.111 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.33, 1.73]}
17:05:54.111 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.111 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.42, genes=[2.36, 1.54]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.111 [main] INFO Algorithm - Generation: 84 Fittest: Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.111 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.111 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.111 [main] DEBUG Individual - Percentage: 0.02
17:05:54.111 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.111 [main] DEBUG Individual - Percentage: 0.03
17:05:54.111 [main] DEBUG Algorithm - New offspring Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.111 [main] DEBUG Algorithm - Weakest Individual{fitness=1.42, genes=[2.36, 1.54]}
17:05:54.112 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.112 [main] INFO Algorithm - Generation: 85 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.112 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.112 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.112 [main] DEBUG Individual - Percentage: 0.06
17:05:54.112 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.112 [main] DEBUG Individual - Percentage: 0.02
17:05:54.112 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[1.93, 1.47]}
17:05:54.112 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.113 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.113 [main] INFO Algorithm - Generation: 86 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.113 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.113 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.113 [main] DEBUG Individual - Percentage: 0.08
17:05:54.113 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.113 [main] DEBUG Individual - Percentage: 0.09
17:05:54.113 [main] DEBUG Algorithm - New offspring Individual{fitness=0.57, genes=[2.06, 1.25]}
17:05:54.113 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.113 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.113 [main] INFO Algorithm - Generation: 87 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.113 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.113 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.114 [main] DEBUG Individual - Percentage: 0.00
17:05:54.114 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.114 [main] DEBUG Individual - Percentage: 0.08
17:05:54.114 [main] DEBUG Algorithm - New offspring Individual{fitness=0.61, genes=[2.33, 1.82]}
17:05:54.114 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.114 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.114 [main] INFO Algorithm - Generation: 88 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.114 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.114 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.114 [main] DEBUG Individual - Percentage: 0.07
17:05:54.114 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.115 [main] DEBUG Individual - Percentage: 0.04
17:05:54.115 [main] DEBUG Algorithm - New offspring Individual{fitness=1.20, genes=[2.36, 1.66]}
17:05:54.115 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.115 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.115 [main] INFO Algorithm - Generation: 89 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.115 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.115 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.115 [main] DEBUG Individual - Percentage: 0.07
17:05:54.115 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.115 [main] DEBUG Individual - Percentage: 0.03
17:05:54.115 [main] DEBUG Algorithm - New offspring Individual{fitness=0.56, genes=[1.90, 1.42]}
17:05:54.117 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.118 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.118 [main] INFO Algorithm - Generation: 90 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.118 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.118 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.118 [main] DEBUG Individual - Percentage: 0.08
17:05:54.118 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.119 [main] DEBUG Individual - Percentage: 0.06
17:05:54.119 [main] DEBUG Algorithm - New offspring Individual{fitness=0.33, genes=[1.96, 1.29]}
17:05:54.120 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.120 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.120 [main] INFO Algorithm - Generation: 91 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.120 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.120 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.120 [main] DEBUG Individual - Percentage: 0.02
17:05:54.120 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.120 [main] DEBUG Individual - Percentage: 0.08
17:05:54.120 [main] DEBUG Algorithm - New offspring Individual{fitness=0.57, genes=[2.06, 1.27]}
17:05:54.120 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.121 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.121 [main] INFO Algorithm - Generation: 92 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.121 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.121 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.121 [main] DEBUG Individual - Percentage: 0.09
17:05:54.121 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.121 [main] DEBUG Individual - Percentage: 0.02
17:05:54.121 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.47, 1.66]}
17:05:54.121 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.121 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.121 [main] INFO Algorithm - Generation: 93 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.122 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.122 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.122 [main] DEBUG Individual - Percentage: 0.07
17:05:54.122 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.122 [main] DEBUG Individual - Percentage: 0.08
17:05:54.122 [main] DEBUG Algorithm - New offspring Individual{fitness=0.18, genes=[1.90, 1.27]}
17:05:54.122 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.122 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.122 [main] INFO Algorithm - Generation: 94 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.122 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.122 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.122 [main] DEBUG Individual - Percentage: 0.10
17:05:54.122 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.123 [main] DEBUG Individual - Percentage: 0.01
17:05:54.123 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.63, 1.61]}
17:05:54.123 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.123 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.123 [main] INFO Algorithm - Generation: 95 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.123 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.123 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.123 [main] DEBUG Individual - Percentage: 0.05
17:05:54.123 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.123 [main] DEBUG Individual - Percentage: 0.10
17:05:54.123 [main] DEBUG Algorithm - New offspring Individual{fitness=0.61, genes=[2.32, 1.90]}
17:05:54.123 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.124 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.124 [main] INFO Algorithm - Generation: 96 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.124 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.124 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.124 [main] DEBUG Individual - Percentage: 0.09
17:05:54.124 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.124 [main] DEBUG Individual - Percentage: 0.09
17:05:54.124 [main] DEBUG Algorithm - New offspring Individual{fitness=0.22, genes=[1.89, 1.30]}
17:05:54.124 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.124 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.124 [main] INFO Algorithm - Generation: 97 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.124 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.124 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.124 [main] DEBUG Individual - Percentage: 0.03
17:05:54.125 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.125 [main] DEBUG Individual - Percentage: 0.05
17:05:54.125 [main] DEBUG Algorithm - New offspring Individual{fitness=0.98, genes=[2.23, 1.36]}
17:05:54.125 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.125 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.125 [main] INFO Algorithm - Generation: 98 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.125 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.125 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.125 [main] DEBUG Individual - Percentage: 0.06
17:05:54.125 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.125 [main] DEBUG Individual - Percentage: 0.07
17:05:54.125 [main] DEBUG Algorithm - New offspring Individual{fitness=0.27, genes=[1.93, 1.30]}
17:05:54.125 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.126 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.126 [main] INFO Algorithm - Generation: 99 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.126 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.126 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.126 [main] DEBUG Individual - Percentage: 0.04
17:05:54.126 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.126 [main] DEBUG Individual - Percentage: 0.10
17:05:54.126 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.10, 1.17]}
17:05:54.126 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.126 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.126 [main] INFO Algorithm - Generation: 100 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.126 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.126 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.126 [main] DEBUG Individual - Percentage: 0.05
17:05:54.126 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.126 [main] DEBUG Individual - Percentage: 0.01
17:05:54.126 [main] DEBUG Algorithm - New offspring Individual{fitness=1.38, genes=[2.00, 1.58]}
17:05:54.126 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.127 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.127 [main] INFO Algorithm - Generation: 101 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.127 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.127 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.127 [main] DEBUG Individual - Percentage: 0.07
17:05:54.127 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.127 [main] DEBUG Individual - Percentage: 0.04
17:05:54.127 [main] DEBUG Algorithm - New offspring Individual{fitness=0.62, genes=[2.54, 1.68]}
17:05:54.127 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.127 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.127 [main] INFO Algorithm - Generation: 102 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.127 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.127 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.128 [main] DEBUG Individual - Percentage: 0.08
17:05:54.129 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.129 [main] DEBUG Individual - Percentage: 0.03
17:05:54.129 [main] DEBUG Algorithm - New offspring Individual{fitness=0.61, genes=[1.86, 1.44]}
17:05:54.129 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.129 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.130 [main] INFO Algorithm - Generation: 103 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.130 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.131 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.134 [main] DEBUG Individual - Percentage: 0.00
17:05:54.134 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.134 [main] DEBUG Individual - Percentage: 0.05
17:05:54.134 [main] DEBUG Algorithm - New offspring Individual{fitness=0.88, genes=[2.32, 1.38]}
17:05:54.134 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.134 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.134 [main] INFO Algorithm - Generation: 104 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.135 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.135 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.135 [main] DEBUG Individual - Percentage: 0.00
17:05:54.135 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.135 [main] DEBUG Individual - Percentage: 0.06
17:05:54.135 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.18, 1.79]}
17:05:54.135 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.135 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.135 [main] INFO Algorithm - Generation: 105 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.136 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.136 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.136 [main] DEBUG Individual - Percentage: 0.03
17:05:54.136 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.136 [main] DEBUG Individual - Percentage: 0.07
17:05:54.136 [main] DEBUG Algorithm - New offspring Individual{fitness=0.90, genes=[2.07, 1.40]}
17:05:54.136 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.136 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.46, genes=[2.29, 1.49]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.137 [main] INFO Algorithm - Generation: 106 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.137 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.137 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.137 [main] DEBUG Individual - Percentage: 0.02
17:05:54.137 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.137 [main] DEBUG Individual - Percentage: 0.03
17:05:54.137 [main] DEBUG Algorithm - New offspring Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.137 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.29, 1.49]}
17:05:54.137 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.137 [main] INFO Algorithm - Generation: 107 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.137 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.138 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.138 [main] DEBUG Individual - Percentage: 0.03
17:05:54.138 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.138 [main] DEBUG Individual - Percentage: 0.10
17:05:54.138 [main] DEBUG Algorithm - New offspring Individual{fitness=0.82, genes=[2.22, 1.24]}
17:05:54.138 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.138 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.138 [main] INFO Algorithm - Generation: 108 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.138 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.138 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.138 [main] DEBUG Individual - Percentage: 0.09
17:05:54.138 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.139 [main] DEBUG Individual - Percentage: 0.06
17:05:54.139 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.41, 1.72]}
17:05:54.139 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.139 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.139 [main] INFO Algorithm - Generation: 109 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.139 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.139 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.139 [main] DEBUG Individual - Percentage: 0.09
17:05:54.139 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.139 [main] DEBUG Individual - Percentage: 0.02
17:05:54.139 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.61, 1.65]}
17:05:54.140 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.140 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.140 [main] INFO Algorithm - Generation: 110 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.140 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.140 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.140 [main] DEBUG Individual - Percentage: 0.06
17:05:54.140 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.140 [main] DEBUG Individual - Percentage: 0.03
17:05:54.140 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.37, 1.71]}
17:05:54.140 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.141 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.141 [main] INFO Algorithm - Generation: 111 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.141 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.141 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.141 [main] DEBUG Individual - Percentage: 0.06
17:05:54.141 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.141 [main] DEBUG Individual - Percentage: 0.06
17:05:54.141 [main] DEBUG Algorithm - New offspring Individual{fitness=0.54, genes=[2.03, 1.30]}
17:05:54.141 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.142 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.142 [main] INFO Algorithm - Generation: 112 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.142 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.142 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.142 [main] DEBUG Individual - Percentage: 0.07
17:05:54.142 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.142 [main] DEBUG Individual - Percentage: 0.08
17:05:54.142 [main] DEBUG Algorithm - New offspring Individual{fitness=0.43, genes=[2.38, 1.85]}
17:05:54.142 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.142 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.36, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.142 [main] INFO Algorithm - Generation: 113 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.142 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.143 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.143 [main] DEBUG Individual - Percentage: 0.04
17:05:54.143 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.143 [main] DEBUG Individual - Percentage: 0.01
17:05:54.143 [main] DEBUG Algorithm - New offspring Individual{fitness=1.46, genes=[2.03, 1.56]}
17:05:54.143 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.36, 1.57]}
17:05:54.143 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.03, 1.56]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.143 [main] INFO Algorithm - Generation: 114 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.143 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.143 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.143 [main] DEBUG Individual - Percentage: 0.04
17:05:54.143 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.143 [main] DEBUG Individual - Percentage: 0.06
17:05:54.143 [main] DEBUG Algorithm - New offspring Individual{fitness=1.13, genes=[2.35, 1.68]}
17:05:54.143 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.03, 1.56]}
17:05:54.144 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.46, genes=[2.03, 1.56]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.144 [main] INFO Algorithm - Generation: 115 Fittest: Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.144 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.144 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.144 [main] DEBUG Individual - Percentage: 0.02
17:05:54.144 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.144 [main] DEBUG Individual - Percentage: 0.02
17:05:54.144 [main] DEBUG Algorithm - New offspring Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.144 [main] DEBUG Algorithm - Weakest Individual{fitness=1.46, genes=[2.03, 1.56]}
17:05:54.144 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.144 [main] INFO Algorithm - Generation: 116 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.144 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.144 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.144 [main] DEBUG Individual - Percentage: 0.07
17:05:54.144 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.144 [main] DEBUG Individual - Percentage: 0.06
17:05:54.144 [main] DEBUG Algorithm - New offspring Individual{fitness=0.47, genes=[2.41, 1.77]}
17:05:54.145 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.145 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.145 [main] INFO Algorithm - Generation: 117 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.145 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.145 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.145 [main] DEBUG Individual - Percentage: 0.03
17:05:54.145 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.145 [main] DEBUG Individual - Percentage: 0.01
17:05:54.145 [main] DEBUG Algorithm - New offspring Individual{fitness=1.32, genes=[2.40, 1.58]}
17:05:54.145 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.146 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.146 [main] INFO Algorithm - Generation: 118 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.146 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.146 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.146 [main] DEBUG Individual - Percentage: 0.07
17:05:54.146 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.146 [main] DEBUG Individual - Percentage: 0.08
17:05:54.146 [main] DEBUG Algorithm - New offspring Individual{fitness=0.35, genes=[2.40, 1.86]}
17:05:54.146 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.146 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.146 [main] INFO Algorithm - Generation: 119 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.146 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.146 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.146 [main] DEBUG Individual - Percentage: 0.01
17:05:54.146 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.146 [main] DEBUG Individual - Percentage: 0.08
17:05:54.146 [main] DEBUG Algorithm - New offspring Individual{fitness=0.58, genes=[2.35, 1.79]}
17:05:54.147 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.147 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.147 [main] INFO Algorithm - Generation: 120 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.147 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.147 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.147 [main] DEBUG Individual - Percentage: 0.04
17:05:54.147 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.147 [main] DEBUG Individual - Percentage: 0.07
17:05:54.147 [main] DEBUG Algorithm - New offspring Individual{fitness=1.02, genes=[2.23, 1.75]}
17:05:54.147 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.148 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.148 [main] INFO Algorithm - Generation: 121 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.148 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.148 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.148 [main] DEBUG Individual - Percentage: 0.09
17:05:54.148 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.148 [main] DEBUG Individual - Percentage: 0.09
17:05:54.148 [main] DEBUG Algorithm - New offspring Individual{fitness=0.54, genes=[2.04, 1.28]}
17:05:54.148 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.148 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.148 [main] INFO Algorithm - Generation: 122 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.148 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.148 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.148 [main] DEBUG Individual - Percentage: 0.05
17:05:54.148 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.149 [main] DEBUG Individual - Percentage: 0.01
17:05:54.149 [main] DEBUG Algorithm - New offspring Individual{fitness=1.16, genes=[2.07, 1.45]}
17:05:54.149 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.149 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.149 [main] INFO Algorithm - Generation: 123 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.149 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.149 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.149 [main] DEBUG Individual - Percentage: 0.08
17:05:54.149 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.149 [main] DEBUG Individual - Percentage: 0.04
17:05:54.149 [main] DEBUG Algorithm - New offspring Individual{fitness=0.78, genes=[1.95, 1.44]}
17:05:54.149 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.150 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.150 [main] INFO Algorithm - Generation: 124 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.150 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.150 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.150 [main] DEBUG Individual - Percentage: 0.07
17:05:54.150 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.150 [main] DEBUG Individual - Percentage: 0.09
17:05:54.150 [main] DEBUG Algorithm - New offspring Individual{fitness=0.68, genes=[2.10, 1.26]}
17:05:54.150 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.150 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.150 [main] INFO Algorithm - Generation: 125 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.151 [main] INFO Algorithm - Selected Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.151 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.151 [main] DEBUG Individual - Percentage: 0.04
17:05:54.151 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.151 [main] DEBUG Individual - Percentage: 0.06
17:05:54.151 [main] DEBUG Algorithm - New offspring Individual{fitness=0.92, genes=[2.20, 1.33]}
17:05:54.151 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.151 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.151 [main] INFO Algorithm - Generation: 126 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.151 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.151 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.151 [main] DEBUG Individual - Percentage: 0.08
17:05:54.151 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.152 [main] DEBUG Individual - Percentage: 0.04
17:05:54.152 [main] DEBUG Algorithm - New offspring Individual{fitness=0.52, genes=[1.97, 1.37]}
17:05:54.152 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.152 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.152 [main] INFO Algorithm - Generation: 127 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.152 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.152 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.152 [main] DEBUG Individual - Percentage: 0.09
17:05:54.152 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.152 [main] DEBUG Individual - Percentage: 0.07
17:05:54.152 [main] DEBUG Algorithm - New offspring Individual{fitness=0.15, genes=[2.60, 1.78]}
17:05:54.152 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.153 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.153 [main] INFO Algorithm - Generation: 128 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.153 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.153 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.153 [main] DEBUG Individual - Percentage: 0.09
17:05:54.153 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.153 [main] DEBUG Individual - Percentage: 0.04
17:05:54.153 [main] DEBUG Algorithm - New offspring Individual{fitness=0.55, genes=[2.62, 1.69]}
17:05:54.153 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.153 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.153 [main] INFO Algorithm - Generation: 129 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.154 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.154 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.154 [main] DEBUG Individual - Percentage: 0.10
17:05:54.154 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.154 [main] DEBUG Individual - Percentage: 0.01
17:05:54.154 [main] DEBUG Algorithm - New offspring Individual{fitness=1.07, genes=[2.51, 1.59]}
17:05:54.154 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.154 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.47, genes=[2.10, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.154 [main] INFO Algorithm - Generation: 130 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.154 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.154 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.154 [main] DEBUG Individual - Percentage: 0.04
17:05:54.154 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.155 [main] DEBUG Individual - Percentage: 0.02
17:05:54.155 [main] DEBUG Algorithm - New offspring Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.155 [main] DEBUG Algorithm - Weakest Individual{fitness=1.47, genes=[2.10, 1.50]}
17:05:54.155 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.155 [main] INFO Algorithm - Generation: 131 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.155 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.155 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.155 [main] DEBUG Individual - Percentage: 0.02
17:05:54.155 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.155 [main] DEBUG Individual - Percentage: 0.03
17:05:54.155 [main] DEBUG Algorithm - New offspring Individual{fitness=1.28, genes=[2.23, 1.43]}
17:05:54.155 [main] DEBUG Algorithm - Weakest Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:54.156 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.156 [main] INFO Algorithm - Generation: 132 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.156 [main] INFO Algorithm - Selected Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.157 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.157 [main] DEBUG Individual - Percentage: 0.00
17:05:54.157 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.157 [main] DEBUG Individual - Percentage: 0.06
17:05:54.157 [main] DEBUG Algorithm - New offspring Individual{fitness=0.74, genes=[2.31, 1.32]}
17:05:54.157 [main] DEBUG Algorithm - Weakest Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:54.161 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.49, genes=[2.14, 1.48]}]}
17:05:54.161 [main] INFO Algorithm - Generation: 133 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.161 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.161 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.161 [main] DEBUG Individual - Percentage: 0.03
17:05:54.161 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.161 [main] DEBUG Individual - Percentage: 0.00
17:05:54.161 [main] DEBUG Algorithm - New offspring Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.161 [main] DEBUG Algorithm - Weakest Individual{fitness=1.49, genes=[2.14, 1.48]}
17:05:54.161 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.162 [main] INFO Algorithm - Generation: 134 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.162 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.162 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.162 [main] DEBUG Individual - Percentage: 0.07
17:05:54.162 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.162 [main] DEBUG Individual - Percentage: 0.06
17:05:54.162 [main] DEBUG Algorithm - New offspring Individual{fitness=0.93, genes=[2.11, 1.38]}
17:05:54.162 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.162 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.162 [main] INFO Algorithm - Generation: 135 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.162 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.162 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.162 [main] DEBUG Individual - Percentage: 0.01
17:05:54.162 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.162 [main] DEBUG Individual - Percentage: 0.06
17:05:54.162 [main] DEBUG Algorithm - New offspring Individual{fitness=0.75, genes=[2.34, 1.75]}
17:05:54.162 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.163 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.163 [main] INFO Algorithm - Generation: 136 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.163 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.163 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.163 [main] DEBUG Individual - Percentage: 0.05
17:05:54.163 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.163 [main] DEBUG Individual - Percentage: 0.04
17:05:54.163 [main] DEBUG Algorithm - New offspring Individual{fitness=1.37, genes=[2.17, 1.45]}
17:05:54.163 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.164 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.164 [main] INFO Algorithm - Generation: 137 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.164 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.164 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.164 [main] DEBUG Individual - Percentage: 0.02
17:05:54.164 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.164 [main] DEBUG Individual - Percentage: 0.03
17:05:54.164 [main] DEBUG Algorithm - New offspring Individual{fitness=1.36, genes=[2.27, 1.46]}
17:05:54.164 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.164 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.164 [main] INFO Algorithm - Generation: 138 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.164 [main] INFO Algorithm - Selected Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.164 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.164 [main] DEBUG Individual - Percentage: 0.08
17:05:54.164 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.164 [main] DEBUG Individual - Percentage: 0.10
17:05:54.164 [main] DEBUG Algorithm - New offspring Individual{fitness=0.29, genes=[1.97, 1.18]}
17:05:54.164 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.165 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.165 [main] INFO Algorithm - Generation: 139 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.165 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.165 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.165 [main] DEBUG Individual - Percentage: 0.01
17:05:54.165 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.165 [main] DEBUG Individual - Percentage: 0.02
17:05:54.165 [main] DEBUG Algorithm - New offspring Individual{fitness=1.41, genes=[2.34, 1.62]}
17:05:54.165 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.166 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.166 [main] INFO Algorithm - Generation: 140 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.166 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.166 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.166 [main] DEBUG Individual - Percentage: 0.01
17:05:54.166 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.166 [main] DEBUG Individual - Percentage: 0.06
17:05:54.166 [main] DEBUG Algorithm - New offspring Individual{fitness=1.12, genes=[2.13, 1.41]}
17:05:54.166 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.166 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.166 [main] INFO Algorithm - Generation: 141 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.166 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.166 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.166 [main] DEBUG Individual - Percentage: 0.09
17:05:54.166 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.166 [main] DEBUG Individual - Percentage: 0.08
17:05:54.166 [main] DEBUG Algorithm - New offspring Individual{fitness=0.20, genes=[2.48, 1.81]}
17:05:54.166 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.167 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.167 [main] INFO Algorithm - Generation: 142 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.167 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.167 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.167 [main] DEBUG Individual - Percentage: 0.05
17:05:54.167 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.167 [main] DEBUG Individual - Percentage: 0.05
17:05:54.167 [main] DEBUG Algorithm - New offspring Individual{fitness=1.26, genes=[2.27, 1.69]}
17:05:54.167 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.168 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.168 [main] INFO Algorithm - Generation: 143 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.168 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.168 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.168 [main] DEBUG Individual - Percentage: 0.00
17:05:54.168 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.168 [main] DEBUG Individual - Percentage: 0.02
17:05:54.168 [main] DEBUG Algorithm - New offspring Individual{fitness=1.41, genes=[2.17, 1.67]}
17:05:54.168 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.168 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.168 [main] INFO Algorithm - Generation: 144 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.168 [main] INFO Algorithm - Selected Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.168 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.168 [main] DEBUG Individual - Percentage: 0.06
17:05:54.168 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.168 [main] DEBUG Individual - Percentage: 0.10
17:05:54.168 [main] DEBUG Algorithm - New offspring Individual{fitness=0.47, genes=[2.38, 1.80]}
17:05:54.169 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.169 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.169 [main] INFO Algorithm - Generation: 145 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.169 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.169 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.169 [main] DEBUG Individual - Percentage: 0.07
17:05:54.169 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.169 [main] DEBUG Individual - Percentage: 0.05
17:05:54.169 [main] DEBUG Algorithm - New offspring Individual{fitness=0.66, genes=[2.41, 1.72]}
17:05:54.169 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.169 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.169 [main] INFO Algorithm - Generation: 146 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.170 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.170 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.170 [main] DEBUG Individual - Percentage: 0.00
17:05:54.170 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.170 [main] DEBUG Individual - Percentage: 0.06
17:05:54.170 [main] DEBUG Algorithm - New offspring Individual{fitness=0.99, genes=[2.21, 1.76]}
17:05:54.170 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.176 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.176 [main] INFO Algorithm - Generation: 147 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.176 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.177 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.177 [main] DEBUG Individual - Percentage: 0.06
17:05:54.177 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.177 [main] DEBUG Individual - Percentage: 0.09
17:05:54.177 [main] DEBUG Algorithm - New offspring Individual{fitness=0.08, genes=[2.52, 1.86]}
17:05:54.177 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.177 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.50, genes=[2.32, 1.52]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.177 [main] INFO Algorithm - Generation: 148 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.177 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.177 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.177 [main] DEBUG Individual - Percentage: 0.05
17:05:54.177 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.177 [main] DEBUG Individual - Percentage: 0.01
17:05:54.177 [main] DEBUG Algorithm - New offspring Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.177 [main] DEBUG Algorithm - Weakest Individual{fitness=1.50, genes=[2.32, 1.52]}
17:05:54.178 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.178 [main] INFO Algorithm - Generation: 149 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.178 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.178 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.178 [main] DEBUG Individual - Percentage: 0.10
17:05:54.178 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.178 [main] DEBUG Individual - Percentage: 0.00
17:05:54.178 [main] DEBUG Algorithm - New offspring Individual{fitness=0.93, genes=[1.81, 1.51]}
17:05:54.178 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.178 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.178 [main] INFO Algorithm - Generation: 150 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.178 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.178 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.178 [main] DEBUG Individual - Percentage: 0.03
17:05:54.178 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.178 [main] DEBUG Individual - Percentage: 0.00
17:05:54.178 [main] DEBUG Algorithm - New offspring Individual{fitness=1.26, genes=[2.43, 1.57]}
17:05:54.179 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.179 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.179 [main] INFO Algorithm - Generation: 151 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.179 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.179 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.179 [main] DEBUG Individual - Percentage: 0.07
17:05:54.179 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.179 [main] DEBUG Individual - Percentage: 0.09
17:05:54.179 [main] DEBUG Algorithm - New offspring Individual{fitness=0.37, genes=[1.97, 1.29]}
17:05:54.179 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.180 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.180 [main] INFO Algorithm - Generation: 152 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.180 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.180 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.180 [main] DEBUG Individual - Percentage: 0.08
17:05:54.180 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.180 [main] DEBUG Individual - Percentage: 0.06
17:05:54.180 [main] DEBUG Algorithm - New offspring Individual{fitness=0.51, genes=[1.95, 1.38]}
17:05:54.180 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.180 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.180 [main] INFO Algorithm - Generation: 153 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.180 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.181 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.181 [main] DEBUG Individual - Percentage: 0.09
17:05:54.181 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.181 [main] DEBUG Individual - Percentage: 0.06
17:05:54.181 [main] DEBUG Algorithm - New offspring Individual{fitness=0.71, genes=[1.95, 1.43]}
17:05:54.181 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.181 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.181 [main] INFO Algorithm - Generation: 154 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.181 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.181 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.181 [main] DEBUG Individual - Percentage: 0.08
17:05:54.181 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.181 [main] DEBUG Individual - Percentage: 0.06
17:05:54.181 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.42, 1.72]}
17:05:54.181 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.182 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.182 [main] INFO Algorithm - Generation: 155 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.182 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.182 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.182 [main] DEBUG Individual - Percentage: 0.01
17:05:54.182 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.182 [main] DEBUG Individual - Percentage: 0.09
17:05:54.182 [main] DEBUG Algorithm - New offspring Individual{fitness=0.81, genes=[2.21, 1.87]}
17:05:54.182 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.183 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.183 [main] INFO Algorithm - Generation: 156 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.183 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.183 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.183 [main] DEBUG Individual - Percentage: 0.04
17:05:54.183 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.183 [main] DEBUG Individual - Percentage: 0.05
17:05:54.183 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[2.29, 1.77]}
17:05:54.183 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.185 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.186 [main] INFO Algorithm - Generation: 157 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.186 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.187 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.188 [main] DEBUG Individual - Percentage: 0.03
17:05:54.202 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.203 [main] DEBUG Individual - Percentage: 0.10
17:05:54.203 [main] DEBUG Algorithm - New offspring Individual{fitness=0.83, genes=[2.23, 1.82]}
17:05:54.203 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.204 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.205 [main] INFO Algorithm - Generation: 158 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.205 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.205 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.205 [main] DEBUG Individual - Percentage: 0.04
17:05:54.205 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.205 [main] DEBUG Individual - Percentage: 0.07
17:05:54.205 [main] DEBUG Algorithm - New offspring Individual{fitness=0.56, genes=[2.04, 1.30]}
17:05:54.205 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.206 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.206 [main] INFO Algorithm - Generation: 159 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.206 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.206 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.206 [main] DEBUG Individual - Percentage: 0.04
17:05:54.206 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.206 [main] DEBUG Individual - Percentage: 0.09
17:05:54.206 [main] DEBUG Algorithm - New offspring Individual{fitness=0.61, genes=[2.05, 1.31]}
17:05:54.206 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.207 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.207 [main] INFO Algorithm - Generation: 160 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.207 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.207 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.207 [main] DEBUG Individual - Percentage: 0.06
17:05:54.207 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.207 [main] DEBUG Individual - Percentage: 0.01
17:05:54.207 [main] DEBUG Algorithm - New offspring Individual{fitness=1.35, genes=[2.00, 1.55]}
17:05:54.207 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.207 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.207 [main] INFO Algorithm - Generation: 161 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.208 [main] INFO Algorithm - Selected Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.208 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.208 [main] DEBUG Individual - Percentage: 0.04
17:05:54.208 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.208 [main] DEBUG Individual - Percentage: 0.01
17:05:54.208 [main] DEBUG Algorithm - New offspring Individual{fitness=1.25, genes=[2.05, 1.48]}
17:05:54.208 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.216 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.216 [main] INFO Algorithm - Generation: 162 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.216 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.216 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.216 [main] DEBUG Individual - Percentage: 0.07
17:05:54.218 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.219 [main] DEBUG Individual - Percentage: 0.05
17:05:54.219 [main] DEBUG Algorithm - New offspring Individual{fitness=0.69, genes=[1.99, 1.40]}
17:05:54.220 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.220 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.220 [main] INFO Algorithm - Generation: 163 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.220 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.220 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.220 [main] DEBUG Individual - Percentage: 0.10
17:05:54.220 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.220 [main] DEBUG Individual - Percentage: 0.01
17:05:54.220 [main] DEBUG Algorithm - New offspring Individual{fitness=1.04, genes=[2.47, 1.62]}
17:05:54.220 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.221 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.221 [main] INFO Algorithm - Generation: 164 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.221 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.222 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.223 [main] DEBUG Individual - Percentage: 0.01
17:05:54.224 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.226 [main] DEBUG Individual - Percentage: 0.09
17:05:54.226 [main] DEBUG Algorithm - New offspring Individual{fitness=0.83, genes=[2.17, 1.27]}
17:05:54.227 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.229 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.229 [main] INFO Algorithm - Generation: 165 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.229 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.230 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.230 [main] DEBUG Individual - Percentage: 0.02
17:05:54.230 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.230 [main] DEBUG Individual - Percentage: 0.07
17:05:54.230 [main] DEBUG Algorithm - New offspring Individual{fitness=1.09, genes=[2.19, 1.39]}
17:05:54.230 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.231 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.231 [main] INFO Algorithm - Generation: 166 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.232 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.232 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.232 [main] DEBUG Individual - Percentage: 0.05
17:05:54.232 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.232 [main] DEBUG Individual - Percentage: 0.07
17:05:54.232 [main] DEBUG Algorithm - New offspring Individual{fitness=0.35, genes=[2.41, 1.82]}
17:05:54.232 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.233 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.233 [main] INFO Algorithm - Generation: 167 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.233 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.233 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.233 [main] DEBUG Individual - Percentage: 0.06
17:05:54.233 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.233 [main] DEBUG Individual - Percentage: 0.04
17:05:54.233 [main] DEBUG Algorithm - New offspring Individual{fitness=1.12, genes=[1.99, 1.49]}
17:05:54.233 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.233 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.233 [main] INFO Algorithm - Generation: 168 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.233 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.233 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.233 [main] DEBUG Individual - Percentage: 0.02
17:05:54.233 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.233 [main] DEBUG Individual - Percentage: 0.01
17:05:54.233 [main] DEBUG Algorithm - New offspring Individual{fitness=1.48, genes=[2.32, 1.63]}
17:05:54.234 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.234 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.234 [main] INFO Algorithm - Generation: 169 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.234 [main] INFO Algorithm - Selected Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.234 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.234 [main] DEBUG Individual - Percentage: 0.03
17:05:54.234 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.234 [main] DEBUG Individual - Percentage: 0.08
17:05:54.234 [main] DEBUG Algorithm - New offspring Individual{fitness=0.70, genes=[2.11, 1.25]}
17:05:54.234 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.234 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.234 [main] INFO Algorithm - Generation: 170 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.235 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.235 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.235 [main] DEBUG Individual - Percentage: 0.10
17:05:54.235 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.235 [main] DEBUG Individual - Percentage: 0.01
17:05:54.235 [main] DEBUG Algorithm - New offspring Individual{fitness=0.95, genes=[1.87, 1.50]}
17:05:54.235 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.235 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.235 [main] INFO Algorithm - Generation: 171 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.235 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.236 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.236 [main] DEBUG Individual - Percentage: 0.00
17:05:54.236 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.236 [main] DEBUG Individual - Percentage: 0.10
17:05:54.236 [main] DEBUG Algorithm - New offspring Individual{fitness=0.85, genes=[2.24, 1.30]}
17:05:54.236 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.237 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.237 [main] INFO Algorithm - Generation: 172 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.237 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.237 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.237 [main] DEBUG Individual - Percentage: 0.06
17:05:54.238 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.238 [main] DEBUG Individual - Percentage: 0.09
17:05:54.238 [main] DEBUG Algorithm - New offspring Individual{fitness=0.12, genes=[2.50, 1.85]}
17:05:54.238 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.240 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.53, genes=[2.22, 1.48]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.241 [main] INFO Algorithm - Generation: 173 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.242 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.243 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.243 [main] DEBUG Individual - Percentage: 0.03
17:05:54.243 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.243 [main] DEBUG Individual - Percentage: 0.01
17:05:54.243 [main] DEBUG Algorithm - New offspring Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.243 [main] DEBUG Algorithm - Weakest Individual{fitness=1.53, genes=[2.22, 1.48]}
17:05:54.245 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.246 [main] INFO Algorithm - Generation: 174 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.246 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.246 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.247 [main] DEBUG Individual - Percentage: 0.09
17:05:54.247 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.247 [main] DEBUG Individual - Percentage: 0.05
17:05:54.247 [main] DEBUG Algorithm - New offspring Individual{fitness=0.46, genes=[2.45, 1.74]}
17:05:54.247 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.247 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.248 [main] INFO Algorithm - Generation: 175 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.248 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.248 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.248 [main] DEBUG Individual - Percentage: 0.10
17:05:54.248 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.248 [main] DEBUG Individual - Percentage: 0.04
17:05:54.248 [main] DEBUG Algorithm - New offspring Individual{fitness=0.39, genes=[1.86, 1.39]}
17:05:54.248 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.248 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.248 [main] INFO Algorithm - Generation: 176 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.248 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.248 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.248 [main] DEBUG Individual - Percentage: 0.07
17:05:54.248 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.248 [main] DEBUG Individual - Percentage: 0.10
17:05:54.248 [main] DEBUG Algorithm - New offspring Individual{fitness=0.14, genes=[1.89, 1.21]}
17:05:54.248 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.249 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.249 [main] INFO Algorithm - Generation: 177 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.249 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.249 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.249 [main] DEBUG Individual - Percentage: 0.04
17:05:54.249 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.249 [main] DEBUG Individual - Percentage: 0.06
17:05:54.249 [main] DEBUG Algorithm - New offspring Individual{fitness=0.60, genes=[2.04, 1.33]}
17:05:54.249 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.273 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.273 [main] INFO Algorithm - Generation: 178 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.273 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.273 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.273 [main] DEBUG Individual - Percentage: 0.07
17:05:54.273 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.273 [main] DEBUG Individual - Percentage: 0.03
17:05:54.273 [main] DEBUG Algorithm - New offspring Individual{fitness=0.69, genes=[2.46, 1.69]}
17:05:54.274 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.274 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.274 [main] INFO Algorithm - Generation: 179 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.274 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.274 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.274 [main] DEBUG Individual - Percentage: 0.07
17:05:54.274 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.274 [main] DEBUG Individual - Percentage: 0.05
17:05:54.274 [main] DEBUG Algorithm - New offspring Individual{fitness=1.10, genes=[2.03, 1.46]}
17:05:54.274 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.287 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.287 [main] INFO Algorithm - Generation: 180 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.287 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.287 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.287 [main] DEBUG Individual - Percentage: 0.08
17:05:54.287 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.287 [main] DEBUG Individual - Percentage: 0.04
17:05:54.287 [main] DEBUG Algorithm - New offspring Individual{fitness=0.82, genes=[1.93, 1.46]}
17:05:54.287 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.288 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.288 [main] INFO Algorithm - Generation: 181 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.288 [main] INFO Algorithm - Selected Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.288 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.288 [main] DEBUG Individual - Percentage: 0.02
17:05:54.288 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.288 [main] DEBUG Individual - Percentage: 0.05
17:05:54.288 [main] DEBUG Algorithm - New offspring Individual{fitness=1.06, genes=[2.25, 1.39]}
17:05:54.288 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.288 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.288 [main] INFO Algorithm - Generation: 182 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.288 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.288 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.288 [main] DEBUG Individual - Percentage: 0.01
17:05:54.288 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.288 [main] DEBUG Individual - Percentage: 0.09
17:05:54.288 [main] DEBUG Algorithm - New offspring Individual{fitness=0.77, genes=[2.13, 1.28]}
17:05:54.288 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.289 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.289 [main] INFO Algorithm - Generation: 183 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.289 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.289 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.289 [main] DEBUG Individual - Percentage: 0.06
17:05:54.289 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.289 [main] DEBUG Individual - Percentage: 0.00
17:05:54.289 [main] DEBUG Algorithm - New offspring Individual{fitness=1.52, genes=[2.07, 1.61]}
17:05:54.289 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.289 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.289 [main] INFO Algorithm - Generation: 184 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.289 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.290 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.290 [main] DEBUG Individual - Percentage: 0.09
17:05:54.290 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.290 [main] DEBUG Individual - Percentage: 0.08
17:05:54.290 [main] DEBUG Algorithm - New offspring Individual{fitness=0.52, genes=[2.40, 1.76]}
17:05:54.290 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.290 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.290 [main] INFO Algorithm - Generation: 185 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.290 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.290 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.290 [main] DEBUG Individual - Percentage: 0.06
17:05:54.290 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.290 [main] DEBUG Individual - Percentage: 0.05
17:05:54.290 [main] DEBUG Algorithm - New offspring Individual{fitness=0.42, genes=[2.43, 1.76]}
17:05:54.290 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.291 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.291 [main] INFO Algorithm - Generation: 186 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.291 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.291 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.293 [main] DEBUG Individual - Percentage: 0.07
17:05:54.294 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.295 [main] DEBUG Individual - Percentage: 0.06
17:05:54.295 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.02, 1.42]}
17:05:54.296 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.297 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.297 [main] INFO Algorithm - Generation: 187 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.297 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.297 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.297 [main] DEBUG Individual - Percentage: 0.06
17:05:54.297 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.297 [main] DEBUG Individual - Percentage: 0.00
17:05:54.297 [main] DEBUG Algorithm - New offspring Individual{fitness=1.36, genes=[2.00, 1.55]}
17:05:54.297 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.298 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.298 [main] INFO Algorithm - Generation: 188 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.298 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.298 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.298 [main] DEBUG Individual - Percentage: 0.07
17:05:54.298 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.298 [main] DEBUG Individual - Percentage: 0.03
17:05:54.298 [main] DEBUG Algorithm - New offspring Individual{fitness=0.94, genes=[1.92, 1.48]}
17:05:54.298 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.298 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.298 [main] INFO Algorithm - Generation: 189 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.298 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.298 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.298 [main] DEBUG Individual - Percentage: 0.01
17:05:54.298 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.298 [main] DEBUG Individual - Percentage: 0.08
17:05:54.298 [main] DEBUG Algorithm - New offspring Individual{fitness=1.01, genes=[2.22, 1.36]}
17:05:54.298 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.299 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.299 [main] INFO Algorithm - Generation: 190 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.299 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.299 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.299 [main] DEBUG Individual - Percentage: 0.00
17:05:54.299 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.299 [main] DEBUG Individual - Percentage: 0.06
17:05:54.299 [main] DEBUG Algorithm - New offspring Individual{fitness=1.00, genes=[2.15, 1.37]}
17:05:54.299 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.301 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.301 [main] INFO Algorithm - Generation: 191 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.301 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.301 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.301 [main] DEBUG Individual - Percentage: 0.02
17:05:54.301 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.301 [main] DEBUG Individual - Percentage: 0.10
17:05:54.301 [main] DEBUG Algorithm - New offspring Individual{fitness=0.60, genes=[2.32, 1.92]}
17:05:54.301 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.302 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.302 [main] INFO Algorithm - Generation: 192 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.302 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.302 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.302 [main] DEBUG Individual - Percentage: 0.06
17:05:54.302 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.302 [main] DEBUG Individual - Percentage: 0.04
17:05:54.302 [main] DEBUG Algorithm - New offspring Individual{fitness=0.50, genes=[2.43, 1.74]}
17:05:54.302 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.302 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.302 [main] INFO Algorithm - Generation: 193 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.302 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.302 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.302 [main] DEBUG Individual - Percentage: 0.06
17:05:54.302 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.302 [main] DEBUG Individual - Percentage: 0.08
17:05:54.302 [main] DEBUG Algorithm - New offspring Individual{fitness=0.62, genes=[2.35, 1.79]}
17:05:54.302 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.303 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.303 [main] INFO Algorithm - Generation: 194 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.303 [main] INFO Algorithm - Selected Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.303 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.303 [main] DEBUG Individual - Percentage: 0.04
17:05:54.303 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.303 [main] DEBUG Individual - Percentage: 0.05
17:05:54.303 [main] DEBUG Algorithm - New offspring Individual{fitness=0.72, genes=[2.07, 1.34]}
17:05:54.303 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.303 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.303 [main] INFO Algorithm - Generation: 195 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.303 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.303 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.303 [main] DEBUG Individual - Percentage: 0.02
17:05:54.303 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.303 [main] DEBUG Individual - Percentage: 0.02
17:05:54.303 [main] DEBUG Algorithm - New offspring Individual{fitness=1.51, genes=[2.08, 1.52]}
17:05:54.303 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.304 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.304 [main] INFO Algorithm - Generation: 196 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.304 [main] INFO Algorithm - Selected Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.304 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.304 [main] DEBUG Individual - Percentage: 0.07
17:05:54.304 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.304 [main] DEBUG Individual - Percentage: 0.07
17:05:54.304 [main] DEBUG Algorithm - New offspring Individual{fitness=0.59, genes=[2.42, 1.73]}
17:05:54.304 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.304 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.304 [main] INFO Algorithm - Generation: 197 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.304 [main] INFO Algorithm - Selected Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.304 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.304 [main] DEBUG Individual - Percentage: 0.05
17:05:54.304 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.304 [main] DEBUG Individual - Percentage: 0.09
17:05:54.304 [main] DEBUG Algorithm - New offspring Individual{fitness=0.27, genes=[1.95, 1.24]}
17:05:54.304 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.305 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.55, genes=[2.31, 1.53]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.305 [main] INFO Algorithm - Generation: 198 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.305 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.305 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.305 [main] DEBUG Individual - Percentage: 0.01
17:05:54.305 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.305 [main] DEBUG Individual - Percentage: 0.03
17:05:54.305 [main] DEBUG Algorithm - New offspring Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.305 [main] DEBUG Algorithm - Weakest Individual{fitness=1.55, genes=[2.31, 1.53]}
17:05:54.306 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.306 [main] INFO Algorithm - Generation: 199 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.306 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.306 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.306 [main] DEBUG Individual - Percentage: 0.01
17:05:54.306 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.306 [main] DEBUG Individual - Percentage: 0.04
17:05:54.306 [main] DEBUG Algorithm - New offspring Individual{fitness=1.15, genes=[2.15, 1.41]}
17:05:54.306 [main] DEBUG Algorithm - Weakest Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.306 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.306 [main] INFO Algorithm - Generation: 200 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.306 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.307 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.307 [main] DEBUG Individual - Percentage: 0.05
17:05:54.307 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.307 [main] DEBUG Individual - Percentage: 0.08
17:05:54.307 [main] DEBUG Algorithm - New offspring Individual{fitness=0.81, genes=[2.10, 1.35]}
17:05:54.307 [main] DEBUG Algorithm - Weakest Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.307 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.307 [main] INFO Algorithm - Generation: 201 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.307 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.307 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.307 [main] DEBUG Individual - Percentage: 0.04
17:05:54.307 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.307 [main] DEBUG Individual - Percentage: 0.08
17:05:54.307 [main] DEBUG Algorithm - New offspring Individual{fitness=0.59, genes=[2.34, 1.82]}
17:05:54.307 [main] DEBUG Algorithm - Weakest Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.308 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.308 [main] INFO Algorithm - Generation: 202 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.308 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.308 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.308 [main] DEBUG Individual - Percentage: 0.07
17:05:54.308 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.308 [main] DEBUG Individual - Percentage: 0.07
17:05:54.308 [main] DEBUG Algorithm - New offspring Individual{fitness=0.52, genes=[1.99, 1.35]}
17:05:54.308 [main] DEBUG Algorithm - Weakest Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.308 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.308 [main] INFO Algorithm - Generation: 203 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.308 [main] INFO Algorithm - Selected Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.308 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.308 [main] DEBUG Individual - Percentage: 0.09
17:05:54.308 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.308 [main] DEBUG Individual - Percentage: 0.08
17:05:54.308 [main] DEBUG Algorithm - New offspring Individual{fitness=0.55, genes=[2.03, 1.32]}
17:05:54.308 [main] DEBUG Algorithm - Weakest Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.310 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.58, genes=[2.32, 1.55]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.311 [main] INFO Algorithm - Generation: 204 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.311 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.311 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.311 [main] DEBUG Individual - Percentage: 0.02
17:05:54.311 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.311 [main] DEBUG Individual - Percentage: 0.01
17:05:54.311 [main] DEBUG Algorithm - New offspring Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.311 [main] DEBUG Algorithm - Weakest Individual{fitness=1.58, genes=[2.32, 1.55]}
17:05:54.311 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.311 [main] INFO Algorithm - Generation: 205 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.311 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.311 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.311 [main] DEBUG Individual - Percentage: 0.05
17:05:54.311 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.311 [main] DEBUG Individual - Percentage: 0.06
17:05:54.311 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.02, 1.43]}
17:05:54.311 [main] DEBUG Algorithm - Weakest Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.312 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.312 [main] INFO Algorithm - Generation: 206 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.312 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.312 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.312 [main] DEBUG Individual - Percentage: 0.03
17:05:54.312 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.312 [main] DEBUG Individual - Percentage: 0.05
17:05:54.312 [main] DEBUG Algorithm - New offspring Individual{fitness=1.24, genes=[2.08, 1.46]}
17:05:54.312 [main] DEBUG Algorithm - Weakest Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.312 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.312 [main] INFO Algorithm - Generation: 207 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.312 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.312 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.312 [main] DEBUG Individual - Percentage: 0.01
17:05:54.312 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.312 [main] DEBUG Individual - Percentage: 0.07
17:05:54.312 [main] DEBUG Algorithm - New offspring Individual{fitness=0.99, genes=[2.18, 1.36]}
17:05:54.312 [main] DEBUG Algorithm - Weakest Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.313 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.313 [main] INFO Algorithm - Generation: 208 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.313 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.313 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.313 [main] DEBUG Individual - Percentage: 0.09
17:05:54.313 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.313 [main] DEBUG Individual - Percentage: 0.04
17:05:54.313 [main] DEBUG Algorithm - New offspring Individual{fitness=0.60, genes=[2.49, 1.70]}
17:05:54.313 [main] DEBUG Algorithm - Weakest Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.313 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.59, genes=[2.32, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.313 [main] INFO Algorithm - Generation: 209 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.313 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.313 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.313 [main] DEBUG Individual - Percentage: 0.02
17:05:54.313 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.313 [main] DEBUG Individual - Percentage: 0.00
17:05:54.313 [main] DEBUG Algorithm - New offspring Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.313 [main] DEBUG Algorithm - Weakest Individual{fitness=1.59, genes=[2.32, 1.57]}
17:05:54.314 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.314 [main] INFO Algorithm - Generation: 210 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.314 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.314 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.314 [main] DEBUG Individual - Percentage: 0.04
17:05:54.314 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.314 [main] DEBUG Individual - Percentage: 0.07
17:05:54.314 [main] DEBUG Algorithm - New offspring Individual{fitness=0.62, genes=[2.02, 1.35]}
17:05:54.314 [main] DEBUG Algorithm - Weakest Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.314 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.61, genes=[2.12, 1.52]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.314 [main] INFO Algorithm - Generation: 211 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.314 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.314 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.314 [main] DEBUG Individual - Percentage: 0.02
17:05:54.315 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.315 [main] DEBUG Individual - Percentage: 0.01
17:05:54.315 [main] DEBUG Algorithm - New offspring Individual{fitness=1.62, genes=[2.22, 1.64]}
17:05:54.315 [main] DEBUG Algorithm - Weakest Individual{fitness=1.61, genes=[2.12, 1.52]}
17:05:54.315 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.62, genes=[2.22, 1.64]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.315 [main] INFO Algorithm - Generation: 212 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.315 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.315 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.315 [main] DEBUG Individual - Percentage: 0.01
17:05:54.315 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.315 [main] DEBUG Individual - Percentage: 0.04
17:05:54.315 [main] DEBUG Algorithm - New offspring Individual{fitness=1.30, genes=[2.11, 1.46]}
17:05:54.315 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.22, 1.64]}
17:05:54.319 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.62, genes=[2.22, 1.64]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.319 [main] INFO Algorithm - Generation: 213 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.319 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.319 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.319 [main] DEBUG Individual - Percentage: 0.05
17:05:54.320 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.320 [main] DEBUG Individual - Percentage: 0.01
17:05:54.320 [main] DEBUG Algorithm - New offspring Individual{fitness=1.43, genes=[2.37, 1.59]}
17:05:54.320 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.22, 1.64]}
17:05:54.320 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.62, genes=[2.22, 1.64]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.320 [main] INFO Algorithm - Generation: 214 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.320 [main] INFO Algorithm - Selected Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.320 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.320 [main] DEBUG Individual - Percentage: 0.01
17:05:54.320 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.320 [main] DEBUG Individual - Percentage: 0.02
17:05:54.320 [main] DEBUG Algorithm - New offspring Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.320 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.22, 1.64]}
17:05:54.321 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.321 [main] INFO Algorithm - Generation: 215 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.321 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.321 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.321 [main] DEBUG Individual - Percentage: 0.02
17:05:54.321 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.321 [main] DEBUG Individual - Percentage: 0.07
17:05:54.321 [main] DEBUG Algorithm - New offspring Individual{fitness=1.04, genes=[2.12, 1.40]}
17:05:54.321 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.321 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.321 [main] INFO Algorithm - Generation: 216 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.321 [main] INFO Algorithm - Selected Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.321 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.321 [main] DEBUG Individual - Percentage: 0.05
17:05:54.321 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.321 [main] DEBUG Individual - Percentage: 0.06
17:05:54.321 [main] DEBUG Algorithm - New offspring Individual{fitness=1.03, genes=[2.06, 1.43]}
17:05:54.321 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.322 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.322 [main] INFO Algorithm - Generation: 217 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.322 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.322 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.322 [main] DEBUG Individual - Percentage: 0.02
17:05:54.322 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.322 [main] DEBUG Individual - Percentage: 0.05
17:05:54.322 [main] DEBUG Algorithm - New offspring Individual{fitness=1.15, genes=[2.22, 1.72]}
17:05:54.322 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.322 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.322 [main] INFO Algorithm - Generation: 218 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.322 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.322 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.322 [main] DEBUG Individual - Percentage: 0.09
17:05:54.322 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.323 [main] DEBUG Individual - Percentage: 0.05
17:05:54.323 [main] DEBUG Algorithm - New offspring Individual{fitness=0.77, genes=[1.90, 1.46]}
17:05:54.323 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.323 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.323 [main] INFO Algorithm - Generation: 219 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.323 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.323 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.323 [main] DEBUG Individual - Percentage: 0.02
17:05:54.323 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.323 [main] DEBUG Individual - Percentage: 0.03
17:05:54.323 [main] DEBUG Algorithm - New offspring Individual{fitness=1.58, genes=[2.11, 1.52]}
17:05:54.323 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.323 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.323 [main] INFO Algorithm - Generation: 220 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.324 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.324 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.325 [main] DEBUG Individual - Percentage: 0.09
17:05:54.325 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.325 [main] DEBUG Individual - Percentage: 0.01
17:05:54.325 [main] DEBUG Algorithm - New offspring Individual{fitness=1.28, genes=[1.97, 1.58]}
17:05:54.325 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.325 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.326 [main] INFO Algorithm - Generation: 221 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.326 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.326 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.326 [main] DEBUG Individual - Percentage: 0.07
17:05:54.326 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.326 [main] DEBUG Individual - Percentage: 0.08
17:05:54.326 [main] DEBUG Algorithm - New offspring Individual{fitness=0.46, genes=[1.99, 1.32]}
17:05:54.326 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.326 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.326 [main] INFO Algorithm - Generation: 222 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.326 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.326 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.326 [main] DEBUG Individual - Percentage: 0.00
17:05:54.326 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.326 [main] DEBUG Individual - Percentage: 0.04
17:05:54.326 [main] DEBUG Algorithm - New offspring Individual{fitness=1.32, genes=[2.16, 1.69]}
17:05:54.326 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.327 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.327 [main] INFO Algorithm - Generation: 223 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.327 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.327 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.327 [main] DEBUG Individual - Percentage: 0.04
17:05:54.327 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.327 [main] DEBUG Individual - Percentage: 0.09
17:05:54.327 [main] DEBUG Algorithm - New offspring Individual{fitness=0.68, genes=[2.08, 1.30]}
17:05:54.327 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.327 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.327 [main] INFO Algorithm - Generation: 224 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.327 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.327 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.328 [main] DEBUG Individual - Percentage: 0.03
17:05:54.328 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.328 [main] DEBUG Individual - Percentage: 0.06
17:05:54.328 [main] DEBUG Algorithm - New offspring Individual{fitness=0.91, genes=[2.12, 1.37]}
17:05:54.328 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.328 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.328 [main] INFO Algorithm - Generation: 225 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.328 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.328 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.328 [main] DEBUG Individual - Percentage: 0.10
17:05:54.328 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.328 [main] DEBUG Individual - Percentage: 0.06
17:05:54.330 [main] DEBUG Algorithm - New offspring Individual{fitness=0.27, genes=[2.51, 1.76]}
17:05:54.330 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.330 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.330 [main] INFO Algorithm - Generation: 226 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.330 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.330 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.330 [main] DEBUG Individual - Percentage: 0.03
17:05:54.330 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.330 [main] DEBUG Individual - Percentage: 0.04
17:05:54.330 [main] DEBUG Algorithm - New offspring Individual{fitness=1.44, genes=[2.09, 1.50]}
17:05:54.330 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.331 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.331 [main] INFO Algorithm - Generation: 227 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.331 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.331 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.331 [main] DEBUG Individual - Percentage: 0.06
17:05:54.331 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.331 [main] DEBUG Individual - Percentage: 0.04
17:05:54.331 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.35, 1.72]}
17:05:54.331 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.331 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.331 [main] INFO Algorithm - Generation: 228 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.331 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.331 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.332 [main] DEBUG Individual - Percentage: 0.05
17:05:54.332 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.333 [main] DEBUG Individual - Percentage: 0.01
17:05:54.333 [main] DEBUG Algorithm - New offspring Individual{fitness=1.43, genes=[2.03, 1.59]}
17:05:54.333 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.333 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.333 [main] INFO Algorithm - Generation: 229 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.333 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.333 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.333 [main] DEBUG Individual - Percentage: 0.04
17:05:54.333 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.333 [main] DEBUG Individual - Percentage: 0.05
17:05:54.333 [main] DEBUG Algorithm - New offspring Individual{fitness=1.18, genes=[2.07, 1.46]}
17:05:54.333 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.334 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.334 [main] INFO Algorithm - Generation: 230 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.334 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.334 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.334 [main] DEBUG Individual - Percentage: 0.08
17:05:54.334 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.334 [main] DEBUG Individual - Percentage: 0.02
17:05:54.334 [main] DEBUG Algorithm - New offspring Individual{fitness=1.11, genes=[1.94, 1.52]}
17:05:54.334 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.334 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.334 [main] INFO Algorithm - Generation: 231 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.334 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.334 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.334 [main] DEBUG Individual - Percentage: 0.03
17:05:54.334 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.334 [main] DEBUG Individual - Percentage: 0.07
17:05:54.334 [main] DEBUG Algorithm - New offspring Individual{fitness=0.70, genes=[2.04, 1.36]}
17:05:54.334 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.335 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.335 [main] INFO Algorithm - Generation: 232 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.335 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.335 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.335 [main] DEBUG Individual - Percentage: 0.02
17:05:54.335 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.335 [main] DEBUG Individual - Percentage: 0.09
17:05:54.335 [main] DEBUG Algorithm - New offspring Individual{fitness=0.79, genes=[2.14, 1.28]}
17:05:54.335 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.335 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.62, genes=[2.19, 1.50]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.335 [main] INFO Algorithm - Generation: 233 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.335 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.335 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.335 [main] DEBUG Individual - Percentage: 0.01
17:05:54.335 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.335 [main] DEBUG Individual - Percentage: 0.02
17:05:54.335 [main] DEBUG Algorithm - New offspring Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.336 [main] DEBUG Algorithm - Weakest Individual{fitness=1.62, genes=[2.19, 1.50]}
17:05:54.336 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.336 [main] INFO Algorithm - Generation: 234 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.336 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.336 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.336 [main] DEBUG Individual - Percentage: 0.08
17:05:54.336 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.336 [main] DEBUG Individual - Percentage: 0.05
17:05:54.336 [main] DEBUG Algorithm - New offspring Individual{fitness=0.53, genes=[2.47, 1.72]}
17:05:54.336 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.336 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.336 [main] INFO Algorithm - Generation: 235 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.337 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.337 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.337 [main] DEBUG Individual - Percentage: 0.06
17:05:54.337 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.337 [main] DEBUG Individual - Percentage: 0.07
17:05:54.337 [main] DEBUG Algorithm - New offspring Individual{fitness=0.40, genes=[1.99, 1.29]}
17:05:54.337 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.337 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.337 [main] INFO Algorithm - Generation: 236 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.337 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.337 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.337 [main] DEBUG Individual - Percentage: 0.02
17:05:54.337 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.337 [main] DEBUG Individual - Percentage: 0.05
17:05:54.337 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.24, 1.79]}
17:05:54.337 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.338 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.338 [main] INFO Algorithm - Generation: 237 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.338 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.338 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.338 [main] DEBUG Individual - Percentage: 0.02
17:05:54.338 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.338 [main] DEBUG Individual - Percentage: 0.05
17:05:54.338 [main] DEBUG Algorithm - New offspring Individual{fitness=1.18, genes=[2.21, 1.72]}
17:05:54.338 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.338 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.338 [main] INFO Algorithm - Generation: 238 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.338 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.338 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.338 [main] DEBUG Individual - Percentage: 0.02
17:05:54.339 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.339 [main] DEBUG Individual - Percentage: 0.03
17:05:54.339 [main] DEBUG Algorithm - New offspring Individual{fitness=1.10, genes=[2.26, 1.72]}
17:05:54.339 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.339 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.339 [main] INFO Algorithm - Generation: 239 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.339 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.339 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.339 [main] DEBUG Individual - Percentage: 0.07
17:05:54.339 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.339 [main] DEBUG Individual - Percentage: 0.09
17:05:54.339 [main] DEBUG Algorithm - New offspring Individual{fitness=0.38, genes=[1.99, 1.28]}
17:05:54.339 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.339 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.339 [main] INFO Algorithm - Generation: 240 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.339 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.339 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.340 [main] DEBUG Individual - Percentage: 0.07
17:05:54.340 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.340 [main] DEBUG Individual - Percentage: 0.08
17:05:54.340 [main] DEBUG Algorithm - New offspring Individual{fitness=0.34, genes=[1.97, 1.29]}
17:05:54.341 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.341 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.341 [main] INFO Algorithm - Generation: 241 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.341 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.341 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.341 [main] DEBUG Individual - Percentage: 0.01
17:05:54.341 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.341 [main] DEBUG Individual - Percentage: 0.05
17:05:54.341 [main] DEBUG Algorithm - New offspring Individual{fitness=1.16, genes=[2.19, 1.72]}
17:05:54.341 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.341 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.341 [main] INFO Algorithm - Generation: 242 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.342 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.342 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.342 [main] DEBUG Individual - Percentage: 0.04
17:05:54.342 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.342 [main] DEBUG Individual - Percentage: 0.00
17:05:54.342 [main] DEBUG Algorithm - New offspring Individual{fitness=1.62, genes=[2.28, 1.62]}
17:05:54.342 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.342 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.342 [main] INFO Algorithm - Generation: 243 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.342 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.342 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.342 [main] DEBUG Individual - Percentage: 0.10
17:05:54.342 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.342 [main] DEBUG Individual - Percentage: 0.01
17:05:54.342 [main] DEBUG Algorithm - New offspring Individual{fitness=1.01, genes=[2.51, 1.61]}
17:05:54.342 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.342 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.343 [main] INFO Algorithm - Generation: 244 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.343 [main] INFO Algorithm - Selected Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.343 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.343 [main] DEBUG Individual - Percentage: 0.01
17:05:54.343 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.343 [main] DEBUG Individual - Percentage: 0.04
17:05:54.343 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.26, 1.76]}
17:05:54.343 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.343 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.343 [main] INFO Algorithm - Generation: 245 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.343 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.343 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.343 [main] DEBUG Individual - Percentage: 0.04
17:05:54.343 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.343 [main] DEBUG Individual - Percentage: 0.02
17:05:54.343 [main] DEBUG Algorithm - New offspring Individual{fitness=1.23, genes=[2.02, 1.49]}
17:05:54.343 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.344 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.344 [main] INFO Algorithm - Generation: 246 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.344 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.344 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.344 [main] DEBUG Individual - Percentage: 0.02
17:05:54.344 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.344 [main] DEBUG Individual - Percentage: 0.00
17:05:54.344 [main] DEBUG Algorithm - New offspring Individual{fitness=1.62, genes=[2.09, 1.57]}
17:05:54.344 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.344 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.344 [main] INFO Algorithm - Generation: 247 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.344 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.344 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.344 [main] DEBUG Individual - Percentage: 0.09
17:05:54.344 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.344 [main] DEBUG Individual - Percentage: 0.01
17:05:54.344 [main] DEBUG Algorithm - New offspring Individual{fitness=1.21, genes=[2.44, 1.55]}
17:05:54.344 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.345 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.345 [main] INFO Algorithm - Generation: 248 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.345 [main] INFO Algorithm - Selected Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.345 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.345 [main] DEBUG Individual - Percentage: 0.01
17:05:54.345 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.345 [main] DEBUG Individual - Percentage: 0.07
17:05:54.345 [main] DEBUG Algorithm - New offspring Individual{fitness=0.76, genes=[2.26, 1.85]}
17:05:54.345 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.345 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.345 [main] INFO Algorithm - Generation: 249 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.345 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.345 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.345 [main] DEBUG Individual - Percentage: 0.06
17:05:54.345 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.345 [main] DEBUG Individual - Percentage: 0.02
17:05:54.345 [main] DEBUG Algorithm - New offspring Individual{fitness=1.21, genes=[2.00, 1.51]}
17:05:54.345 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.346 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.346 [main] INFO Algorithm - Generation: 250 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.346 [main] INFO Algorithm - Selected Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.346 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.346 [main] DEBUG Individual - Percentage: 0.01
17:05:54.346 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.346 [main] DEBUG Individual - Percentage: 0.02
17:05:54.346 [main] DEBUG Algorithm - New offspring Individual{fitness=1.37, genes=[2.20, 1.68]}
17:05:54.346 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.346 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.346 [main] INFO Algorithm - Generation: 251 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.346 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.346 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.346 [main] DEBUG Individual - Percentage: 0.01
17:05:54.346 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.346 [main] DEBUG Individual - Percentage: 0.03
17:05:54.346 [main] DEBUG Algorithm - New offspring Individual{fitness=1.24, genes=[2.15, 1.43]}
17:05:54.346 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.347 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.347 [main] INFO Algorithm - Generation: 252 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.347 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.347 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.347 [main] DEBUG Individual - Percentage: 0.07
17:05:54.347 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.347 [main] DEBUG Individual - Percentage: 0.00
17:05:54.347 [main] DEBUG Algorithm - New offspring Individual{fitness=1.28, genes=[2.42, 1.58]}
17:05:54.347 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.347 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.347 [main] INFO Algorithm - Generation: 253 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.347 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.347 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.347 [main] DEBUG Individual - Percentage: 0.05
17:05:54.347 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.347 [main] DEBUG Individual - Percentage: 0.04
17:05:54.347 [main] DEBUG Algorithm - New offspring Individual{fitness=0.94, genes=[2.01, 1.45]}
17:05:54.347 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.348 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.348 [main] INFO Algorithm - Generation: 254 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.348 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.348 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.348 [main] DEBUG Individual - Percentage: 0.02
17:05:54.348 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.352 [main] DEBUG Individual - Percentage: 0.08
17:05:54.352 [main] DEBUG Algorithm - New offspring Individual{fitness=0.81, genes=[2.09, 1.35]}
17:05:54.352 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.352 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.63, genes=[2.09, 1.57]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.352 [main] INFO Algorithm - Generation: 255 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.352 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.352 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.352 [main] DEBUG Individual - Percentage: 0.00
17:05:54.352 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.352 [main] DEBUG Individual - Percentage: 0.01
17:05:54.352 [main] DEBUG Algorithm - New offspring Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.352 [main] DEBUG Algorithm - Weakest Individual{fitness=1.63, genes=[2.09, 1.57]}
17:05:54.353 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.353 [main] INFO Algorithm - Generation: 256 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.353 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.353 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.353 [main] DEBUG Individual - Percentage: 0.05
17:05:54.353 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.353 [main] DEBUG Individual - Percentage: 0.03
17:05:54.353 [main] DEBUG Algorithm - New offspring Individual{fitness=1.20, genes=[2.37, 1.65]}
17:05:54.353 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.353 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.353 [main] INFO Algorithm - Generation: 257 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.353 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.353 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.353 [main] DEBUG Individual - Percentage: 0.07
17:05:54.353 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.353 [main] DEBUG Individual - Percentage: 0.07
17:05:54.353 [main] DEBUG Algorithm - New offspring Individual{fitness=0.36, genes=[2.43, 1.79]}
17:05:54.353 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.353 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.353 [main] INFO Algorithm - Generation: 258 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.353 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.354 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.354 [main] DEBUG Individual - Percentage: 0.05
17:05:54.354 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.354 [main] DEBUG Individual - Percentage: 0.01
17:05:54.354 [main] DEBUG Algorithm - New offspring Individual{fitness=1.31, genes=[2.36, 1.64]}
17:05:54.354 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.354 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.354 [main] INFO Algorithm - Generation: 259 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.354 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.354 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.354 [main] DEBUG Individual - Percentage: 0.01
17:05:54.354 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.354 [main] DEBUG Individual - Percentage: 0.05
17:05:54.354 [main] DEBUG Algorithm - New offspring Individual{fitness=1.17, genes=[2.22, 1.72]}
17:05:54.354 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.354 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.354 [main] INFO Algorithm - Generation: 260 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.354 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.354 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.354 [main] DEBUG Individual - Percentage: 0.01
17:05:54.354 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.354 [main] DEBUG Individual - Percentage: 0.08
17:05:54.354 [main] DEBUG Algorithm - New offspring Individual{fitness=1.00, genes=[2.16, 1.37]}
17:05:54.354 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.355 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.355 [main] INFO Algorithm - Generation: 261 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.355 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.355 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.355 [main] DEBUG Individual - Percentage: 0.05
17:05:54.355 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.355 [main] DEBUG Individual - Percentage: 0.10
17:05:54.355 [main] DEBUG Algorithm - New offspring Individual{fitness=0.53, genes=[2.04, 1.26]}
17:05:54.355 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.355 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.355 [main] INFO Algorithm - Generation: 262 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.355 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.355 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.355 [main] DEBUG Individual - Percentage: 0.03
17:05:54.355 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.355 [main] DEBUG Individual - Percentage: 0.04
17:05:54.355 [main] DEBUG Algorithm - New offspring Individual{fitness=1.56, genes=[2.25, 1.65]}
17:05:54.355 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.356 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.356 [main] INFO Algorithm - Generation: 263 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.356 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.356 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.356 [main] DEBUG Individual - Percentage: 0.02
17:05:54.356 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.356 [main] DEBUG Individual - Percentage: 0.09
17:05:54.356 [main] DEBUG Algorithm - New offspring Individual{fitness=0.75, genes=[2.26, 1.86]}
17:05:54.356 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.356 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.356 [main] INFO Algorithm - Generation: 264 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.356 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.356 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.356 [main] DEBUG Individual - Percentage: 0.05
17:05:54.356 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.356 [main] DEBUG Individual - Percentage: 0.10
17:05:54.356 [main] DEBUG Algorithm - New offspring Individual{fitness=0.59, genes=[2.06, 1.26]}
17:05:54.356 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.357 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.64, genes=[2.19, 1.63]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.357 [main] INFO Algorithm - Generation: 265 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.357 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.357 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.357 [main] DEBUG Individual - Percentage: 0.00
17:05:54.357 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.357 [main] DEBUG Individual - Percentage: 0.01
17:05:54.357 [main] DEBUG Algorithm - New offspring Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.357 [main] DEBUG Algorithm - Weakest Individual{fitness=1.64, genes=[2.19, 1.63]}
17:05:54.357 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.357 [main] INFO Algorithm - Generation: 266 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.357 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.357 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.357 [main] DEBUG Individual - Percentage: 0.05
17:05:54.357 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.357 [main] DEBUG Individual - Percentage: 0.03
17:05:54.357 [main] DEBUG Algorithm - New offspring Individual{fitness=1.13, genes=[2.37, 1.66]}
17:05:54.357 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.357 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.357 [main] INFO Algorithm - Generation: 267 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.357 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.358 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.358 [main] DEBUG Individual - Percentage: 0.04
17:05:54.358 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.358 [main] DEBUG Individual - Percentage: 0.03
17:05:54.358 [main] DEBUG Algorithm - New offspring Individual{fitness=1.18, genes=[2.07, 1.46]}
17:05:54.358 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.358 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.358 [main] INFO Algorithm - Generation: 268 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.358 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.358 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.358 [main] DEBUG Individual - Percentage: 0.08
17:05:54.358 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.358 [main] DEBUG Individual - Percentage: 0.02
17:05:54.358 [main] DEBUG Algorithm - New offspring Individual{fitness=1.07, genes=[2.46, 1.62]}
17:05:54.358 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.358 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.358 [main] INFO Algorithm - Generation: 269 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.358 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.358 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.358 [main] DEBUG Individual - Percentage: 0.03
17:05:54.358 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.359 [main] DEBUG Individual - Percentage: 0.06
17:05:54.359 [main] DEBUG Algorithm - New offspring Individual{fitness=0.85, genes=[2.31, 1.75]}
17:05:54.359 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.359 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.359 [main] INFO Algorithm - Generation: 270 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.359 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.359 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.359 [main] DEBUG Individual - Percentage: 0.07
17:05:54.359 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.359 [main] DEBUG Individual - Percentage: 0.02
17:05:54.359 [main] DEBUG Algorithm - New offspring Individual{fitness=0.92, genes=[2.42, 1.67]}
17:05:54.359 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.359 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.359 [main] INFO Algorithm - Generation: 271 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.359 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.359 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.359 [main] DEBUG Individual - Percentage: 0.00
17:05:54.359 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.359 [main] DEBUG Individual - Percentage: 0.09
17:05:54.359 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.19, 1.30]}
17:05:54.359 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.360 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.360 [main] INFO Algorithm - Generation: 272 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.360 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.360 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.360 [main] DEBUG Individual - Percentage: 0.03
17:05:54.360 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.360 [main] DEBUG Individual - Percentage: 0.03
17:05:54.360 [main] DEBUG Algorithm - New offspring Individual{fitness=1.45, genes=[2.11, 1.49]}
17:05:54.360 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.360 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.360 [main] INFO Algorithm - Generation: 273 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.360 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.360 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.360 [main] DEBUG Individual - Percentage: 0.05
17:05:54.360 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.360 [main] DEBUG Individual - Percentage: 0.05
17:05:54.360 [main] DEBUG Algorithm - New offspring Individual{fitness=0.61, genes=[2.00, 1.37]}
17:05:54.360 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.361 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.361 [main] INFO Algorithm - Generation: 274 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.361 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.361 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.361 [main] DEBUG Individual - Percentage: 0.08
17:05:54.361 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.361 [main] DEBUG Individual - Percentage: 0.00
17:05:54.361 [main] DEBUG Algorithm - New offspring Individual{fitness=1.25, genes=[1.95, 1.56]}
17:05:54.361 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.361 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.361 [main] INFO Algorithm - Generation: 275 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.361 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.361 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.361 [main] DEBUG Individual - Percentage: 0.00
17:05:54.361 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.361 [main] DEBUG Individual - Percentage: 0.08
17:05:54.362 [main] DEBUG Algorithm - New offspring Individual{fitness=0.95, genes=[2.20, 1.34]}
17:05:54.362 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.362 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.69, genes=[2.23, 1.62]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.362 [main] INFO Algorithm - Generation: 276 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.362 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.362 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.362 [main] DEBUG Individual - Percentage: 0.01
17:05:54.362 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.362 [main] DEBUG Individual - Percentage: 0.00
17:05:54.362 [main] DEBUG Algorithm - New offspring Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.362 [main] DEBUG Algorithm - Weakest Individual{fitness=1.69, genes=[2.23, 1.62]}
17:05:54.363 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.363 [main] INFO Algorithm - Generation: 277 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.363 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.363 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.363 [main] DEBUG Individual - Percentage: 0.09
17:05:54.363 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.363 [main] DEBUG Individual - Percentage: 0.06
17:05:54.363 [main] DEBUG Algorithm - New offspring Individual{fitness=0.47, genes=[1.93, 1.38]}
17:05:54.363 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.363 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.363 [main] INFO Algorithm - Generation: 278 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.363 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.363 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.363 [main] DEBUG Individual - Percentage: 0.06
17:05:54.363 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.363 [main] DEBUG Individual - Percentage: 0.06
17:05:54.363 [main] DEBUG Algorithm - New offspring Individual{fitness=0.66, genes=[1.98, 1.40]}
17:05:54.363 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.364 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.364 [main] INFO Algorithm - Generation: 279 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.364 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.364 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.364 [main] DEBUG Individual - Percentage: 0.01
17:05:54.364 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.364 [main] DEBUG Individual - Percentage: 0.09
17:05:54.364 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.22, 1.89]}
17:05:54.364 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.364 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.364 [main] INFO Algorithm - Generation: 280 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.364 [main] INFO Algorithm - Selected Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.364 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.364 [main] DEBUG Individual - Percentage: 0.06
17:05:54.376 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.376 [main] DEBUG Individual - Percentage: 0.05
17:05:54.376 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.01, 1.45]}
17:05:54.376 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.377 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.70, genes=[2.25, 1.61]}]}
17:05:54.377 [main] INFO Algorithm - Generation: 281 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.377 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.377 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.377 [main] DEBUG Individual - Percentage: 0.00
17:05:54.377 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.377 [main] DEBUG Individual - Percentage: 0.01
17:05:54.377 [main] DEBUG Algorithm - New offspring Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.377 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.25, 1.61]}
17:05:54.377 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.377 [main] INFO Algorithm - Generation: 282 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.377 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.377 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.377 [main] DEBUG Individual - Percentage: 0.01
17:05:54.377 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.377 [main] DEBUG Individual - Percentage: 0.03
17:05:54.377 [main] DEBUG Algorithm - New offspring Individual{fitness=1.38, genes=[2.13, 1.47]}
17:05:54.378 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.378 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.378 [main] INFO Algorithm - Generation: 283 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.378 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.378 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.378 [main] DEBUG Individual - Percentage: 0.07
17:05:54.378 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.378 [main] DEBUG Individual - Percentage: 0.05
17:05:54.378 [main] DEBUG Algorithm - New offspring Individual{fitness=0.62, genes=[2.39, 1.75]}
17:05:54.378 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.378 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.378 [main] INFO Algorithm - Generation: 284 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.378 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.378 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.379 [main] DEBUG Individual - Percentage: 0.09
17:05:54.379 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.379 [main] DEBUG Individual - Percentage: 0.09
17:05:54.379 [main] DEBUG Algorithm - New offspring Individual{fitness=0.11, genes=[2.50, 1.87]}
17:05:54.379 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.379 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.379 [main] INFO Algorithm - Generation: 285 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.379 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.379 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.379 [main] DEBUG Individual - Percentage: 0.10
17:05:54.379 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.379 [main] DEBUG Individual - Percentage: 0.09
17:05:54.379 [main] DEBUG Algorithm - New offspring Individual{fitness=0.26, genes=[1.92, 1.30]}
17:05:54.379 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.379 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.379 [main] INFO Algorithm - Generation: 286 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.379 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.379 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.379 [main] DEBUG Individual - Percentage: 0.02
17:05:54.379 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.379 [main] DEBUG Individual - Percentage: 0.07
17:05:54.379 [main] DEBUG Algorithm - New offspring Individual{fitness=0.86, genes=[2.13, 1.34]}
17:05:54.379 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.380 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.380 [main] INFO Algorithm - Generation: 287 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.380 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.380 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.380 [main] DEBUG Individual - Percentage: 0.05
17:05:54.380 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.380 [main] DEBUG Individual - Percentage: 0.00
17:05:54.380 [main] DEBUG Algorithm - New offspring Individual{fitness=1.40, genes=[2.02, 1.55]}
17:05:54.380 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.381 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.381 [main] INFO Algorithm - Generation: 288 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.381 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.381 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.381 [main] DEBUG Individual - Percentage: 0.06
17:05:54.381 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.381 [main] DEBUG Individual - Percentage: 0.08
17:05:54.381 [main] DEBUG Algorithm - New offspring Individual{fitness=0.36, genes=[2.41, 1.82]}
17:05:54.381 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.381 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.381 [main] INFO Algorithm - Generation: 289 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.381 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.381 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.381 [main] DEBUG Individual - Percentage: 0.09
17:05:54.381 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.381 [main] DEBUG Individual - Percentage: 0.04
17:05:54.381 [main] DEBUG Algorithm - New offspring Individual{fitness=0.33, genes=[2.50, 1.75]}
17:05:54.381 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.382 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.382 [main] INFO Algorithm - Generation: 290 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.382 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.382 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.382 [main] DEBUG Individual - Percentage: 0.01
17:05:54.382 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.382 [main] DEBUG Individual - Percentage: 0.10
17:05:54.382 [main] DEBUG Algorithm - New offspring Individual{fitness=0.77, genes=[2.25, 1.87]}
17:05:54.382 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.382 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.70, genes=[2.19, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.382 [main] INFO Algorithm - Generation: 291 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.382 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.382 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.382 [main] DEBUG Individual - Percentage: 0.00
17:05:54.382 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.382 [main] DEBUG Individual - Percentage: 0.03
17:05:54.382 [main] DEBUG Algorithm - New offspring Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.382 [main] DEBUG Algorithm - Weakest Individual{fitness=1.70, genes=[2.19, 1.62]}
17:05:54.383 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.383 [main] INFO Algorithm - Generation: 292 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.383 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.383 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.383 [main] DEBUG Individual - Percentage: 0.04
17:05:54.383 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.383 [main] DEBUG Individual - Percentage: 0.01
17:05:54.383 [main] DEBUG Algorithm - New offspring Individual{fitness=1.53, genes=[2.07, 1.54]}
17:05:54.383 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.383 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.383 [main] INFO Algorithm - Generation: 293 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.383 [main] INFO Algorithm - Selected Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.383 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.383 [main] DEBUG Individual - Percentage: 0.02
17:05:54.383 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.383 [main] DEBUG Individual - Percentage: 0.02
17:05:54.384 [main] DEBUG Algorithm - New offspring Individual{fitness=1.31, genes=[2.07, 1.48]}
17:05:54.384 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.384 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.384 [main] INFO Algorithm - Generation: 294 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.384 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.384 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.384 [main] DEBUG Individual - Percentage: 0.01
17:05:54.384 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.395 [main] DEBUG Individual - Percentage: 0.09
17:05:54.395 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.24, 1.84]}
17:05:54.395 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.395 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.395 [main] INFO Algorithm - Generation: 295 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.395 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.395 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.395 [main] DEBUG Individual - Percentage: 0.02
17:05:54.395 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.395 [main] DEBUG Individual - Percentage: 0.05
17:05:54.395 [main] DEBUG Algorithm - New offspring Individual{fitness=1.03, genes=[2.28, 1.73]}
17:05:54.396 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.396 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.396 [main] INFO Algorithm - Generation: 296 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.396 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.396 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.396 [main] DEBUG Individual - Percentage: 0.05
17:05:54.396 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.396 [main] DEBUG Individual - Percentage: 0.07
17:05:54.396 [main] DEBUG Algorithm - New offspring Individual{fitness=0.49, genes=[1.99, 1.34]}
17:05:54.396 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.396 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.396 [main] INFO Algorithm - Generation: 297 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.396 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.396 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.396 [main] DEBUG Individual - Percentage: 0.01
17:05:54.396 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.396 [main] DEBUG Individual - Percentage: 0.10
17:05:54.396 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.19, 1.88]}
17:05:54.396 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.396 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.396 [main] INFO Algorithm - Generation: 298 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.397 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.397 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.397 [main] DEBUG Individual - Percentage: 0.02
17:05:54.397 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.397 [main] DEBUG Individual - Percentage: 0.07
17:05:54.397 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.14, 1.34]}
17:05:54.397 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.397 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.397 [main] INFO Algorithm - Generation: 299 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.397 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.397 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.397 [main] DEBUG Individual - Percentage: 0.05
17:05:54.397 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.397 [main] DEBUG Individual - Percentage: 0.06
17:05:54.397 [main] DEBUG Algorithm - New offspring Individual{fitness=0.58, genes=[2.38, 1.77]}
17:05:54.397 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.397 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.397 [main] INFO Algorithm - Generation: 300 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.397 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.397 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.397 [main] DEBUG Individual - Percentage: 0.03
17:05:54.397 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.397 [main] DEBUG Individual - Percentage: 0.08
17:05:54.397 [main] DEBUG Algorithm - New offspring Individual{fitness=0.73, genes=[2.29, 1.83]}
17:05:54.397 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.398 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.398 [main] INFO Algorithm - Generation: 301 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.398 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.398 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.398 [main] DEBUG Individual - Percentage: 0.09
17:05:54.398 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.398 [main] DEBUG Individual - Percentage: 0.09
17:05:54.398 [main] DEBUG Algorithm - New offspring Individual{fitness=0.15, genes=[2.48, 1.86]}
17:05:54.398 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.398 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.398 [main] INFO Algorithm - Generation: 302 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.398 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.398 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.398 [main] DEBUG Individual - Percentage: 0.09
17:05:54.416 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.419 [main] DEBUG Individual - Percentage: 0.09
17:05:54.422 [main] DEBUG Algorithm - New offspring Individual{fitness=0.25, genes=[1.93, 1.27]}
17:05:54.423 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.423 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.423 [main] INFO Algorithm - Generation: 303 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.423 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.423 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.423 [main] DEBUG Individual - Percentage: 0.08
17:05:54.423 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.423 [main] DEBUG Individual - Percentage: 0.08
17:05:54.423 [main] DEBUG Algorithm - New offspring Individual{fitness=0.45, genes=[1.98, 1.33]}
17:05:54.423 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.423 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.423 [main] INFO Algorithm - Generation: 304 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.423 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.423 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.423 [main] DEBUG Individual - Percentage: 0.03
17:05:54.423 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.423 [main] DEBUG Individual - Percentage: 0.03
17:05:54.424 [main] DEBUG Algorithm - New offspring Individual{fitness=1.17, genes=[2.31, 1.69]}
17:05:54.424 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.424 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.424 [main] INFO Algorithm - Generation: 305 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.424 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.424 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.424 [main] DEBUG Individual - Percentage: 0.09
17:05:54.424 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.424 [main] DEBUG Individual - Percentage: 0.00
17:05:54.424 [main] DEBUG Algorithm - New offspring Individual{fitness=1.21, genes=[1.93, 1.57]}
17:05:54.424 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.424 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.424 [main] INFO Algorithm - Generation: 306 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.424 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.424 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.424 [main] DEBUG Individual - Percentage: 0.06
17:05:54.424 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.424 [main] DEBUG Individual - Percentage: 0.04
17:05:54.424 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[1.97, 1.45]}
17:05:54.424 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.425 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.425 [main] INFO Algorithm - Generation: 307 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.425 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.425 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.425 [main] DEBUG Individual - Percentage: 0.08
17:05:54.425 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.425 [main] DEBUG Individual - Percentage: 0.05
17:05:54.425 [main] DEBUG Algorithm - New offspring Individual{fitness=0.45, genes=[2.47, 1.73]}
17:05:54.425 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.425 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.425 [main] INFO Algorithm - Generation: 308 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.425 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.425 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.425 [main] DEBUG Individual - Percentage: 0.04
17:05:54.425 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.425 [main] DEBUG Individual - Percentage: 0.02
17:05:54.425 [main] DEBUG Algorithm - New offspring Individual{fitness=1.47, genes=[2.07, 1.52]}
17:05:54.425 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.430 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.431 [main] INFO Algorithm - Generation: 309 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.431 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.431 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.431 [main] DEBUG Individual - Percentage: 0.06
17:05:54.431 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.431 [main] DEBUG Individual - Percentage: 0.08
17:05:54.431 [main] DEBUG Algorithm - New offspring Individual{fitness=0.59, genes=[2.04, 1.32]}
17:05:54.431 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.431 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.431 [main] INFO Algorithm - Generation: 310 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.431 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.431 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.431 [main] DEBUG Individual - Percentage: 0.08
17:05:54.431 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.431 [main] DEBUG Individual - Percentage: 0.01
17:05:54.431 [main] DEBUG Algorithm - New offspring Individual{fitness=1.20, genes=[1.93, 1.57]}
17:05:54.431 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.432 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.432 [main] INFO Algorithm - Generation: 311 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.432 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.432 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.432 [main] DEBUG Individual - Percentage: 0.07
17:05:54.432 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.432 [main] DEBUG Individual - Percentage: 0.05
17:05:54.432 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.00, 1.43]}
17:05:54.432 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.433 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.433 [main] INFO Algorithm - Generation: 312 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.433 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.433 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.433 [main] DEBUG Individual - Percentage: 0.10
17:05:54.433 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.433 [main] DEBUG Individual - Percentage: 0.07
17:05:54.433 [main] DEBUG Algorithm - New offspring Individual{fitness=0.31, genes=[1.89, 1.35]}
17:05:54.434 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.434 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.434 [main] INFO Algorithm - Generation: 313 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.435 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.435 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.435 [main] DEBUG Individual - Percentage: 0.05
17:05:54.435 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.435 [main] DEBUG Individual - Percentage: 0.03
17:05:54.435 [main] DEBUG Algorithm - New offspring Individual{fitness=1.22, genes=[2.35, 1.66]}
17:05:54.436 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.438 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.438 [main] INFO Algorithm - Generation: 314 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.438 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.438 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.439 [main] DEBUG Individual - Percentage: 0.06
17:05:54.439 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.439 [main] DEBUG Individual - Percentage: 0.02
17:05:54.439 [main] DEBUG Algorithm - New offspring Individual{fitness=1.30, genes=[2.36, 1.64]}
17:05:54.439 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.439 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.439 [main] INFO Algorithm - Generation: 315 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.439 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.439 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.439 [main] DEBUG Individual - Percentage: 0.04
17:05:54.439 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.439 [main] DEBUG Individual - Percentage: 0.06
17:05:54.439 [main] DEBUG Algorithm - New offspring Individual{fitness=0.98, genes=[2.27, 1.74]}
17:05:54.439 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.439 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.439 [main] INFO Algorithm - Generation: 316 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.439 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.439 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.439 [main] DEBUG Individual - Percentage: 0.07
17:05:54.439 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.439 [main] DEBUG Individual - Percentage: 0.08
17:05:54.439 [main] DEBUG Algorithm - New offspring Individual{fitness=0.27, genes=[2.44, 1.82]}
17:05:54.439 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.440 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.440 [main] INFO Algorithm - Generation: 317 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.440 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.440 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.440 [main] DEBUG Individual - Percentage: 0.05
17:05:54.440 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.440 [main] DEBUG Individual - Percentage: 0.03
17:05:54.440 [main] DEBUG Algorithm - New offspring Individual{fitness=1.16, genes=[2.37, 1.66]}
17:05:54.440 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.440 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.440 [main] INFO Algorithm - Generation: 318 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.440 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.440 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.440 [main] DEBUG Individual - Percentage: 0.05
17:05:54.440 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.440 [main] DEBUG Individual - Percentage: 0.06
17:05:54.440 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.33, 1.79]}
17:05:54.440 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.441 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.441 [main] INFO Algorithm - Generation: 319 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.441 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.441 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.441 [main] DEBUG Individual - Percentage: 0.10
17:05:54.441 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.441 [main] DEBUG Individual - Percentage: 0.04
17:05:54.441 [main] DEBUG Algorithm - New offspring Individual{fitness=0.65, genes=[1.91, 1.43]}
17:05:54.441 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.441 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.441 [main] INFO Algorithm - Generation: 320 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.441 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.444 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.444 [main] DEBUG Individual - Percentage: 0.05
17:05:54.444 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.444 [main] DEBUG Individual - Percentage: 0.01
17:05:54.445 [main] DEBUG Algorithm - New offspring Individual{fitness=1.56, genes=[2.32, 1.60]}
17:05:54.445 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.445 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.446 [main] INFO Algorithm - Generation: 321 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.446 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.446 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.446 [main] DEBUG Individual - Percentage: 0.08
17:05:54.446 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.446 [main] DEBUG Individual - Percentage: 0.02
17:05:54.446 [main] DEBUG Algorithm - New offspring Individual{fitness=1.03, genes=[2.45, 1.64]}
17:05:54.446 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.446 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.446 [main] INFO Algorithm - Generation: 322 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.446 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.446 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.450 [main] DEBUG Individual - Percentage: 0.01
17:05:54.450 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.450 [main] DEBUG Individual - Percentage: 0.02
17:05:54.450 [main] DEBUG Algorithm - New offspring Individual{fitness=1.60, genes=[2.12, 1.52]}
17:05:54.450 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.451 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.452 [main] INFO Algorithm - Generation: 323 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.452 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.453 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.453 [main] DEBUG Individual - Percentage: 0.09
17:05:54.454 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.454 [main] DEBUG Individual - Percentage: 0.06
17:05:54.454 [main] DEBUG Algorithm - New offspring Individual{fitness=0.22, genes=[2.52, 1.77]}
17:05:54.454 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.455 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.455 [main] INFO Algorithm - Generation: 324 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.455 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.455 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.455 [main] DEBUG Individual - Percentage: 0.01
17:05:54.455 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.455 [main] DEBUG Individual - Percentage: 0.09
17:05:54.455 [main] DEBUG Algorithm - New offspring Individual{fitness=0.81, genes=[2.22, 1.87]}
17:05:54.455 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.455 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.455 [main] INFO Algorithm - Generation: 325 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.455 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.455 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.455 [main] DEBUG Individual - Percentage: 0.02
17:05:54.455 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.455 [main] DEBUG Individual - Percentage: 0.08
17:05:54.455 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[2.15, 1.31]}
17:05:54.455 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.456 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.456 [main] INFO Algorithm - Generation: 326 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.456 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.456 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.456 [main] DEBUG Individual - Percentage: 0.07
17:05:54.456 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.456 [main] DEBUG Individual - Percentage: 0.00
17:05:54.456 [main] DEBUG Algorithm - New offspring Individual{fitness=1.29, genes=[1.97, 1.56]}
17:05:54.456 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.456 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.456 [main] INFO Algorithm - Generation: 327 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.456 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.456 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.456 [main] DEBUG Individual - Percentage: 0.05
17:05:54.456 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.456 [main] DEBUG Individual - Percentage: 0.03
17:05:54.456 [main] DEBUG Algorithm - New offspring Individual{fitness=1.29, genes=[2.32, 1.66]}
17:05:54.456 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.457 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.457 [main] INFO Algorithm - Generation: 328 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.457 [main] INFO Algorithm - Selected Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.457 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.457 [main] DEBUG Individual - Percentage: 0.04
17:05:54.457 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.457 [main] DEBUG Individual - Percentage: 0.04
17:05:54.457 [main] DEBUG Algorithm - New offspring Individual{fitness=0.81, genes=[2.00, 1.42]}
17:05:54.457 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.457 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.457 [main] INFO Algorithm - Generation: 329 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.458 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.458 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.458 [main] DEBUG Individual - Percentage: 0.01
17:05:54.458 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.458 [main] DEBUG Individual - Percentage: 0.06
17:05:54.458 [main] DEBUG Algorithm - New offspring Individual{fitness=1.01, genes=[2.14, 1.38]}
17:05:54.458 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.458 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.458 [main] INFO Algorithm - Generation: 330 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.458 [main] INFO Algorithm - Selected Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.459 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.459 [main] DEBUG Individual - Percentage: 0.00
17:05:54.459 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.459 [main] DEBUG Individual - Percentage: 0.06
17:05:54.459 [main] DEBUG Algorithm - New offspring Individual{fitness=0.95, genes=[2.14, 1.37]}
17:05:54.459 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.480 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.480 [main] INFO Algorithm - Generation: 331 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.480 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.481 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.481 [main] DEBUG Individual - Percentage: 0.02
17:05:54.481 [main] DEBUG Individual - Mutated by: 0.08
17:05:54.481 [main] DEBUG Individual - Percentage: 0.02
17:05:54.481 [main] DEBUG Algorithm - New offspring Individual{fitness=1.45, genes=[2.28, 1.65]}
17:05:54.481 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.482 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.482 [main] INFO Algorithm - Generation: 332 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.482 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.482 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.482 [main] DEBUG Individual - Percentage: 0.04
17:05:54.482 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.482 [main] DEBUG Individual - Percentage: 0.08
17:05:54.482 [main] DEBUG Algorithm - New offspring Individual{fitness=0.75, genes=[2.10, 1.31]}
17:05:54.482 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.483 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.483 [main] INFO Algorithm - Generation: 333 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.483 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.484 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.484 [main] DEBUG Individual - Percentage: 0.05
17:05:54.484 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.484 [main] DEBUG Individual - Percentage: 0.08
17:05:54.484 [main] DEBUG Algorithm - New offspring Individual{fitness=0.58, genes=[2.02, 1.34]}
17:05:54.484 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.484 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.484 [main] INFO Algorithm - Generation: 334 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.484 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.484 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.484 [main] DEBUG Individual - Percentage: 0.08
17:05:54.484 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.484 [main] DEBUG Individual - Percentage: 0.09
17:05:54.484 [main] DEBUG Algorithm - New offspring Individual{fitness=0.33, genes=[1.96, 1.30]}
17:05:54.484 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.485 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.485 [main] INFO Algorithm - Generation: 335 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.487 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.487 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.487 [main] DEBUG Individual - Percentage: 0.08
17:05:54.487 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.487 [main] DEBUG Individual - Percentage: 0.06
17:05:54.487 [main] DEBUG Algorithm - New offspring Individual{fitness=0.27, genes=[2.48, 1.77]}
17:05:54.487 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.488 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.488 [main] INFO Algorithm - Generation: 336 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.488 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.488 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.488 [main] DEBUG Individual - Percentage: 0.07
17:05:54.488 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.488 [main] DEBUG Individual - Percentage: 0.08
17:05:54.489 [main] DEBUG Algorithm - New offspring Individual{fitness=0.29, genes=[2.43, 1.82]}
17:05:54.489 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.489 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.489 [main] INFO Algorithm - Generation: 337 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.489 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.489 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.490 [main] DEBUG Individual - Percentage: 0.06
17:05:54.490 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.490 [main] DEBUG Individual - Percentage: 0.06
17:05:54.490 [main] DEBUG Algorithm - New offspring Individual{fitness=0.83, genes=[2.34, 1.73]}
17:05:54.490 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.491 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.491 [main] INFO Algorithm - Generation: 338 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.491 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.491 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.491 [main] DEBUG Individual - Percentage: 0.04
17:05:54.491 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.491 [main] DEBUG Individual - Percentage: 0.06
17:05:54.492 [main] DEBUG Algorithm - New offspring Individual{fitness=0.79, genes=[2.31, 1.77]}
17:05:54.492 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.503 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.504 [main] INFO Algorithm - Generation: 339 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.504 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.504 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.504 [main] DEBUG Individual - Percentage: 0.05
17:05:54.505 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.505 [main] DEBUG Individual - Percentage: 0.06
17:05:54.505 [main] DEBUG Algorithm - New offspring Individual{fitness=0.74, genes=[2.03, 1.38]}
17:05:54.506 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.506 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.506 [main] INFO Algorithm - Generation: 340 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.506 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.506 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.506 [main] DEBUG Individual - Percentage: 0.07
17:05:54.506 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.506 [main] DEBUG Individual - Percentage: 0.00
17:05:54.506 [main] DEBUG Algorithm - New offspring Individual{fitness=1.24, genes=[1.95, 1.55]}
17:05:54.506 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.507 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.507 [main] INFO Algorithm - Generation: 341 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.507 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.507 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.507 [main] DEBUG Individual - Percentage: 0.00
17:05:54.507 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.507 [main] DEBUG Individual - Percentage: 0.06
17:05:54.507 [main] DEBUG Algorithm - New offspring Individual{fitness=1.11, genes=[2.19, 1.39]}
17:05:54.507 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.507 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.507 [main] INFO Algorithm - Generation: 342 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.507 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.507 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.507 [main] DEBUG Individual - Percentage: 0.09
17:05:54.507 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.507 [main] DEBUG Individual - Percentage: 0.06
17:05:54.507 [main] DEBUG Algorithm - New offspring Individual{fitness=0.44, genes=[2.44, 1.75]}
17:05:54.507 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.508 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.508 [main] INFO Algorithm - Generation: 343 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.508 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.508 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.508 [main] DEBUG Individual - Percentage: 0.04
17:05:54.508 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.508 [main] DEBUG Individual - Percentage: 0.10
17:05:54.508 [main] DEBUG Algorithm - New offspring Individual{fitness=0.55, genes=[2.34, 1.87]}
17:05:54.508 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.508 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.508 [main] INFO Algorithm - Generation: 344 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.508 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.508 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.508 [main] DEBUG Individual - Percentage: 0.06
17:05:54.508 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.508 [main] DEBUG Individual - Percentage: 0.07
17:05:54.508 [main] DEBUG Algorithm - New offspring Individual{fitness=0.53, genes=[2.36, 1.81]}
17:05:54.508 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.510 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.511 [main] INFO Algorithm - Generation: 345 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.511 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.512 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.512 [main] DEBUG Individual - Percentage: 0.09
17:05:54.512 [main] DEBUG Individual - Mutated by: 0.29
17:05:54.512 [main] DEBUG Individual - Percentage: 0.09
17:05:54.512 [main] DEBUG Algorithm - New offspring Individual{fitness=0.10, genes=[2.51, 1.87]}
17:05:54.512 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.513 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.513 [main] INFO Algorithm - Generation: 346 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.513 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.513 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.513 [main] DEBUG Individual - Percentage: 0.01
17:05:54.513 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.513 [main] DEBUG Individual - Percentage: 0.02
17:05:54.513 [main] DEBUG Algorithm - New offspring Individual{fitness=1.70, genes=[2.23, 1.62]}
17:05:54.513 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.513 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.513 [main] INFO Algorithm - Generation: 347 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.513 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.514 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.514 [main] DEBUG Individual - Percentage: 0.01
17:05:54.514 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.514 [main] DEBUG Individual - Percentage: 0.10
17:05:54.514 [main] DEBUG Algorithm - New offspring Individual{fitness=0.73, genes=[2.12, 1.26]}
17:05:54.514 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.514 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.514 [main] INFO Algorithm - Generation: 348 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.514 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.514 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.514 [main] DEBUG Individual - Percentage: 0.07
17:05:54.514 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.514 [main] DEBUG Individual - Percentage: 0.04
17:05:54.514 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[1.99, 1.43]}
17:05:54.514 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.514 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.514 [main] INFO Algorithm - Generation: 349 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.514 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.514 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.514 [main] DEBUG Individual - Percentage: 0.10
17:05:54.514 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.514 [main] DEBUG Individual - Percentage: 0.05
17:05:54.514 [main] DEBUG Algorithm - New offspring Individual{fitness=0.58, genes=[2.46, 1.71]}
17:05:54.514 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.515 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.515 [main] INFO Algorithm - Generation: 350 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.515 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.515 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.515 [main] DEBUG Individual - Percentage: 0.09
17:05:54.515 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.515 [main] DEBUG Individual - Percentage: 0.09
17:05:54.515 [main] DEBUG Algorithm - New offspring Individual{fitness=0.28, genes=[1.90, 1.33]}
17:05:54.515 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.515 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.515 [main] INFO Algorithm - Generation: 351 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.515 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.515 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.515 [main] DEBUG Individual - Percentage: 0.03
17:05:54.515 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.515 [main] DEBUG Individual - Percentage: 0.09
17:05:54.515 [main] DEBUG Algorithm - New offspring Individual{fitness=0.72, genes=[2.28, 1.88]}
17:05:54.515 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.515 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.515 [main] INFO Algorithm - Generation: 352 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.515 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.515 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.515 [main] DEBUG Individual - Percentage: 0.05
17:05:54.515 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.515 [main] DEBUG Individual - Percentage: 0.00
17:05:54.515 [main] DEBUG Algorithm - New offspring Individual{fitness=1.48, genes=[2.36, 1.58]}
17:05:54.515 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.516 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.516 [main] INFO Algorithm - Generation: 353 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.516 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.516 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.516 [main] DEBUG Individual - Percentage: 0.02
17:05:54.516 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.516 [main] DEBUG Individual - Percentage: 0.06
17:05:54.516 [main] DEBUG Algorithm - New offspring Individual{fitness=0.92, genes=[2.22, 1.78]}
17:05:54.516 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.517 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.517 [main] INFO Algorithm - Generation: 354 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.517 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.517 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.517 [main] DEBUG Individual - Percentage: 0.04
17:05:54.517 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.517 [main] DEBUG Individual - Percentage: 0.03
17:05:54.517 [main] DEBUG Algorithm - New offspring Individual{fitness=1.10, genes=[2.31, 1.70]}
17:05:54.517 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.517 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.517 [main] INFO Algorithm - Generation: 355 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.517 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.518 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.518 [main] DEBUG Individual - Percentage: 0.02
17:05:54.518 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.518 [main] DEBUG Individual - Percentage: 0.04
17:05:54.518 [main] DEBUG Algorithm - New offspring Individual{fitness=1.23, genes=[2.13, 1.43]}
17:05:54.518 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.518 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.518 [main] INFO Algorithm - Generation: 356 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.518 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.518 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.518 [main] DEBUG Individual - Percentage: 0.03
17:05:54.518 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.518 [main] DEBUG Individual - Percentage: 0.08
17:05:54.518 [main] DEBUG Algorithm - New offspring Individual{fitness=0.73, genes=[2.27, 1.87]}
17:05:54.518 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.518 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.518 [main] INFO Algorithm - Generation: 357 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.518 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.518 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.518 [main] DEBUG Individual - Percentage: 0.00
17:05:54.518 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.518 [main] DEBUG Individual - Percentage: 0.10
17:05:54.518 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[2.19, 1.26]}
17:05:54.518 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.519 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.519 [main] INFO Algorithm - Generation: 358 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.519 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.519 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.519 [main] DEBUG Individual - Percentage: 0.03
17:05:54.519 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.519 [main] DEBUG Individual - Percentage: 0.02
17:05:54.519 [main] DEBUG Algorithm - New offspring Individual{fitness=1.51, genes=[2.29, 1.64]}
17:05:54.519 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.519 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.519 [main] INFO Algorithm - Generation: 359 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.519 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.519 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.519 [main] DEBUG Individual - Percentage: 0.07
17:05:54.519 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.519 [main] DEBUG Individual - Percentage: 0.01
17:05:54.519 [main] DEBUG Algorithm - New offspring Individual{fitness=1.38, genes=[2.37, 1.61]}
17:05:54.519 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.519 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.519 [main] INFO Algorithm - Generation: 360 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.520 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.520 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.520 [main] DEBUG Individual - Percentage: 0.00
17:05:54.520 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.520 [main] DEBUG Individual - Percentage: 0.04
17:05:54.520 [main] DEBUG Algorithm - New offspring Individual{fitness=1.53, genes=[2.18, 1.66]}
17:05:54.520 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.520 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.520 [main] INFO Algorithm - Generation: 361 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.520 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.520 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.520 [main] DEBUG Individual - Percentage: 0.05
17:05:54.520 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.520 [main] DEBUG Individual - Percentage: 0.00
17:05:54.520 [main] DEBUG Algorithm - New offspring Individual{fitness=1.51, genes=[2.05, 1.56]}
17:05:54.520 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.520 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.520 [main] INFO Algorithm - Generation: 362 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.520 [main] INFO Algorithm - Selected Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.520 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.520 [main] DEBUG Individual - Percentage: 0.03
17:05:54.520 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.520 [main] DEBUG Individual - Percentage: 0.05
17:05:54.520 [main] DEBUG Algorithm - New offspring Individual{fitness=1.29, genes=[2.23, 1.70]}
17:05:54.520 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.521 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.521 [main] INFO Algorithm - Generation: 363 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.521 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.521 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.521 [main] DEBUG Individual - Percentage: 0.08
17:05:54.521 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.521 [main] DEBUG Individual - Percentage: 0.04
17:05:54.521 [main] DEBUG Algorithm - New offspring Individual{fitness=0.68, genes=[2.47, 1.69]}
17:05:54.521 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.522 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.522 [main] INFO Algorithm - Generation: 364 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.522 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.522 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.522 [main] DEBUG Individual - Percentage: 0.03
17:05:54.522 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.522 [main] DEBUG Individual - Percentage: 0.06
17:05:54.522 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.11, 1.39]}
17:05:54.522 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.522 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.522 [main] INFO Algorithm - Generation: 365 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.522 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.522 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.522 [main] DEBUG Individual - Percentage: 0.03
17:05:54.522 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.524 [main] DEBUG Individual - Percentage: 0.00
17:05:54.524 [main] DEBUG Algorithm - New offspring Individual{fitness=1.52, genes=[2.06, 1.60]}
17:05:54.524 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.524 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.71, genes=[2.17, 1.62]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.524 [main] INFO Algorithm - Generation: 366 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.525 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.525 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.525 [main] DEBUG Individual - Percentage: 0.00
17:05:54.525 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.525 [main] DEBUG Individual - Percentage: 0.00
17:05:54.525 [main] DEBUG Algorithm - New offspring Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.525 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.62]}
17:05:54.525 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.525 [main] INFO Algorithm - Generation: 367 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.525 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.525 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.525 [main] DEBUG Individual - Percentage: 0.02
17:05:54.525 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.525 [main] DEBUG Individual - Percentage: 0.01
17:05:54.525 [main] DEBUG Algorithm - New offspring Individual{fitness=1.68, genes=[2.23, 1.62]}
17:05:54.525 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.525 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.525 [main] INFO Algorithm - Generation: 368 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.526 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.526 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.526 [main] DEBUG Individual - Percentage: 0.02
17:05:54.526 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.526 [main] DEBUG Individual - Percentage: 0.02
17:05:54.526 [main] DEBUG Algorithm - New offspring Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.526 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.526 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.526 [main] INFO Algorithm - Generation: 369 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.526 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.526 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.526 [main] DEBUG Individual - Percentage: 0.04
17:05:54.526 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.526 [main] DEBUG Individual - Percentage: 0.06
17:05:54.526 [main] DEBUG Algorithm - New offspring Individual{fitness=0.74, genes=[2.34, 1.75]}
17:05:54.526 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.526 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.526 [main] INFO Algorithm - Generation: 370 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.526 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.526 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.526 [main] DEBUG Individual - Percentage: 0.08
17:05:54.526 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.526 [main] DEBUG Individual - Percentage: 0.03
17:05:54.526 [main] DEBUG Algorithm - New offspring Individual{fitness=0.94, genes=[1.96, 1.47]}
17:05:54.527 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.527 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.527 [main] INFO Algorithm - Generation: 371 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.527 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.527 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.527 [main] DEBUG Individual - Percentage: 0.05
17:05:54.527 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.527 [main] DEBUG Individual - Percentage: 0.05
17:05:54.527 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[2.00, 1.43]}
17:05:54.527 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.527 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.527 [main] INFO Algorithm - Generation: 372 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.527 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.527 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.527 [main] DEBUG Individual - Percentage: 0.09
17:05:54.527 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.527 [main] DEBUG Individual - Percentage: 0.05
17:05:54.527 [main] DEBUG Algorithm - New offspring Individual{fitness=0.43, genes=[2.44, 1.75]}
17:05:54.527 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.528 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.528 [main] INFO Algorithm - Generation: 373 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.528 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.528 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.528 [main] DEBUG Individual - Percentage: 0.01
17:05:54.528 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.528 [main] DEBUG Individual - Percentage: 0.07
17:05:54.528 [main] DEBUG Algorithm - New offspring Individual{fitness=0.87, genes=[2.24, 1.79]}
17:05:54.528 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.528 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.528 [main] INFO Algorithm - Generation: 374 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.528 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.528 [main] DEBUG Individual - Mutated by: 0.06
17:05:54.528 [main] DEBUG Individual - Percentage: 0.02
17:05:54.528 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.528 [main] DEBUG Individual - Percentage: 0.10
17:05:54.528 [main] DEBUG Algorithm - New offspring Individual{fitness=0.79, genes=[2.23, 1.91]}
17:05:54.528 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.528 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.528 [main] INFO Algorithm - Generation: 375 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.528 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.528 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.528 [main] DEBUG Individual - Percentage: 0.08
17:05:54.528 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.528 [main] DEBUG Individual - Percentage: 0.02
17:05:54.528 [main] DEBUG Algorithm - New offspring Individual{fitness=1.03, genes=[1.91, 1.51]}
17:05:54.528 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.529 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.529 [main] INFO Algorithm - Generation: 376 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.529 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.529 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.529 [main] DEBUG Individual - Percentage: 0.02
17:05:54.529 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.529 [main] DEBUG Individual - Percentage: 0.03
17:05:54.529 [main] DEBUG Algorithm - New offspring Individual{fitness=1.43, genes=[2.21, 1.67]}
17:05:54.529 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.529 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.529 [main] INFO Algorithm - Generation: 377 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.529 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.529 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.529 [main] DEBUG Individual - Percentage: 0.08
17:05:54.529 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.529 [main] DEBUG Individual - Percentage: 0.10
17:05:54.529 [main] DEBUG Algorithm - New offspring Individual{fitness=0.18, genes=[2.46, 1.87]}
17:05:54.529 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.529 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.530 [main] INFO Algorithm - Generation: 378 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.530 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.530 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.530 [main] DEBUG Individual - Percentage: 0.10
17:05:54.530 [main] DEBUG Individual - Mutated by: 0.30
17:05:54.530 [main] DEBUG Individual - Percentage: 0.10
17:05:54.530 [main] DEBUG Algorithm - New offspring Individual{fitness=0.07, genes=[2.53, 1.87]}
17:05:54.530 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.530 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.530 [main] INFO Algorithm - Generation: 379 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.530 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.530 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.530 [main] DEBUG Individual - Percentage: 0.04
17:05:54.530 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.530 [main] DEBUG Individual - Percentage: 0.07
17:05:54.530 [main] DEBUG Algorithm - New offspring Individual{fitness=0.64, genes=[2.33, 1.80]}
17:05:54.530 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.530 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.530 [main] INFO Algorithm - Generation: 380 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.530 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.530 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.530 [main] DEBUG Individual - Percentage: 0.02
17:05:54.530 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.530 [main] DEBUG Individual - Percentage: 0.09
17:05:54.530 [main] DEBUG Algorithm - New offspring Individual{fitness=0.81, genes=[2.21, 1.86]}
17:05:54.530 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.531 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.531 [main] INFO Algorithm - Generation: 381 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.531 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.531 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.531 [main] DEBUG Individual - Percentage: 0.08
17:05:54.531 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.531 [main] DEBUG Individual - Percentage: 0.03
17:05:54.531 [main] DEBUG Algorithm - New offspring Individual{fitness=0.85, genes=[1.90, 1.47]}
17:05:54.531 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.531 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.531 [main] INFO Algorithm - Generation: 382 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.531 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.531 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.531 [main] DEBUG Individual - Percentage: 0.07
17:05:54.531 [main] DEBUG Individual - Mutated by: 0.18
17:05:54.531 [main] DEBUG Individual - Percentage: 0.06
17:05:54.531 [main] DEBUG Algorithm - New offspring Individual{fitness=0.45, genes=[2.44, 1.75]}
17:05:54.531 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.531 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.531 [main] INFO Algorithm - Generation: 383 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.531 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.531 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.531 [main] DEBUG Individual - Percentage: 0.10
17:05:54.531 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.531 [main] DEBUG Individual - Percentage: 0.07
17:05:54.531 [main] DEBUG Algorithm - New offspring Individual{fitness=0.34, genes=[1.87, 1.37]}
17:05:54.531 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.532 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.532 [main] INFO Algorithm - Generation: 384 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.532 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.532 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.532 [main] DEBUG Individual - Percentage: 0.10
17:05:54.532 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.532 [main] DEBUG Individual - Percentage: 0.09
17:05:54.532 [main] DEBUG Algorithm - New offspring Individual{fitness=0.16, genes=[1.85, 1.30]}
17:05:54.532 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.532 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.532 [main] INFO Algorithm - Generation: 385 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.532 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.532 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.532 [main] DEBUG Individual - Percentage: 0.05
17:05:54.532 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.532 [main] DEBUG Individual - Percentage: 0.01
17:05:54.532 [main] DEBUG Algorithm - New offspring Individual{fitness=1.31, genes=[2.00, 1.54]}
17:05:54.532 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.532 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.532 [main] INFO Algorithm - Generation: 386 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.532 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.532 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.532 [main] DEBUG Individual - Percentage: 0.01
17:05:54.532 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.532 [main] DEBUG Individual - Percentage: 0.08
17:05:54.532 [main] DEBUG Algorithm - New offspring Individual{fitness=0.88, genes=[2.16, 1.32]}
17:05:54.532 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.533 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.533 [main] INFO Algorithm - Generation: 387 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.533 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.533 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.533 [main] DEBUG Individual - Percentage: 0.09
17:05:54.533 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.533 [main] DEBUG Individual - Percentage: 0.07
17:05:54.533 [main] DEBUG Algorithm - New offspring Individual{fitness=0.36, genes=[1.93, 1.35]}
17:05:54.533 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.533 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.533 [main] INFO Algorithm - Generation: 388 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.533 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.533 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.533 [main] DEBUG Individual - Percentage: 0.08
17:05:54.533 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.533 [main] DEBUG Individual - Percentage: 0.01
17:05:54.533 [main] DEBUG Algorithm - New offspring Individual{fitness=1.18, genes=[1.92, 1.56]}
17:05:54.533 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.534 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.534 [main] INFO Algorithm - Generation: 389 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.534 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.534 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.534 [main] DEBUG Individual - Percentage: 0.05
17:05:54.534 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.534 [main] DEBUG Individual - Percentage: 0.04
17:05:54.534 [main] DEBUG Algorithm - New offspring Individual{fitness=1.15, genes=[2.32, 1.69]}
17:05:54.534 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.534 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.534 [main] INFO Algorithm - Generation: 390 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.534 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.534 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.534 [main] DEBUG Individual - Percentage: 0.01
17:05:54.534 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.534 [main] DEBUG Individual - Percentage: 0.03
17:05:54.534 [main] DEBUG Algorithm - New offspring Individual{fitness=1.54, genes=[2.18, 1.48]}
17:05:54.534 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.534 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.534 [main] INFO Algorithm - Generation: 391 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.534 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.534 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.534 [main] DEBUG Individual - Percentage: 0.04
17:05:54.534 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.534 [main] DEBUG Individual - Percentage: 0.03
17:05:54.534 [main] DEBUG Algorithm - New offspring Individual{fitness=1.34, genes=[2.08, 1.48]}
17:05:54.534 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.535 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.535 [main] INFO Algorithm - Generation: 392 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.535 [main] INFO Algorithm - Selected Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.535 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.535 [main] DEBUG Individual - Percentage: 0.04
17:05:54.535 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.535 [main] DEBUG Individual - Percentage: 0.07
17:05:54.535 [main] DEBUG Algorithm - New offspring Individual{fitness=0.63, genes=[2.06, 1.31]}
17:05:54.535 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.535 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.535 [main] INFO Algorithm - Generation: 393 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.535 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.535 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.535 [main] DEBUG Individual - Percentage: 0.03
17:05:54.535 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.535 [main] DEBUG Individual - Percentage: 0.09
17:05:54.535 [main] DEBUG Algorithm - New offspring Individual{fitness=0.76, genes=[2.12, 1.30]}
17:05:54.535 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.535 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.535 [main] INFO Algorithm - Generation: 394 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.535 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.535 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.535 [main] DEBUG Individual - Percentage: 0.05
17:05:54.536 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.536 [main] DEBUG Individual - Percentage: 0.03
17:05:54.536 [main] DEBUG Algorithm - New offspring Individual{fitness=1.40, genes=[2.08, 1.50]}
17:05:54.536 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.536 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.536 [main] INFO Algorithm - Generation: 395 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.536 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.536 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.536 [main] DEBUG Individual - Percentage: 0.06
17:05:54.536 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.536 [main] DEBUG Individual - Percentage: 0.08
17:05:54.536 [main] DEBUG Algorithm - New offspring Individual{fitness=0.32, genes=[2.41, 1.86]}
17:05:54.536 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.536 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.536 [main] INFO Algorithm - Generation: 396 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.536 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.536 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.536 [main] DEBUG Individual - Percentage: 0.06
17:05:54.536 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.536 [main] DEBUG Individual - Percentage: 0.10
17:05:54.536 [main] DEBUG Algorithm - New offspring Individual{fitness=0.46, genes=[2.02, 1.26]}
17:05:54.536 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.536 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.536 [main] INFO Algorithm - Generation: 397 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.536 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.536 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.537 [main] DEBUG Individual - Percentage: 0.00
17:05:54.537 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.537 [main] DEBUG Individual - Percentage: 0.06
17:05:54.537 [main] DEBUG Algorithm - New offspring Individual{fitness=1.07, genes=[2.16, 1.39]}
17:05:54.537 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.537 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.537 [main] INFO Algorithm - Generation: 398 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.537 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.537 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.537 [main] DEBUG Individual - Percentage: 0.08
17:05:54.537 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.537 [main] DEBUG Individual - Percentage: 0.09
17:05:54.537 [main] DEBUG Algorithm - New offspring Individual{fitness=0.21, genes=[1.90, 1.28]}
17:05:54.537 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.537 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.537 [main] INFO Algorithm - Generation: 399 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.537 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.537 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.537 [main] DEBUG Individual - Percentage: 0.08
17:05:54.537 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.537 [main] DEBUG Individual - Percentage: 0.07
17:05:54.537 [main] DEBUG Algorithm - New offspring Individual{fitness=0.42, genes=[1.95, 1.35]}
17:05:54.537 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.537 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.537 [main] INFO Algorithm - Generation: 400 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.538 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.538 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.538 [main] DEBUG Individual - Percentage: 0.03
17:05:54.538 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.538 [main] DEBUG Individual - Percentage: 0.04
17:05:54.538 [main] DEBUG Algorithm - New offspring Individual{fitness=1.27, genes=[2.29, 1.68]}
17:05:54.538 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.538 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.538 [main] INFO Algorithm - Generation: 401 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.538 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.538 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.538 [main] DEBUG Individual - Percentage: 0.08
17:05:54.538 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.538 [main] DEBUG Individual - Percentage: 0.06
17:05:54.538 [main] DEBUG Algorithm - New offspring Individual{fitness=0.56, genes=[1.97, 1.38]}
17:05:54.538 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.538 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.538 [main] INFO Algorithm - Generation: 402 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.538 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.538 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.538 [main] DEBUG Individual - Percentage: 0.09
17:05:54.538 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.538 [main] DEBUG Individual - Percentage: 0.07
17:05:54.538 [main] DEBUG Algorithm - New offspring Individual{fitness=0.33, genes=[1.92, 1.34]}
17:05:54.538 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.538 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.538 [main] INFO Algorithm - Generation: 403 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.538 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.538 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.539 [main] DEBUG Individual - Percentage: 0.01
17:05:54.539 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.539 [main] DEBUG Individual - Percentage: 0.08
17:05:54.539 [main] DEBUG Algorithm - New offspring Individual{fitness=0.83, genes=[2.19, 1.84]}
17:05:54.539 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.539 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.539 [main] INFO Algorithm - Generation: 404 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.539 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.539 [main] DEBUG Individual - Mutated by: 0.26
17:05:54.539 [main] DEBUG Individual - Percentage: 0.08
17:05:54.539 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.539 [main] DEBUG Individual - Percentage: 0.05
17:05:54.539 [main] DEBUG Algorithm - New offspring Individual{fitness=0.40, genes=[2.48, 1.74]}
17:05:54.539 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.539 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.539 [main] INFO Algorithm - Generation: 405 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.539 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.539 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.539 [main] DEBUG Individual - Percentage: 0.01
17:05:54.539 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.539 [main] DEBUG Individual - Percentage: 0.02
17:05:54.539 [main] DEBUG Algorithm - New offspring Individual{fitness=1.65, genes=[2.20, 1.51]}
17:05:54.539 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.539 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.539 [main] INFO Algorithm - Generation: 406 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.539 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.539 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.539 [main] DEBUG Individual - Percentage: 0.01
17:05:54.539 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.539 [main] DEBUG Individual - Percentage: 0.05
17:05:54.539 [main] DEBUG Algorithm - New offspring Individual{fitness=1.26, genes=[2.18, 1.43]}
17:05:54.539 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.540 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.540 [main] INFO Algorithm - Generation: 407 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.540 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.540 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.540 [main] DEBUG Individual - Percentage: 0.09
17:05:54.540 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.540 [main] DEBUG Individual - Percentage: 0.07
17:05:54.540 [main] DEBUG Algorithm - New offspring Individual{fitness=0.46, genes=[1.90, 1.39]}
17:05:54.540 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.540 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.540 [main] INFO Algorithm - Generation: 408 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.540 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.540 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.540 [main] DEBUG Individual - Percentage: 0.03
17:05:54.540 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.540 [main] DEBUG Individual - Percentage: 0.04
17:05:54.540 [main] DEBUG Algorithm - New offspring Individual{fitness=1.28, genes=[2.12, 1.45]}
17:05:54.540 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.540 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.540 [main] INFO Algorithm - Generation: 409 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.540 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.540 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.540 [main] DEBUG Individual - Percentage: 0.01
17:05:54.540 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.540 [main] DEBUG Individual - Percentage: 0.02
17:05:54.541 [main] DEBUG Algorithm - New offspring Individual{fitness=1.64, genes=[2.19, 1.50]}
17:05:54.541 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.541 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.541 [main] INFO Algorithm - Generation: 410 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.541 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.541 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.541 [main] DEBUG Individual - Percentage: 0.04
17:05:54.541 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.541 [main] DEBUG Individual - Percentage: 0.05
17:05:54.541 [main] DEBUG Algorithm - New offspring Individual{fitness=1.05, genes=[2.08, 1.43]}
17:05:54.541 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.541 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.541 [main] INFO Algorithm - Generation: 411 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.541 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.541 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.541 [main] DEBUG Individual - Percentage: 0.00
17:05:54.541 [main] DEBUG Individual - Mutated by: 0.14
17:05:54.541 [main] DEBUG Individual - Percentage: 0.05
17:05:54.541 [main] DEBUG Algorithm - New offspring Individual{fitness=1.16, genes=[2.23, 1.72]}
17:05:54.541 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.541 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.541 [main] INFO Algorithm - Generation: 412 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.541 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.541 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.541 [main] DEBUG Individual - Percentage: 0.08
17:05:54.541 [main] DEBUG Individual - Mutated by: 0.03
17:05:54.541 [main] DEBUG Individual - Percentage: 0.01
17:05:54.541 [main] DEBUG Algorithm - New offspring Individual{fitness=1.18, genes=[2.44, 1.60]}
17:05:54.541 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.542 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.542 [main] INFO Algorithm - Generation: 413 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.542 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.542 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.542 [main] DEBUG Individual - Percentage: 0.07
17:05:54.542 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.542 [main] DEBUG Individual - Percentage: 0.04
17:05:54.542 [main] DEBUG Algorithm - New offspring Individual{fitness=0.65, genes=[2.45, 1.71]}
17:05:54.542 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.542 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.542 [main] INFO Algorithm - Generation: 414 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.542 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.542 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.542 [main] DEBUG Individual - Percentage: 0.02
17:05:54.542 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.542 [main] DEBUG Individual - Percentage: 0.09
17:05:54.542 [main] DEBUG Algorithm - New offspring Individual{fitness=0.79, genes=[2.14, 1.27]}
17:05:54.542 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.542 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.17, 1.53]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.542 [main] INFO Algorithm - Generation: 415 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.542 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.542 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.542 [main] DEBUG Individual - Percentage: 0.03
17:05:54.542 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.542 [main] DEBUG Individual - Percentage: 0.00
17:05:54.542 [main] DEBUG Algorithm - New offspring Individual{fitness=1.71, genes=[2.27, 1.59]}
17:05:54.542 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.17, 1.53]}
17:05:54.543 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.71, genes=[2.27, 1.59]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.543 [main] INFO Algorithm - Generation: 416 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.543 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.543 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.543 [main] DEBUG Individual - Percentage: 0.01
17:05:54.543 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.543 [main] DEBUG Individual - Percentage: 0.01
17:05:54.543 [main] DEBUG Algorithm - New offspring Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.543 [main] DEBUG Algorithm - Weakest Individual{fitness=1.71, genes=[2.27, 1.59]}
17:05:54.543 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.543 [main] INFO Algorithm - Generation: 417 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.543 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.543 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.543 [main] DEBUG Individual - Percentage: 0.06
17:05:54.543 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.543 [main] DEBUG Individual - Percentage: 0.07
17:05:54.543 [main] DEBUG Algorithm - New offspring Individual{fitness=0.53, genes=[1.98, 1.36]}
17:05:54.543 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.543 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.543 [main] INFO Algorithm - Generation: 418 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.543 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.543 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.543 [main] DEBUG Individual - Percentage: 0.08
17:05:54.543 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.543 [main] DEBUG Individual - Percentage: 0.07
17:05:54.543 [main] DEBUG Algorithm - New offspring Individual{fitness=0.33, genes=[2.45, 1.78]}
17:05:54.543 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.543 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.543 [main] INFO Algorithm - Generation: 419 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.543 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.544 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.544 [main] DEBUG Individual - Percentage: 0.04
17:05:54.544 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.544 [main] DEBUG Individual - Percentage: 0.06
17:05:54.544 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.29, 1.78]}
17:05:54.544 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.544 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.544 [main] INFO Algorithm - Generation: 420 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.544 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.544 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.544 [main] DEBUG Individual - Percentage: 0.04
17:05:54.544 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.544 [main] DEBUG Individual - Percentage: 0.07
17:05:54.544 [main] DEBUG Algorithm - New offspring Individual{fitness=0.80, genes=[2.08, 1.36]}
17:05:54.544 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.544 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.544 [main] INFO Algorithm - Generation: 421 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.544 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.544 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.544 [main] DEBUG Individual - Percentage: 0.08
17:05:54.544 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.544 [main] DEBUG Individual - Percentage: 0.06
17:05:54.544 [main] DEBUG Algorithm - New offspring Individual{fitness=0.42, genes=[2.44, 1.76]}
17:05:54.544 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.544 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.544 [main] INFO Algorithm - Generation: 422 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.544 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.544 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.544 [main] DEBUG Individual - Percentage: 0.08
17:05:54.544 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.544 [main] DEBUG Individual - Percentage: 0.01
17:05:54.544 [main] DEBUG Algorithm - New offspring Individual{fitness=1.17, genes=[2.41, 1.63]}
17:05:54.545 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.545 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.545 [main] INFO Algorithm - Generation: 423 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.545 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.545 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.545 [main] DEBUG Individual - Percentage: 0.03
17:05:54.545 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.545 [main] DEBUG Individual - Percentage: 0.01
17:05:54.545 [main] DEBUG Algorithm - New offspring Individual{fitness=1.51, genes=[2.05, 1.56]}
17:05:54.545 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.545 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.545 [main] INFO Algorithm - Generation: 424 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.545 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.545 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.545 [main] DEBUG Individual - Percentage: 0.09
17:05:54.545 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.545 [main] DEBUG Individual - Percentage: 0.04
17:05:54.545 [main] DEBUG Algorithm - New offspring Individual{fitness=0.74, genes=[2.48, 1.68]}
17:05:54.545 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.545 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.545 [main] INFO Algorithm - Generation: 425 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.545 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.545 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.545 [main] DEBUG Individual - Percentage: 0.07
17:05:54.545 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.545 [main] DEBUG Individual - Percentage: 0.00
17:05:54.545 [main] DEBUG Algorithm - New offspring Individual{fitness=1.44, genes=[2.36, 1.58]}
17:05:54.545 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.546 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.546 [main] INFO Algorithm - Generation: 426 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.546 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.546 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.546 [main] DEBUG Individual - Percentage: 0.06
17:05:54.546 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.546 [main] DEBUG Individual - Percentage: 0.07
17:05:54.546 [main] DEBUG Algorithm - New offspring Individual{fitness=0.44, genes=[2.40, 1.79]}
17:05:54.546 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.546 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.546 [main] INFO Algorithm - Generation: 427 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.546 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.546 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.546 [main] DEBUG Individual - Percentage: 0.06
17:05:54.546 [main] DEBUG Individual - Mutated by: -0.31
17:05:54.546 [main] DEBUG Individual - Percentage: 0.10
17:05:54.546 [main] DEBUG Algorithm - New offspring Individual{fitness=0.48, genes=[2.01, 1.30]}
17:05:54.546 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.546 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.546 [main] INFO Algorithm - Generation: 428 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.546 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.546 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.546 [main] DEBUG Individual - Percentage: 0.05
17:05:54.546 [main] DEBUG Individual - Mutated by: 0.02
17:05:54.546 [main] DEBUG Individual - Percentage: 0.01
17:05:54.546 [main] DEBUG Algorithm - New offspring Individual{fitness=1.27, genes=[2.38, 1.63]}
17:05:54.546 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.547 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.547 [main] INFO Algorithm - Generation: 429 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.547 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.547 [main] DEBUG Individual - Mutated by: 0.04
17:05:54.547 [main] DEBUG Individual - Percentage: 0.01
17:05:54.547 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.547 [main] DEBUG Individual - Percentage: 0.02
17:05:54.547 [main] DEBUG Algorithm - New offspring Individual{fitness=1.64, genes=[2.20, 1.64]}
17:05:54.547 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.547 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.547 [main] INFO Algorithm - Generation: 430 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.547 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.547 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.547 [main] DEBUG Individual - Percentage: 0.04
17:05:54.547 [main] DEBUG Individual - Mutated by: -0.28
17:05:54.547 [main] DEBUG Individual - Percentage: 0.09
17:05:54.547 [main] DEBUG Algorithm - New offspring Individual{fitness=0.73, genes=[2.10, 1.29]}
17:05:54.547 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.547 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.547 [main] INFO Algorithm - Generation: 431 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.547 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.547 [main] DEBUG Individual - Mutated by: 0.00
17:05:54.547 [main] DEBUG Individual - Percentage: 0.00
17:05:54.547 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.547 [main] DEBUG Individual - Percentage: 0.05
17:05:54.547 [main] DEBUG Algorithm - New offspring Individual{fitness=1.08, genes=[2.16, 1.73]}
17:05:54.547 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.547 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.547 [main] INFO Algorithm - Generation: 432 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.547 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.548 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.548 [main] DEBUG Individual - Percentage: 0.00
17:05:54.548 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.548 [main] DEBUG Individual - Percentage: 0.07
17:05:54.548 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.20, 1.34]}
17:05:54.548 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.548 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.548 [main] INFO Algorithm - Generation: 433 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.548 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.548 [main] DEBUG Individual - Mutated by: 0.09
17:05:54.548 [main] DEBUG Individual - Percentage: 0.03
17:05:54.548 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.548 [main] DEBUG Individual - Percentage: 0.04
17:05:54.548 [main] DEBUG Algorithm - New offspring Individual{fitness=1.24, genes=[2.29, 1.68]}
17:05:54.548 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.548 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.548 [main] INFO Algorithm - Generation: 434 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.548 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.548 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.548 [main] DEBUG Individual - Percentage: 0.05
17:05:54.548 [main] DEBUG Individual - Mutated by: -0.12
17:05:54.548 [main] DEBUG Individual - Percentage: 0.04
17:05:54.548 [main] DEBUG Algorithm - New offspring Individual{fitness=0.99, genes=[2.01, 1.45]}
17:05:54.548 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.548 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.548 [main] INFO Algorithm - Generation: 435 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.548 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.548 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.548 [main] DEBUG Individual - Percentage: 0.06
17:05:54.548 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.548 [main] DEBUG Individual - Percentage: 0.02
17:05:54.549 [main] DEBUG Algorithm - New offspring Individual{fitness=1.15, genes=[1.97, 1.51]}
17:05:54.549 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.549 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.549 [main] INFO Algorithm - Generation: 436 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.549 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.549 [main] DEBUG Individual - Mutated by: -0.15
17:05:54.549 [main] DEBUG Individual - Percentage: 0.05
17:05:54.549 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.549 [main] DEBUG Individual - Percentage: 0.09
17:05:54.549 [main] DEBUG Algorithm - New offspring Individual{fitness=0.54, genes=[2.02, 1.31]}
17:05:54.549 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.549 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.549 [main] INFO Algorithm - Generation: 437 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.549 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.549 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.549 [main] DEBUG Individual - Percentage: 0.01
17:05:54.549 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.549 [main] DEBUG Individual - Percentage: 0.03
17:05:54.549 [main] DEBUG Algorithm - New offspring Individual{fitness=1.38, genes=[2.14, 1.46]}
17:05:54.549 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.549 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.549 [main] INFO Algorithm - Generation: 438 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.549 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.549 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.549 [main] DEBUG Individual - Percentage: 0.07
17:05:54.549 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.549 [main] DEBUG Individual - Percentage: 0.10
17:05:54.550 [main] DEBUG Algorithm - New offspring Individual{fitness=0.41, genes=[2.38, 1.90]}
17:05:54.550 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.550 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.550 [main] INFO Algorithm - Generation: 439 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.550 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.550 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.550 [main] DEBUG Individual - Percentage: 0.10
17:05:54.550 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.550 [main] DEBUG Individual - Percentage: 0.08
17:05:54.550 [main] DEBUG Algorithm - New offspring Individual{fitness=0.11, genes=[2.53, 1.82]}
17:05:54.550 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.550 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.550 [main] INFO Algorithm - Generation: 440 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.550 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.550 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.550 [main] DEBUG Individual - Percentage: 0.08
17:05:54.550 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.550 [main] DEBUG Individual - Percentage: 0.00
17:05:54.550 [main] DEBUG Algorithm - New offspring Individual{fitness=1.22, genes=[2.44, 1.57]}
17:05:54.550 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.550 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.550 [main] INFO Algorithm - Generation: 441 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.550 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.550 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.550 [main] DEBUG Individual - Percentage: 0.07
17:05:54.550 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.550 [main] DEBUG Individual - Percentage: 0.05
17:05:54.551 [main] DEBUG Algorithm - New offspring Individual{fitness=0.60, genes=[1.93, 1.41]}
17:05:54.551 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.551 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.551 [main] INFO Algorithm - Generation: 442 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.551 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.551 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.551 [main] DEBUG Individual - Percentage: 0.10
17:05:54.551 [main] DEBUG Individual - Mutated by: 0.05
17:05:54.551 [main] DEBUG Individual - Percentage: 0.02
17:05:54.551 [main] DEBUG Algorithm - New offspring Individual{fitness=0.98, genes=[2.51, 1.62]}
17:05:54.551 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.551 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.551 [main] INFO Algorithm - Generation: 443 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.551 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.551 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.551 [main] DEBUG Individual - Percentage: 0.07
17:05:54.551 [main] DEBUG Individual - Mutated by: 0.13
17:05:54.551 [main] DEBUG Individual - Percentage: 0.04
17:05:54.551 [main] DEBUG Algorithm - New offspring Individual{fitness=0.73, genes=[2.43, 1.70]}
17:05:54.551 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.551 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.551 [main] INFO Algorithm - Generation: 444 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.551 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.551 [main] DEBUG Individual - Mutated by: 0.12
17:05:54.551 [main] DEBUG Individual - Percentage: 0.04
17:05:54.551 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.551 [main] DEBUG Individual - Percentage: 0.03
17:05:54.551 [main] DEBUG Algorithm - New offspring Individual{fitness=1.34, genes=[2.27, 1.68]}
17:05:54.551 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.552 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.552 [main] INFO Algorithm - Generation: 445 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.552 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.552 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.552 [main] DEBUG Individual - Percentage: 0.02
17:05:54.552 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.552 [main] DEBUG Individual - Percentage: 0.05
17:05:54.552 [main] DEBUG Algorithm - New offspring Individual{fitness=1.11, genes=[2.22, 1.73]}
17:05:54.552 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.552 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.552 [main] INFO Algorithm - Generation: 446 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.552 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.552 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.552 [main] DEBUG Individual - Percentage: 0.06
17:05:54.552 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.552 [main] DEBUG Individual - Percentage: 0.09
17:05:54.552 [main] DEBUG Algorithm - New offspring Individual{fitness=0.42, genes=[2.00, 1.28]}
17:05:54.552 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.552 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.552 [main] INFO Algorithm - Generation: 447 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.552 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.552 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.552 [main] DEBUG Individual - Percentage: 0.01
17:05:54.552 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.552 [main] DEBUG Individual - Percentage: 0.06
17:05:54.552 [main] DEBUG Algorithm - New offspring Individual{fitness=1.02, genes=[2.18, 1.37]}
17:05:54.552 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.552 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.552 [main] INFO Algorithm - Generation: 448 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.552 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.552 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.552 [main] DEBUG Individual - Percentage: 0.10
17:05:54.552 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.552 [main] DEBUG Individual - Percentage: 0.05
17:05:54.552 [main] DEBUG Algorithm - New offspring Individual{fitness=0.48, genes=[2.52, 1.71]}
17:05:54.552 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.553 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.553 [main] INFO Algorithm - Generation: 449 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.553 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.553 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.553 [main] DEBUG Individual - Percentage: 0.01
17:05:54.553 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.553 [main] DEBUG Individual - Percentage: 0.02
17:05:54.553 [main] DEBUG Algorithm - New offspring Individual{fitness=1.57, genes=[2.16, 1.49]}
17:05:54.553 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.553 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.553 [main] INFO Algorithm - Generation: 450 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.553 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.553 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.553 [main] DEBUG Individual - Percentage: 0.08
17:05:54.553 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.553 [main] DEBUG Individual - Percentage: 0.09
17:05:54.553 [main] DEBUG Algorithm - New offspring Individual{fitness=0.36, genes=[2.40, 1.83]}
17:05:54.553 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.553 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.553 [main] INFO Algorithm - Generation: 451 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.553 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.553 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.553 [main] DEBUG Individual - Percentage: 0.06
17:05:54.553 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.553 [main] DEBUG Individual - Percentage: 0.05
17:05:54.553 [main] DEBUG Algorithm - New offspring Individual{fitness=0.77, genes=[2.03, 1.39]}
17:05:54.553 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.553 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.553 [main] INFO Algorithm - Generation: 452 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.554 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.554 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.554 [main] DEBUG Individual - Percentage: 0.08
17:05:54.554 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.554 [main] DEBUG Individual - Percentage: 0.02
17:05:54.554 [main] DEBUG Algorithm - New offspring Individual{fitness=1.04, genes=[2.44, 1.64]}
17:05:54.554 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.554 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.554 [main] INFO Algorithm - Generation: 453 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.554 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.554 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.554 [main] DEBUG Individual - Percentage: 0.05
17:05:54.554 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.554 [main] DEBUG Individual - Percentage: 0.08
17:05:54.554 [main] DEBUG Algorithm - New offspring Individual{fitness=0.65, genes=[2.32, 1.81]}
17:05:54.554 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.554 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.554 [main] INFO Algorithm - Generation: 454 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.554 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.554 [main] DEBUG Individual - Mutated by: -0.25
17:05:54.554 [main] DEBUG Individual - Percentage: 0.08
17:05:54.554 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.554 [main] DEBUG Individual - Percentage: 0.01
17:05:54.554 [main] DEBUG Algorithm - New offspring Individual{fitness=1.28, genes=[1.97, 1.56]}
17:05:54.554 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.554 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.554 [main] INFO Algorithm - Generation: 455 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.554 [main] INFO Algorithm - Selected Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.554 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.554 [main] DEBUG Individual - Percentage: 0.07
17:05:54.554 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.554 [main] DEBUG Individual - Percentage: 0.05
17:05:54.554 [main] DEBUG Algorithm - New offspring Individual{fitness=0.56, genes=[1.92, 1.41]}
17:05:54.554 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.555 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.555 [main] INFO Algorithm - Generation: 456 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.555 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.555 [main] DEBUG Individual - Mutated by: 0.27
17:05:54.555 [main] DEBUG Individual - Percentage: 0.09
17:05:54.555 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.555 [main] DEBUG Individual - Percentage: 0.02
17:05:54.555 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.47, 1.64]}
17:05:54.555 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.555 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.555 [main] INFO Algorithm - Generation: 457 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.555 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.555 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.555 [main] DEBUG Individual - Percentage: 0.09
17:05:54.555 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.555 [main] DEBUG Individual - Percentage: 0.10
17:05:54.555 [main] DEBUG Algorithm - New offspring Individual{fitness=0.24, genes=[1.93, 1.27]}
17:05:54.555 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.555 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.555 [main] INFO Algorithm - Generation: 458 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.555 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.555 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.555 [main] DEBUG Individual - Percentage: 0.07
17:05:54.555 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.555 [main] DEBUG Individual - Percentage: 0.10
17:05:54.555 [main] DEBUG Algorithm - New offspring Individual{fitness=0.28, genes=[1.95, 1.26]}
17:05:54.555 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.556 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.556 [main] INFO Algorithm - Generation: 459 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.556 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.556 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.556 [main] DEBUG Individual - Percentage: 0.01
17:05:54.556 [main] DEBUG Individual - Mutated by: -0.03
17:05:54.556 [main] DEBUG Individual - Percentage: 0.01
17:05:54.556 [main] DEBUG Algorithm - New offspring Individual{fitness=1.72, genes=[2.13, 1.56]}
17:05:54.556 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.556 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.556 [main] INFO Algorithm - Generation: 460 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.556 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.556 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.556 [main] DEBUG Individual - Percentage: 0.05
17:05:54.556 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.556 [main] DEBUG Individual - Percentage: 0.05
17:05:54.556 [main] DEBUG Algorithm - New offspring Individual{fitness=0.88, genes=[2.33, 1.73]}
17:05:54.556 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.556 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.556 [main] INFO Algorithm - Generation: 461 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.556 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.556 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.556 [main] DEBUG Individual - Percentage: 0.02
17:05:54.556 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.556 [main] DEBUG Individual - Percentage: 0.07
17:05:54.557 [main] DEBUG Algorithm - New offspring Individual{fitness=0.89, genes=[2.12, 1.36]}
17:05:54.557 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.557 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.557 [main] INFO Algorithm - Generation: 462 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.557 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.557 [main] DEBUG Individual - Mutated by: -0.07
17:05:54.557 [main] DEBUG Individual - Percentage: 0.02
17:05:54.557 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.557 [main] DEBUG Individual - Percentage: 0.07
17:05:54.557 [main] DEBUG Algorithm - New offspring Individual{fitness=0.92, genes=[2.15, 1.34]}
17:05:54.557 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.557 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.557 [main] INFO Algorithm - Generation: 463 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.557 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.557 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.557 [main] DEBUG Individual - Percentage: 0.07
17:05:54.557 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.557 [main] DEBUG Individual - Percentage: 0.07
17:05:54.557 [main] DEBUG Algorithm - New offspring Individual{fitness=0.53, genes=[2.38, 1.77]}
17:05:54.557 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.557 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.557 [main] INFO Algorithm - Generation: 464 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.557 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.557 [main] DEBUG Individual - Mutated by: -0.27
17:05:54.557 [main] DEBUG Individual - Percentage: 0.09
17:05:54.557 [main] DEBUG Individual - Mutated by: -0.10
17:05:54.557 [main] DEBUG Individual - Percentage: 0.03
17:05:54.557 [main] DEBUG Algorithm - New offspring Individual{fitness=0.95, genes=[1.95, 1.47]}
17:05:54.558 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.558 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.558 [main] INFO Algorithm - Generation: 465 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.558 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.558 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.558 [main] DEBUG Individual - Percentage: 0.05
17:05:54.558 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.558 [main] DEBUG Individual - Percentage: 0.00
17:05:54.558 [main] DEBUG Algorithm - New offspring Individual{fitness=1.62, genes=[2.31, 1.57]}
17:05:54.558 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.558 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.558 [main] INFO Algorithm - Generation: 466 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.558 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.558 [main] DEBUG Individual - Mutated by: 0.01
17:05:54.558 [main] DEBUG Individual - Percentage: 0.00
17:05:54.558 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.558 [main] DEBUG Individual - Percentage: 0.05
17:05:54.558 [main] DEBUG Algorithm - New offspring Individual{fitness=1.08, genes=[2.21, 1.74]}
17:05:54.558 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.558 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.558 [main] INFO Algorithm - Generation: 467 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.558 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.558 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.558 [main] DEBUG Individual - Percentage: 0.09
17:05:54.558 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.558 [main] DEBUG Individual - Percentage: 0.08
17:05:54.558 [main] DEBUG Algorithm - New offspring Individual{fitness=0.20, genes=[2.48, 1.81]}
17:05:54.558 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.559 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.559 [main] INFO Algorithm - Generation: 468 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.559 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.559 [main] DEBUG Individual - Mutated by: 0.31
17:05:54.559 [main] DEBUG Individual - Percentage: 0.10
17:05:54.559 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.559 [main] DEBUG Individual - Percentage: 0.07
17:05:54.559 [main] DEBUG Algorithm - New offspring Individual{fitness=0.17, genes=[2.53, 1.78]}
17:05:54.559 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.559 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.559 [main] INFO Algorithm - Generation: 469 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.559 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.559 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.559 [main] DEBUG Individual - Percentage: 0.04
17:05:54.559 [main] DEBUG Individual - Mutated by: -0.13
17:05:54.559 [main] DEBUG Individual - Percentage: 0.04
17:05:54.559 [main] DEBUG Algorithm - New offspring Individual{fitness=1.08, genes=[2.07, 1.44]}
17:05:54.559 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.559 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.559 [main] INFO Algorithm - Generation: 470 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.559 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.559 [main] DEBUG Individual - Mutated by: -0.24
17:05:54.559 [main] DEBUG Individual - Percentage: 0.08
17:05:54.559 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.559 [main] DEBUG Individual - Percentage: 0.08
17:05:54.559 [main] DEBUG Algorithm - New offspring Individual{fitness=0.36, genes=[1.96, 1.31]}
17:05:54.559 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.559 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.559 [main] INFO Algorithm - Generation: 471 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.560 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.560 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.560 [main] DEBUG Individual - Percentage: 0.06
17:05:54.560 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.560 [main] DEBUG Individual - Percentage: 0.07
17:05:54.560 [main] DEBUG Algorithm - New offspring Individual{fitness=0.51, genes=[2.39, 1.77]}
17:05:54.560 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.560 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.560 [main] INFO Algorithm - Generation: 472 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.560 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.560 [main] DEBUG Individual - Mutated by: -0.18
17:05:54.560 [main] DEBUG Individual - Percentage: 0.06
17:05:54.560 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.560 [main] DEBUG Individual - Percentage: 0.03
17:05:54.560 [main] DEBUG Algorithm - New offspring Individual{fitness=1.22, genes=[2.04, 1.48]}
17:05:54.560 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.560 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.560 [main] INFO Algorithm - Generation: 473 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.560 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.560 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.560 [main] DEBUG Individual - Percentage: 0.07
17:05:54.560 [main] DEBUG Individual - Mutated by: 0.21
17:05:54.560 [main] DEBUG Individual - Percentage: 0.07
17:05:54.560 [main] DEBUG Algorithm - New offspring Individual{fitness=0.35, genes=[2.43, 1.78]}
17:05:54.560 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.560 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.560 [main] INFO Algorithm - Generation: 474 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.560 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.561 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.561 [main] DEBUG Individual - Percentage: 0.05
17:05:54.561 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.561 [main] DEBUG Individual - Percentage: 0.07
17:05:54.561 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.32, 1.81]}
17:05:54.561 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.561 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.561 [main] INFO Algorithm - Generation: 475 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.561 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.561 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.561 [main] DEBUG Individual - Percentage: 0.01
17:05:54.561 [main] DEBUG Individual - Mutated by: -0.19
17:05:54.561 [main] DEBUG Individual - Percentage: 0.06
17:05:54.561 [main] DEBUG Algorithm - New offspring Individual{fitness=0.96, genes=[2.12, 1.38]}
17:05:54.561 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.561 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.561 [main] INFO Algorithm - Generation: 476 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.561 [main] INFO Algorithm - Selected Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.561 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.561 [main] DEBUG Individual - Percentage: 0.06
17:05:54.561 [main] DEBUG Individual - Mutated by: 0.23
17:05:54.561 [main] DEBUG Individual - Percentage: 0.07
17:05:54.561 [main] DEBUG Algorithm - New offspring Individual{fitness=0.67, genes=[2.32, 1.81]}
17:05:54.561 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.561 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.561 [main] INFO Algorithm - Generation: 477 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.561 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.21, 1.61]}
17:05:54.561 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.561 [main] DEBUG Individual - Percentage: 0.07
17:05:54.561 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.561 [main] DEBUG Individual - Percentage: 0.02
17:05:54.561 [main] DEBUG Algorithm - New offspring Individual{fitness=1.30, genes=[1.98, 1.55]}
17:05:54.561 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.561 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.562 [main] INFO Algorithm - Generation: 478 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.562 [main] DEBUG Individual - Percentage: 0.08
17:05:54.562 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.562 [main] DEBUG Individual - Percentage: 0.06
17:05:54.562 [main] DEBUG Algorithm - New offspring Individual{fitness=0.45, genes=[2.45, 1.74]}
17:05:54.562 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.562 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.562 [main] INFO Algorithm - Generation: 479 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] DEBUG Individual - Mutated by: 0.15
17:05:54.562 [main] DEBUG Individual - Percentage: 0.05
17:05:54.562 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.562 [main] DEBUG Individual - Percentage: 0.02
17:05:54.562 [main] DEBUG Algorithm - New offspring Individual{fitness=1.31, genes=[2.35, 1.64]}
17:05:54.562 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.562 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.562 [main] INFO Algorithm - Generation: 480 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] DEBUG Individual - Mutated by: -0.26
17:05:54.562 [main] DEBUG Individual - Percentage: 0.08
17:05:54.562 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.562 [main] DEBUG Individual - Percentage: 0.00
17:05:54.562 [main] DEBUG Algorithm - New offspring Individual{fitness=1.23, genes=[1.94, 1.56]}
17:05:54.562 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.562 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.562 [main] INFO Algorithm - Generation: 481 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.562 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.562 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.562 [main] DEBUG Individual - Percentage: 0.10
17:05:54.563 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.563 [main] DEBUG Individual - Percentage: 0.01
17:05:54.563 [main] DEBUG Algorithm - New offspring Individual{fitness=1.18, genes=[1.92, 1.56]}
17:05:54.563 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.563 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.563 [main] INFO Algorithm - Generation: 482 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.563 [main] INFO Algorithm - Selected Individual{fitness=1.74, genes=[2.17, 1.60]}
17:05:54.563 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.563 [main] DEBUG Individual - Percentage: 0.05
17:05:54.563 [main] DEBUG Individual - Mutated by: 0.25
17:05:54.563 [main] DEBUG Individual - Percentage: 0.08
17:05:54.563 [main] DEBUG Algorithm - New offspring Individual{fitness=0.55, genes=[2.34, 1.85]}
17:05:54.563 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.563 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.563 [main] INFO Algorithm - Generation: 483 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.563 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.563 [main] DEBUG Individual - Mutated by: -0.02
17:05:54.563 [main] DEBUG Individual - Percentage: 0.01
17:05:54.563 [main] DEBUG Individual - Mutated by: -0.30
17:05:54.563 [main] DEBUG Individual - Percentage: 0.10
17:05:54.563 [main] DEBUG Algorithm - New offspring Individual{fitness=0.84, genes=[2.19, 1.26]}
17:05:54.563 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.563 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.563 [main] INFO Algorithm - Generation: 484 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.563 [main] INFO Algorithm - Selected Individual{fitness=1.76, genes=[2.15, 1.57]}
17:05:54.563 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.563 [main] DEBUG Individual - Percentage: 0.05
17:05:54.563 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.563 [main] DEBUG Individual - Percentage: 0.02
17:05:54.563 [main] DEBUG Algorithm - New offspring Individual{fitness=1.23, genes=[1.99, 1.52]}
17:05:54.563 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.564 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.564 [main] INFO Algorithm - Generation: 485 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.564 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.564 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.564 [main] DEBUG Individual - Percentage: 0.08
17:05:54.564 [main] DEBUG Individual - Mutated by: 0.24
17:05:54.564 [main] DEBUG Individual - Percentage: 0.08
17:05:54.564 [main] DEBUG Algorithm - New offspring Individual{fitness=0.29, genes=[2.44, 1.81]}
17:05:54.564 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.564 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.564 [main] INFO Algorithm - Generation: 486 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.564 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.564 [main] DEBUG Individual - Mutated by: 0.16
17:05:54.564 [main] DEBUG Individual - Percentage: 0.05
17:05:54.564 [main] DEBUG Individual - Mutated by: 0.22
17:05:54.564 [main] DEBUG Individual - Percentage: 0.07
17:05:54.564 [main] DEBUG Algorithm - New offspring Individual{fitness=0.55, genes=[2.37, 1.79]}
17:05:54.564 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.564 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.564 [main] INFO Algorithm - Generation: 487 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.564 [main] INFO Algorithm - Selected Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.564 [main] DEBUG Individual - Mutated by: -0.11
17:05:54.564 [main] DEBUG Individual - Percentage: 0.04
17:05:54.564 [main] DEBUG Individual - Mutated by: -0.08
17:05:54.564 [main] DEBUG Individual - Percentage: 0.02
17:05:54.564 [main] DEBUG Algorithm - New offspring Individual{fitness=1.16, genes=[2.03, 1.47]}
17:05:54.564 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.565 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.565 [main] INFO Algorithm - Generation: 488 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.565 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.565 [main] DEBUG Individual - Mutated by: 0.17
17:05:54.565 [main] DEBUG Individual - Percentage: 0.05
17:05:54.565 [main] DEBUG Individual - Mutated by: 0.10
17:05:54.565 [main] DEBUG Individual - Percentage: 0.03
17:05:54.565 [main] DEBUG Algorithm - New offspring Individual{fitness=1.08, genes=[2.38, 1.67]}
17:05:54.565 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.565 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.565 [main] INFO Algorithm - Generation: 489 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.565 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.22, 1.58]}
17:05:54.565 [main] DEBUG Individual - Mutated by: -0.04
17:05:54.565 [main] DEBUG Individual - Percentage: 0.01
17:05:54.565 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.565 [main] DEBUG Individual - Percentage: 0.05
17:05:54.565 [main] DEBUG Algorithm - New offspring Individual{fitness=1.20, genes=[2.18, 1.42]}
17:05:54.565 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.565 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.565 [main] INFO Algorithm - Generation: 490 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.565 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.565 [main] DEBUG Individual - Mutated by: -0.22
17:05:54.565 [main] DEBUG Individual - Percentage: 0.07
17:05:54.565 [main] DEBUG Individual - Mutated by: -0.01
17:05:54.565 [main] DEBUG Individual - Percentage: 0.00
17:05:54.565 [main] DEBUG Algorithm - New offspring Individual{fitness=1.31, genes=[1.98, 1.56]}
17:05:54.565 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.565 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.565 [main] INFO Algorithm - Generation: 491 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.565 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.566 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.566 [main] DEBUG Individual - Percentage: 0.06
17:05:54.566 [main] DEBUG Individual - Mutated by: 0.28
17:05:54.566 [main] DEBUG Individual - Percentage: 0.09
17:05:54.566 [main] DEBUG Algorithm - New offspring Individual{fitness=0.52, genes=[2.35, 1.87]}
17:05:54.566 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.566 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.72, genes=[2.13, 1.57]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.566 [main] INFO Algorithm - Generation: 492 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.566 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.566 [main] DEBUG Individual - Mutated by: -0.06
17:05:54.566 [main] DEBUG Individual - Percentage: 0.02
17:05:54.566 [main] DEBUG Individual - Mutated by: -0.00
17:05:54.566 [main] DEBUG Individual - Percentage: 0.00
17:05:54.566 [main] DEBUG Algorithm - New offspring Individual{fitness=1.75, genes=[2.15, 1.56]}
17:05:54.566 [main] DEBUG Algorithm - Weakest Individual{fitness=1.72, genes=[2.13, 1.57]}
17:05:54.566 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.566 [main] INFO Algorithm - Generation: 493 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.566 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.566 [main] DEBUG Individual - Mutated by: -0.16
17:05:54.566 [main] DEBUG Individual - Percentage: 0.05
17:05:54.566 [main] DEBUG Individual - Mutated by: -0.20
17:05:54.566 [main] DEBUG Individual - Percentage: 0.06
17:05:54.566 [main] DEBUG Algorithm - New offspring Individual{fitness=0.69, genes=[2.04, 1.37]}
17:05:54.566 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.566 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.566 [main] INFO Algorithm - Generation: 494 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.566 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.566 [main] DEBUG Individual - Mutated by: 0.11
17:05:54.566 [main] DEBUG Individual - Percentage: 0.04
17:05:54.566 [main] DEBUG Individual - Mutated by: 0.07
17:05:54.567 [main] DEBUG Individual - Percentage: 0.02
17:05:54.567 [main] DEBUG Algorithm - New offspring Individual{fitness=1.44, genes=[2.31, 1.64]}
17:05:54.567 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.567 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.567 [main] INFO Algorithm - Generation: 495 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.567 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.21, 1.57]}
17:05:54.567 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.567 [main] DEBUG Individual - Percentage: 0.09
17:05:54.567 [main] DEBUG Individual - Mutated by: -0.29
17:05:54.567 [main] DEBUG Individual - Percentage: 0.09
17:05:54.567 [main] DEBUG Algorithm - New offspring Individual{fitness=0.22, genes=[1.91, 1.27]}
17:05:54.567 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.567 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.567 [main] INFO Algorithm - Generation: 496 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.567 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.567 [main] DEBUG Individual - Mutated by: -0.05
17:05:54.567 [main] DEBUG Individual - Percentage: 0.01
17:05:54.567 [main] DEBUG Individual - Mutated by: -0.21
17:05:54.567 [main] DEBUG Individual - Percentage: 0.07
17:05:54.567 [main] DEBUG Algorithm - New offspring Individual{fitness=0.92, genes=[2.11, 1.37]}
17:05:54.567 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.567 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.567 [main] INFO Algorithm - Generation: 497 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.567 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.56]}
17:05:54.567 [main] DEBUG Individual - Mutated by: 0.20
17:05:54.567 [main] DEBUG Individual - Percentage: 0.06
17:05:54.567 [main] DEBUG Individual - Mutated by: 0.19
17:05:54.567 [main] DEBUG Individual - Percentage: 0.06
17:05:54.567 [main] DEBUG Algorithm - New offspring Individual{fitness=0.69, genes=[2.36, 1.75]}
17:05:54.567 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.568 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.568 [main] INFO Algorithm - Generation: 498 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.568 [main] INFO Algorithm - Selected Individual{fitness=1.77, genes=[2.16, 1.58]}
17:05:54.568 [main] DEBUG Individual - Mutated by: -0.14
17:05:54.568 [main] DEBUG Individual - Percentage: 0.05
17:05:54.568 [main] DEBUG Individual - Mutated by: -0.23
17:05:54.568 [main] DEBUG Individual - Percentage: 0.07
17:05:54.568 [main] DEBUG Algorithm - New offspring Individual{fitness=0.59, genes=[2.02, 1.35]}
17:05:54.568 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.568 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.568 [main] INFO Algorithm - Generation: 499 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.568 [main] INFO Algorithm - Selected Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.568 [main] DEBUG Individual - Mutated by: -0.09
17:05:54.568 [main] DEBUG Individual - Percentage: 0.03
17:05:54.568 [main] DEBUG Individual - Mutated by: -0.17
17:05:54.568 [main] DEBUG Individual - Percentage: 0.05
17:05:54.568 [main] DEBUG Algorithm - New offspring Individual{fitness=1.02, genes=[2.11, 1.40]}
17:05:54.568 [main] DEBUG Algorithm - Weakest Individual{fitness=1.73, genes=[2.14, 1.55]}
17:05:54.568 [main] DEBUG Algorithm - Population{individuals=[Individual{fitness=1.74, genes=[2.17, 1.60]}, Individual{fitness=1.74, genes=[2.21, 1.61]}, Individual{fitness=1.80, genes=[2.22, 1.58]}, Individual{fitness=1.77, genes=[2.16, 1.56]}, Individual{fitness=1.76, genes=[2.15, 1.57]}, Individual{fitness=1.80, genes=[2.21, 1.57]}, Individual{fitness=1.80, genes=[2.20, 1.57]}, Individual{fitness=1.75, genes=[2.15, 1.56]}, Individual{fitness=1.77, genes=[2.16, 1.58]}, Individual{fitness=1.73, genes=[2.14, 1.55]}]}
17:05:54.568 [main] INFO Algorithm - Generation: 500 Fittest: Individual{fitness=1.80, genes=[2.20, 1.57]}
17:05:54.570 [main] INFO Algorithm - The best solution Individual{fitness=1.80, genes=[2.20, 1.57]} 

Process finished with exit code 0
``` 