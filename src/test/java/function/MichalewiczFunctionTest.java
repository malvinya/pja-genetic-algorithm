package function;

import org.junit.Assert;
import org.junit.Test;

public class MichalewiczFunctionTest {

    MichalewiczFunction f = new MichalewiczFunction(10);

    @Test
    public void testCorrectValue() {
        double result = f.apply(new double[]{2.20, 1.57});
        Assert.assertEquals(result, -1.8013, 0.001);
        System.out.println(result);
        //-1.801140718473825
    }
}