package function;

import function.Domain;
import org.junit.Assert;
import org.junit.Test;

public class DomainTest {

    @Test
    public void testSize() {
        Assert.assertEquals(new Domain(0, 5).size, 5,0.0001);
        Assert.assertEquals(new Domain(3, 5).size, 2,0.0001);
        Assert.assertEquals(new Domain(-2, 5).size, 7,0.0001);
    }

    @Test
    public void testWithinDomain() {
        Domain domain = new Domain(0, 5);
        boolean contains = domain.contains(4);
        Assert.assertTrue(contains);
        boolean contains2 = domain.contains(-4);
        Assert.assertFalse(contains2);
        boolean contains3 = domain.contains(0);
        Assert.assertTrue(contains3);
        boolean contains4 = domain.contains(5);
        Assert.assertTrue(contains4);

    }
}