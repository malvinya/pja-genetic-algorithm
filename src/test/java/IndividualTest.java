import function.SquareFunction;
import org.junit.Test;

public class IndividualTest {

    @Test
    public void testMutation() {
        Individual individual = new Individual(new double[]{4}, new SquareFunction());

        System.out.println(individual);
        Individual mutate = individual.mutate();

        System.out.println(mutate);
    }
}