package function;

public class SquareFunction implements FitnessFunction {
    private static final Domain DOMAIN = new Domain(0, 5);

    @Override
    public double apply(double[] vector) {
        double result = 0;
        for (double x : vector) {
            result = result + x * x;
        }
        return result;
    }

    @Override
    public Domain getDomain() {
        return DOMAIN;
    }

    @Override
    public int getDimensionality() {
        return 1;
    }
}
