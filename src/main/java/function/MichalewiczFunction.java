package function;

import java.math.BigDecimal;

/*
https://www.sfu.ca/~ssurjano/michal.html?fbclid=IwAR0F1CRYHvZu7u92mQNtiAFRYXQ2hPqXHbfJJ0FIIk-DoHbceq58rTPCVgw
https://github.com/giskou/Optimization/blob/master/src/libs/functions/Michalewicz.java
 */
public class MichalewiczFunction implements FitnessFunction {

    private static final Domain DOMAIN = new Domain(0, new BigDecimal(Math.PI).doubleValue());
    private int m;

    public MichalewiczFunction(int m) {
        this.m = m;
    }

    @Override
    public double apply(double[] vector) {
        double sum = 0;
        for (int i = 0; i < vector.length; i++) {
            sum -= Math.sin(vector[i]) * Math.pow(Math.sin((i + 1) * Math.pow(vector[i], 2) / Math.PI), 2 * m);
        }
        return -sum;
    }

    @Override
    public Domain getDomain() {
        return DOMAIN;
    }

    @Override
    public int getDimensionality() {
        return 2;
    }
}
