package function;

public interface FitnessFunction {

    double apply(double[] vector);

    Domain getDomain();

    int getDimensionality();
}
