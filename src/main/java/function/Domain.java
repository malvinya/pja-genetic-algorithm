package function;

public class Domain {

    double max;
    double min;
    double size;

    public Domain(double min, double max) {
        this.max = max;
        this.min = min;
        this.size = max - min;
    }

    boolean contains(double value) {
        return value - min >= 0
                && value - max <= 0;
    }

    public double cap(double value) {
        if (value >= min && value <= max) {
            return value;
        }
        if (value > max) return max;
        if (value < min) return min;
        throw new IllegalArgumentException();
    }

    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public double getSize() {
        return size;
    }
}
