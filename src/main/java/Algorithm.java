import function.MichalewiczFunction;
import function.SquareFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Algorithm {

    private static final Logger LOGGER = LoggerFactory.getLogger(Algorithm.class.getName());
    private static final int EVOLUTION_CYCLES = 500;
    private static final int INITIAL_POPULATION_SIZE = 10;
    private static final SquareFunction SQUERE_FUNCTION = new SquareFunction();
    private static final MichalewiczFunction MICHALEWICZ_FUNCTION = new MichalewiczFunction(10);

    public static void main(String[] args) {
        Population population = new Population(INITIAL_POPULATION_SIZE, MICHALEWICZ_FUNCTION);

        int generationCount = 0;
        System.out.println("Generation: " + generationCount + " Fittest: " + population.getFittest());

        while (generationCount < EVOLUTION_CYCLES) {
            ++generationCount;

            Individual selectedIndividual = tournamentSelection(population);

            Individual offspring = selectedIndividual.mutate();
            LOGGER.debug("New offspring {}", offspring);

            Individual weakest = population.getWeakest();
            LOGGER.debug("Weakest {}", weakest);
            if (offspring.getFitness() - weakest.getFitness() > 0) {
                population.replaceWeakest(offspring);
            }
            LOGGER.debug(population.toString());
            LOGGER.info("Generation: {} Fittest: {}", generationCount, population.getFittest());
        }
        LOGGER.info("The best solution {} ", population.getFittest());
    }

    private static Individual tournamentSelection(Population population) {
        Individual one = population.selectIndividualRandomly();
        Individual two = population.selectIndividualRandomly();
        Individual selected = one.getFitness()- two.getFitness() > 0 ? one : two;
        LOGGER.info("Selected {}", selected);
        return selected;
    }
}
