package utils;

import java.util.Random;

public class RandomUtils {

    public static final Random random = new Random();

    public static double random(double range) {
        double randFromDouble = Math.random();
        double actualRandomDec = randFromDouble * range;
//        actualRandomDec = actualRandomDec
//                .setScale(2, BigDecimal.ROUND_DOWN);
        return actualRandomDec;
    }

    public static int randomSign() {
        return random.nextBoolean() ? 1 : -1;
    }

}
