import function.FitnessFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ArrayUtils;
import utils.RandomUtils;

class Individual {

    private static final Logger LOGGER = LoggerFactory.getLogger(Individual.class.getName());
    private static final double MAX_PERCENTAGE_MUTATION = 10.0;
    private final FitnessFunction fitnessFunction;
    private double fitness;
    private double[] genes;

    Individual(double[] genes, FitnessFunction fitnessFunction) {
        this.fitnessFunction = fitnessFunction;
        this.genes = genes;
        fitness = fitnessFunction.apply(genes);
    }

    Individual(FitnessFunction fitnessFunction) {
        this.fitnessFunction = fitnessFunction;
        genes = new double[fitnessFunction.getDimensionality()];
        for (int i = 0; i < genes.length; i++) {
            genes[i] = RandomUtils.random(fitnessFunction.getDomain().getMax());
        }
        fitness = fitnessFunction.apply(genes);
    }

    double getFitness() {
        return fitness;
    }

    Individual mutate() {
        int sign = RandomUtils.randomSign();
        double newValue;
        double modification;
        double mutatedBy;
        double[] newGenes = new double[genes.length];
        for (int i = 0; i < genes.length; i++) {
            modification = RandomUtils.random(MAX_PERCENTAGE_MUTATION) / 100;
            mutatedBy = sign * modification * fitnessFunction.getDomain().getSize();
            newValue = fitnessFunction.getDomain().cap(genes[i] + mutatedBy);
            newGenes[i] = newValue;
            LOGGER.debug("Mutated by: {}", String.format("%.2f", mutatedBy));
            LOGGER.debug("Percentage: {}", String.format("%.2f", modification));
        }
        return new Individual(newGenes, fitnessFunction);
    }

    @Override
    public String toString() {
        return "Individual{" +
                "fitness=" + String.format("%.2f", fitness) +
                ", genes=" + ArrayUtils.toString(genes) +
                '}';
    }

}
