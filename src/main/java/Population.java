import function.FitnessFunction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class Population {

    private final int popSize;
    private ArrayList<Individual> individuals;
    private Random random = new Random();

    public Population(int popSize, FitnessFunction fitnessFunction) {
        this.popSize = popSize;
        individuals = new ArrayList<>(popSize);
        for (int i = 0; i < popSize; i++) {
            individuals.add(new Individual(fitnessFunction));
        }
    }

    public int getWeakestIndex() {
        return individuals.indexOf(getWeakest());
    }

    public Individual getWeakest() {
        return Collections.min(individuals, Comparator.comparing(Individual::getFitness));
    }

    public void replaceWeakest(Individual individual) {
        int weakestIndex = getWeakestIndex();
        individuals.set(weakestIndex, individual);
    }

    public Individual getFittest() {
        return Collections.max(individuals, Comparator.comparing(Individual::getFitness));
    }

    public Individual selectIndividualRandomly() {
        int randomIndex1 = random.nextInt(popSize);
        return individuals.get(randomIndex1);
    }

    @Override
    public String toString() {
        return "Population{" +
                "individuals=" + individuals +
                '}';
    }
}
